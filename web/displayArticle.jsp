<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<jsp:useBean id="article" class="dota.bean.Article" scope="session"></jsp:useBean>
<jsp:useBean id="member" class="dota.bean.Member" scope="session"></jsp:useBean>
<jsp:useBean id="aComments" class="java.util.ArrayList" scope="session"></jsp:useBean>
<jsp:useBean id="comment" class="dota.bean.AComment" scope="session"></jsp:useBean>

<HTML>
<HEAD>
<TITLE>DOTA文章</TITLE>
<meta http-equiv="Content-Style-Type" content="text/css">
<LINK HREF="style.css" TYPE="text/css" REL="stylesheet">

</HEAD>
<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0>
<TABLE WIDTH=766 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR><TD><br><IMG SRC="images/top.jpg" ALT="" WIDTH=766 HEIGHT=60 border="0" usemap="#Map2"></TD>
	</TR>
	<TR>
		<TD WIDTH=766 HEIGHT=532 ALT="" valign="top">
		<TABLE WIDTH=766 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD WIDTH=256 HEIGHT=532 ALT="" valign="top">
		<TABLE WIDTH=256 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD COLSPAN=3>
			<IMG SRC="images/logo.jpg" ALT="" WIDTH=256 HEIGHT=76 border="0" usemap="#Map"></TD>
	</TR>
	<TR>
		<TD>
			<IMG SRC="images/q1.jpg" WIDTH=43 HEIGHT=40 ALT=""></TD>
		<TD>
			<a href="/dota/displayTavern.jsp"><IMG SRC="images/m1.jpg" ALT="" WIDTH=143 HEIGHT=40 border="0"></a></TD>
		<TD>
			<IMG SRC="images/q2.jpg" WIDTH=70 HEIGHT=40 ALT=""></TD>
	</TR>
	<TR>
		<TD COLSPAN=3>
			<IMG SRC="images/q3.jpg" WIDTH=256 HEIGHT=10 ALT=""></TD>
	</TR>
	<TR>
		<TD>
			<IMG SRC="images/q4.jpg" WIDTH=43 HEIGHT=41 ALT=""></TD>
		<TD>
			<a href="displayShop.jsp"><IMG SRC="images/m2.jpg" ALT="" WIDTH=143 HEIGHT=41 border="0"></a></TD>
		<TD>
			<IMG SRC="images/q5.jpg" WIDTH=70 HEIGHT=41 ALT=""></TD>
	</TR>
	<TR>
		<TD COLSPAN=3>
			<IMG SRC="images/q6.jpg" WIDTH=256 HEIGHT=11 ALT=""></TD>
	</TR>
	<TR>
		<TD>
			<IMG SRC="images/q7.jpg" WIDTH=43 HEIGHT=40 ALT=""></TD>
		<TD>
			<a href="servlet/dota.controller.LoadVedio"><IMG SRC="images/m3.jpg" ALT="" WIDTH=143 HEIGHT=40 border="0"></a></TD>
		<TD>
			<IMG SRC="images/q8.jpg" WIDTH=70 HEIGHT=40 ALT=""></TD>
	</TR>
	<TR>
		<TD COLSPAN=3>
			<IMG SRC="images/q9.jpg" WIDTH=256 HEIGHT=11 ALT=""></TD>
	</TR>
	<TR>
		<TD>
			<IMG SRC="images/q10.jpg" WIDTH=43 HEIGHT=39 ALT=""></TD>
		<TD>
			<a href="servlet/dota.controller.LoadArticle"><IMG SRC="images/m4.jpg" ALT="" WIDTH=143 HEIGHT=39 border="0"></a></TD>
		<TD>
			<IMG SRC="images/q11.jpg" WIDTH=70 HEIGHT=39 ALT=""></TD>
	</TR>
	<TR>
		<TD COLSPAN=3>
			<IMG SRC="images/q12.jpg" WIDTH=256 HEIGHT=264 ALT=""></TD>
	</TR>
</TABLE>
		</TD>
		<TD background="images/bg6.jpg" WIDTH=449 HEIGHT=532 ALT="" valign="top" style="padding-left:22px; padding-top:9px ">
<img src="dotaImages/articledetial.jpg"><br>
<br style="line-height:26px "><div style="height:450px; margin-left: 14px; width: 366px"
									class="cont_l" >
									<br>
<center><b style="font-size: 18px">${article.articleTitle}</b></center>
<table border="0">
<tr><td style="text-align:right; font-size: 12px">${article.articleDate}&nbsp;&nbsp;</td></tr>
<tr><td>${article.content}</td></tr>

</table>
<br>

<c:forEach items="${aComments}" var="comment">
<table border='1' bordercolor="#493B1D" width="100%">
<tr><td ><div style="text-align:left">用户：<b><c:out value="${comment.memPetname}"/></b> </div><div style="text-align:right">发表于  <c:out value="${comment.date}"/></div></td></tr>
<tr><td height="50px" style="vertical-align:top"><c:out value="${comment.content}"/></td></tr>
</table>
<br>
</c:forEach>

<br>
<center><b style="font-size: 18px">发表评论</b></center>
<br>
<table width="345" height="100" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="75" valign="top" style="padding-top:5px ">
<div align="right" style="margin-right:10px " class="light">

<strong>
评论内容：
</strong>
</div>
	</td>
    <form action="servlet/dota.controller.RefreshAComment" method="post">
    <td width="270" valign="top">

<textarea class="textarea" name="content"></textarea><br>
<br style="line-height:5px ">
<input type="hidden" name="objectNo" value="${article.articleNo}">
<input type="hidden" name="memPetname" value="${member.name}">
<input type="hidden" name="category" value="a">
<div align="right"><input name="" type="image" src="images/submit1.jpg"></div>
	</td>
	</form>
  </tr>
</table>
<br style="line-height:10px ">
</div>



		</TD>
		<TD><IMG SRC="images/right.jpg" WIDTH=61 HEIGHT=532 ALT=""></TD>
	</TR>
</TABLE>
		</TD>
	</TR>
	<TR>
		<TD background="images/bottom.jpg" WIDTH=766 HEIGHT=108 ALT="" valign="top" style="padding-top:75px ">
		<div align="right" style="margin-right:75px " class="copy">Copyright &copy; 2005 Yourcompany.com | <a href="index-5.html" class="copy">Privacy Policy</a>  | <a href="#" class="copy">Terms of Use</a></div>
		</TD>
	</TR>
</TABLE>
<map name="Map">
  <area shape="rect" coords="18,-6,211,50" href="index.jsp">
</map>
<map name="Map2">
  <area shape="rect" coords="20,28,208,59" href="index.jsp">
</map>
</BODY>
</HTML>
