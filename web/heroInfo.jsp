<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<jsp:useBean id="member" class="dota.bean.Member" scope="session"></jsp:useBean>
<jsp:useBean id="hero" class="dota.bean.Hero" scope="session"></jsp:useBean>
<jsp:useBean id="levelList1" class="java.util.ArrayList" scope="session"></jsp:useBean>
<jsp:useBean id="levelList2" class="java.util.ArrayList" scope="session"></jsp:useBean>
<jsp:useBean id="levelList3" class="java.util.ArrayList" scope="session"></jsp:useBean>
<jsp:useBean id="levelList4" class="java.util.ArrayList" scope="session"></jsp:useBean>
<HTML>
	<HEAD>
		<TITLE>英雄介绍</TITLE>
		<meta http-equiv="Content-Style-Type" content="text/css">
		<LINK HREF="style.css" TYPE="text/css" REL="stylesheet">
	</HEAD>
	<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0
		MARGINHEIGHT=0>
		<TABLE WIDTH=766 BORDER=0 CELLPADDING=0 CELLSPACING=0>
			<TR>
				<TD>
					<br>
					<IMG SRC="images/top.jpg" ALT="" WIDTH=766 HEIGHT=60 border="0"
						usemap="#Map2">
				</TD>
			</TR>
			<TR>
				<TD WIDTH=766 HEIGHT=532 ALT="" valign="top">
					<TABLE WIDTH=766 BORDER=0 CELLPADDING=0 CELLSPACING=0>
						<TR>
							<TD WIDTH=256 HEIGHT=532 ALT="" valign="top">
								<TABLE WIDTH=256 BORDER=0 CELLPADDING=0 CELLSPACING=0>
									<TR>
										<TD COLSPAN=3>
											<IMG SRC="images/logo.jpg" ALT="" WIDTH=256 HEIGHT=76
												border="0" usemap="#Map">
										</TD>
									</TR>
									<TR>
										<TD>
											<IMG SRC="images/q1.jpg" WIDTH=43 HEIGHT=40 ALT="">
										</TD>
										<TD>
											<a href="/dota/displayTavern.jsp"><IMG SRC="images/m1.jpg" ALT=""
													WIDTH=143 HEIGHT=40 border="0"> </a>
										</TD>
										<TD>
											<IMG SRC="images/q2.jpg" WIDTH=70 HEIGHT=40 ALT="">
										</TD>
									</TR>
									<TR>
										<TD COLSPAN=3>
											<IMG SRC="images/q3.jpg" WIDTH=256 HEIGHT=10 ALT="">
										</TD>
									</TR>
									<TR>
										<TD>
											<IMG SRC="images/q4.jpg" WIDTH=43 HEIGHT=41 ALT="">
										</TD>
										<TD>
											<a href="displayShop.jsp"><IMG SRC="images/m2.jpg" ALT=""
													WIDTH=143 HEIGHT=41 border="0"> </a>
										</TD>
										<TD>
											<IMG SRC="images/q5.jpg" WIDTH=70 HEIGHT=41 ALT="">
										</TD>
									</TR>
									<TR>
										<TD COLSPAN=3>
											<IMG SRC="images/q6.jpg" WIDTH=256 HEIGHT=11 ALT="">
										</TD>
									</TR>
									<TR>
										<TD>
											<IMG SRC="images/q7.jpg" WIDTH=43 HEIGHT=40 ALT="">
										</TD>
										<TD>
											<a href="servlet/dota.controller.LoadVedio"><IMG SRC="images/m3.jpg" ALT=""
													WIDTH=143 HEIGHT=40 border="0"> </a>
										</TD>
										<TD>
											<IMG SRC="images/q8.jpg" WIDTH=70 HEIGHT=40 ALT="">
										</TD>
									</TR>
									<TR>
										<TD COLSPAN=3>
											<IMG SRC="images/q9.jpg" WIDTH=256 HEIGHT=11 ALT="">
										</TD>
									</TR>
									<TR>
										<TD>
											<IMG SRC="images/q10.jpg" WIDTH=43 HEIGHT=39 ALT="">
										</TD>
										<TD>
											<a href="servlet/dota.controller.LoadArticle"><IMG
													SRC="images/m4.jpg" ALT="" WIDTH=143 HEIGHT=39 border="0">
											</a>
										</TD>
										<TD>
											<IMG SRC="images/q11.jpg" WIDTH=70 HEIGHT=39 ALT="">
										</TD>
									</TR>
									<TR>
										<TD COLSPAN=3>
											<IMG SRC="images/q12.jpg" WIDTH=256 HEIGHT=264 ALT="">
										</TD>
									</TR>
								</TABLE>
							</TD>
							<TD background="images/bg6.jpg" WIDTH=449 HEIGHT=532 ALT=""
								valign="top" style="padding-left: 22px; padding-top: 9px">
								<img src="dotaImages/herodetial.jpg">
								<br>
								<br style="line-height: 26px">
								<div style="height:450px; margin-left: 14px; width: 366px"
									class="cont_l" >
									<br>
									<b style="font-size: 18px"><c:out value="${hero.heroName}" />
									</b>
									<table>
										<tr>
											<td width='10%' valign="top">
												<img src='dotaImages/${hero.heroPic}.gif' width='75'
													height='75' border="0"/>
											</td>
											<td width='80%' style="font-size: 12px">
												<c:out value="${hero.heroIntro}" />
											</td>
										</tr>
									</table>

									<table border='1' bordercolor="#493B1D">
										<c:forEach items="${skillList}" var="skill">
											<tr>
												<td>

													<table>
														<tr>
															<td>
																<img src='dotaImages/${skill.skillPic}.gif' width='45'
																	height='45' />
															</td>
															<td>

																<table>
																	<tr>
																		<td style="font-size: 12px">
																			<c:out value="${skill.skillName}" />
																			（快捷键：
																			<c:out value="${skill.skillShortcut}" />
																			）
																		</td>
																	</tr>
																	<tr>
																		<td style="font-size: 12px">
																			<c:out value="${skill.skillDepict}"></c:out>
																		</td>
																	</tr>

																</table>

															</td>
														</tr>
														<c:choose>
															<c:when test="${skill.skillNo==1}">
																<c:forEach items="${levelList1}" var="level">
																	<tr>
																		<td style="font-size: 12px" valign="top">
																			等级
																			<c:out value="${level.levelNo}"></c:out>
																			:
																		</td>
																		<td style="font-size: 12px" >
																			<c:out value="${level.levelDepict}"></c:out>
																		</td>
																	</tr>
																</c:forEach>
															</c:when>
															<c:when test="${skill.skillNo==2}">
																<c:forEach items="${levelList2}" var="level">
																	<tr>
																		<td style="font-size: 12px" valign="top">
																			等级
																			<c:out value="${level.levelNo}"></c:out>
																			:
																		</td>
																		<td style="font-size: 12px">
																			<c:out value="${level.levelDepict}"></c:out>
																		</td>
																	</tr>
																</c:forEach>
															</c:when>
															<c:when test="${skill.skillNo==3}">
																<c:forEach items="${levelList3}" var="level">
																	<tr>
																		<td style="font-size: 12px" valign="top">
																			等级
																			<c:out value="${level.levelNo}"></c:out>
																			:
																		</td>
																		<td style="font-size: 12px">
																			<c:out value="${level.levelDepict}"></c:out>
																		</td>
																	</tr>
																</c:forEach>
															</c:when>
															<c:when test="${skill.skillNo==4}">
																<c:forEach items="${levelList4}" var="level">
																	<tr>
																		<td style="font-size: 12px" valign="top">
																			等级
																			<c:out value="${level.levelNo}"></c:out>
																			:
																		</td>
																		<td style="font-size: 12px">
																			<c:out value="${level.levelDepict}"></c:out>
																		</td>
																	</tr>
																</c:forEach>
															</c:when>
														</c:choose>


													</table>

												</td>
											</tr>
										</c:forEach>
									</table>




									<br style="line-height: 10px">
									<!--  	<a href="#" class="cont_l u" id="a">privacy@companyname.com</a>-->
								</div>



							</TD>
							<TD>
								<IMG SRC="images/right.jpg" WIDTH=61 HEIGHT=532 ALT="">
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
			<TR>
				<TD background="images/bottom.jpg" WIDTH=766 HEIGHT=108 ALT=""
					valign="top" style="padding-top: 75px">
					<div align="right" style="margin-right: 75px" class="copy">
						Copyright &copy; 2005 Yourcompany.com |
						<a href="index-5.html" class="copy">Privacy Policy</a> |
						<a href="#" class="copy">Terms of Use</a>
					</div>
				</TD>
			</TR>
		</TABLE>
		<map name="Map">
			<area shape="rect" coords="18,-6,211,50" href="index.jsp">
		</map>
		<map name="Map2">
			<area shape="rect" coords="20,28,208,59" href="index.jsp">
		</map>
	</BODY>
</HTML>