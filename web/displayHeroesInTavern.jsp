<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<jsp:useBean id="member" class="dota.bean.Member" scope="session"></jsp:useBean>
<jsp:useBean id="hero" class="dota.bean.Hero" scope="session"></jsp:useBean>
<jsp:useBean id="heroes" class="java.util.ArrayList" scope="session"></jsp:useBean>
<HTML>
	<HEAD>
		<TITLE>酒馆介绍</TITLE>
		<meta http-equiv="Content-Style-Type" content="text/css">
		<LINK HREF="style.css" TYPE="text/css" REL="stylesheet">
	</HEAD>
	<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0
		MARGINHEIGHT=0>
		<TABLE WIDTH=766 BORDER=0 CELLPADDING=0 CELLSPACING=0>
			<TR>
				<TD>
					<IMG SRC="images/top.jpg" ALT="" WIDTH=766 HEIGHT=60 border="0"
						usemap="#Map2">
				</TD>
			</TR>
			<TR>
				<TD WIDTH=766 HEIGHT=532 ALT="" valign="top">
					<TABLE WIDTH=766 BORDER=0 CELLPADDING=0 CELLSPACING=0>
						<TR>
							<TD WIDTH=256 HEIGHT=532 ALT="" valign="top">
								<TABLE WIDTH=256 BORDER=0 CELLPADDING=0 CELLSPACING=0>
									<TR>
										<TD COLSPAN=3>
											<IMG SRC="images/logo.jpg" ALT="" WIDTH=256 HEIGHT=76
												border="0" usemap="#Map">
										</TD>
									</TR>
									<TR>
										<TD>
											<IMG SRC="images/q1.jpg" WIDTH=43 HEIGHT=40 ALT="">
										</TD>
										<TD>
											<a href="/dota/displayTavern.jsp"><IMG SRC="images/m1.jpg" ALT=""
													WIDTH=143 HEIGHT=40 border="0"> </a>
										</TD>
										<TD>
											<IMG SRC="images/q2.jpg" WIDTH=70 HEIGHT=40 ALT="">
										</TD>
									</TR>
									<TR>
										<TD COLSPAN=3>
											<IMG SRC="images/q3.jpg" WIDTH=256 HEIGHT=10 ALT="">
										</TD>
									</TR>
									<TR>
										<TD>
											<IMG SRC="images/q4.jpg" WIDTH=43 HEIGHT=41 ALT="">
										</TD>
										<TD>
											<a href="displayShop.jsp"><IMG SRC="images/m2.jpg" ALT=""
													WIDTH=143 HEIGHT=41 border="0"> </a>
										</TD>
										<TD>
											<IMG SRC="images/q5.jpg" WIDTH=70 HEIGHT=41 ALT="">
										</TD>
									</TR>
									<TR>
										<TD COLSPAN=3>
											<IMG SRC="images/q6.jpg" WIDTH=256 HEIGHT=11 ALT="">
										</TD>
									</TR>
									<TR>
										<TD>
											<IMG SRC="images/q7.jpg" WIDTH=43 HEIGHT=40 ALT="">
										</TD>
										<TD>
											<a href="servlet/dota.controller.LoadVedio"><IMG SRC="images/m3.jpg" ALT=""
													WIDTH=143 HEIGHT=40 border="0"> </a>
										</TD>
										<TD>
											<IMG SRC="images/q8.jpg" WIDTH=70 HEIGHT=40 ALT="">
										</TD>
									</TR>
									<TR>
										<TD COLSPAN=3>
											<IMG SRC="images/q9.jpg" WIDTH=256 HEIGHT=11 ALT="">
										</TD>
									</TR>
									<TR>
										<TD>
											<IMG SRC="images/q10.jpg" WIDTH=43 HEIGHT=39 ALT="">
										</TD>
										<TD>
											<a href="servlet/dota.controller.LoadArticle"><IMG SRC="images/m4.jpg" ALT=""
													WIDTH=143 HEIGHT=39 border="0"> </a>
										</TD>
										<TD>
											<IMG SRC="images/q11.jpg" WIDTH=70 HEIGHT=39 ALT="">
										</TD>
									</TR>
									<TR>
										<TD COLSPAN=3>
											<IMG SRC="images/q12.jpg" WIDTH=256 HEIGHT=264 ALT="">
										</TD>
									</TR>
								</TABLE>
							</TD>
							<TD background="images/bg52.jpg" WIDTH=449 HEIGHT=532 ALT=""
								valign="top" style="padding-left: 22px; padding-top: 9px">
								<img src="images/h.jpg">
								<br>
								<br style="line-height: 18px">
								<div style="margin-left: 15px" class="cont_l">
								<embed src="D:/MyEclipseWorkspaces/dota/web/pic.swf"
								width="370" height="120"  wmode="Transparent"></embed>
								<br>
								</div>
								<br style="line-height: 30px">
								<img src="dotaImages/herolist.jpg">
								<br>
								<br style="line-height: 41px">
								<div style="margin-left: 48px" valign="top">

									<table border="1">
										<tr>
											<c:forEach items="${heroes}" var="hero" varStatus="state">
												<td width="25%">
												<a	href="
														<c:url value="servlet/dota.controller.LoadHero" >
															<c:set var="click_on" value="${hero.heroNo}" />
															<c:param name="click_on" value="${hero.heroNo}" />
														</c:url>
													">
													<img src='dotaImages/${hero.heroPic}.gif' width='65'
															height='65' alt="${hero.heroName}" border="0" />
												</a>

												</td>
												<c:if test="${state.count%4==0}">
													</tr>
													<tr>
											</c:if>
											</c:forEach>
										</tr>
									</table>

								</div>

							</TD>
							<TD>
								<IMG SRC="images/right.jpg" WIDTH=61 HEIGHT=532 ALT="">
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
			<TR>
				<TD background="images/bottom.jpg" WIDTH=766 HEIGHT=108 ALT=""
					valign="top" style="padding-top: 75px">
					<div align="right" style="margin-right: 75px" class="copy">
						Copyright &copy; 2005 Yourcompany.com |
						<a href="index-5.html" class="copy">Privacy Policy</a> |
						<a href="#" class="copy">Terms of Use</a>
					</div>
				</TD>
			</TR>
		</TABLE>
		<map name="Map">
			<area shape="rect" coords="18,-6,211,50" href="index.jsp">
		</map>
		<map name="Map2">
			<area shape="rect" coords="20,28,208,59" href="index.jsp">
		</map>
	</BODY>
</HTML>