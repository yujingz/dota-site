drop table member cascade;
drop table tavern cascade;
drop table hero cascade;
drop table skill cascade;
drop table level cascade;
drop table shop cascade;
drop table item cascade;
drop table mix cascade;
drop table item_skill cascade;
drop table vedio cascade;
drop table article cascade;
drop table a_comment cascade;


create table member
(
	member_no		char(10),
	password		char(20),
	birthday		date,
	name			char(40),	

	primary key(member_no)
);

create table tavern
(
	tavern_no		varchar(5) not null,
	tavern_name		char(20) ,
	tavern_pic		varchar(20),
	
	primary key(tavern_no)
);

create table hero
(
	tavern_no		varchar(5) references  tavern(tavern_no) not null,
	hero_no			varchar(5) not null,
	hero_name		char(20),
	hero_pic		varchar(20),
	hero_intro		varchar(300),

	primary key(hero_no)
);

create table skill
(
	hero_no			varchar(5)  references  hero(hero_no) not null,
	skill_no		char(1) not null,
	skill_name		char(20),
	skill_pic         	varchar(20),
	skill_shortcut      	char(1),
	skill_depict            varchar(250),
	
	primary key(hero_no,skill_no)
);

create table level
(
	hero_no			varchar(5) references  hero(hero_no) not null,
	skill_no		char(1) not null,
	level_no		char(1) not null,
	level_depict		varchar(150),

	primary key(hero_no, skill_no, level_no)
);

create table shop
(
	shop_no			varchar(5) not null,
	shop_name		char(20),
	shop_pic		varchar(20),

	primary key(shop_no)
);

create table item
(
	shop_no			varchar(2) references shop(shop_no) not null,
	item_no			varchar(3) not null,
	item_name		char(10),
	item_pic		varchar(20),
	item_price		char(5),
	item_depict		varchar(200),

	primary key(item_no)	
);

create table mix
(
	shop_no		varchar(20) references shop(shop_no) not null,
	item_no		varchar(20) references item(item_no) not null,
	mix_shop_no	varchar(20) references shop(shop_no) not null,
	mix_item_no	varchar(20) references item(item_no) not null,

	primary key(shop_no, item_no, mix_shop_no, mix_item_no)
);


create table item_skill
(
	shop_no			varchar(20) references shop(shop_no) not null,
	item_no			varchar(20) references item(item_no) not null,
	item_skill_no		varchar(1),
	item_skill_name  	char(10),
	item_skill_depict	varchar(100),

	primary key(shop_no, item_no, item_skill_no)
);

create table vedio
(
	vedio_no	char(5) not null,
	page_no		char(3) not null,
	vedio_title	varchar(30),
	vedio_depict	varchar(500),
	vedio_comment_num char(3),
	
	primary key(vedio_no)
	
);

create table article
(
	article_no	char(5) not null,
	page_no		char(3) not null,
	category	char(10),
	article_title	varchar(30),
	article_date	date,
	content		varchar(10000),

	primary key(article_no)
);

create table a_comment
(
	comment_no	serial not null,
	object_no 	char(5) not null,
	mem_petname	varchar(20),
	date		date,
	content		varchar(1000),
	category	char(1),

	primary key(comment_no),
	check (category in ('v','a'))
);

