insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('001','1','魔法箭','skill0011','C','
	向一个敌方单位发射魔法箭，造成伤害，并晕眩1.75秒。施法距离：500
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('001','2','恐怖波动','skill0012','E','
	复仇之魂发出恶毒的吼叫，唤起周围敌方单位深深的恐惧。减少他们的护甲和攻击力，持续
	20秒
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('001','3','命令光环','skill0013','D','
	增加周围300范围内友方单位的基础攻击力。 
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('001','4','移形换位','skill0014','W','
	瞬间和一个英雄交换位置。
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','1','1','
	造成100点的伤害。 施法间隔:10秒 施法消耗:95点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','1','2','
	造成175点的伤害。 施法间隔:10秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','1','3','
	造成250点的伤害。 施法间隔:10秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','1','4','
	造成325点的伤害。 施法间隔:10秒 施法消耗:140点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','2','1','
	减少2点的护甲和5%的攻击力，持续20秒。 施法间隔:15秒 施法消耗:40点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','2','2','
	减少3点的护甲和10%的攻击力，持续20秒。 施法间隔:15秒 施法消耗:40点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','2','3','
	减少4点的护甲和15%的攻击力，持续20秒。 施法间隔:15秒 施法消耗:40点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','2','4','
	减少5点的护甲和20%的攻击力，持续20秒。 施法间隔:15秒 施法消耗:40点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','3','1','
	增加12%的基础攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','3','2','
	增加20%的基础攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','3','3','
	增加28%的基础攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','3','4','
	增加36%的基础攻击力。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','4','1','
	施法距离600。 施法间隔:45秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','4','2','
	施法距离900。 施法间隔:45秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('001','4','3','
	施法距离1200。 施法间隔:45秒 施法消耗:200点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('002','1','弧形闪电','skill0021','C','
	释放一道会连续跳跃的闪电，造成伤害。最大跳跃距离500，伤害不随跳跃递减。施法距离：700。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('002','2','雷击','skill0022','G','
	从天空召唤一道闪电对目标造成伤害，并晕眩0.01秒。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('002','3','静电场','skill0023','F','
	每次宙斯使用任何技能，他都能通过瞬间的电击减少周围800范围内敌方英雄当前生命值一定百分比的生命。无视魔法免疫 
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('002','4','雷神之怒','skill0024','W','
	用闪电同时打击所有的敌方（不包括处于隐身状态的）英雄。（括号内为拥有Aghanim的神杖时的数值）
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','1','1','
	造成85点的伤害，跳跃5次。 施法间隔:2秒 施法消耗:65点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','1','2','
	造成100点的伤害，跳跃7次。 施法间隔:2秒 施法消耗:72点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','1','3','
	造成115点的伤害，跳跃9次。 施法间隔:2秒 施法消耗:79点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','1','4','
	造成130点的伤害，跳跃15次。 施法间隔:2秒 施法消耗:86点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','2','1','
	造成100点的伤害。 施法间隔:7秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','2','2','
	造成175点的伤害。 施法间隔:7秒 施法消耗:95点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','2','3','
	造成275点的伤害。 施法间隔:7秒 施法消耗:115点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','2','4','
	造成350点的伤害。 施法间隔:7秒 施法消耗:135点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','3','1','
	减少5%当前生命值的生命。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','3','2','
	减少7%当前生命值的生命。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','3','3','
	减少9%当前生命值的生命。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','3','4','
	减少11%当前生命值的生命。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','4','1','
	造成210点（335点）的伤害。 施法间隔:120秒 施法消耗:225点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','4','2','
	造成335点（460）的伤害。 施法间隔:120秒 施法消耗:325点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('002','4','3','
	造成460点（570）的伤害。 施法间隔:120秒 施法消耗:450点魔法
	');

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('003','1','不可侵犯','skill0031','N','
	当魅惑魔女受到攻击时，她会迷惑该单位以降低其攻击速度。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('003','2','魅诱','skill0032','C','
	操纵一个目标生物，持续120秒。如果目标不能被控制，则降低移动速度，持续7秒。施法距离：700
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('003','3','自然之助','skill0033','R','
	释放出小精灵来治疗魅惑魔女附近的友方单位。每个小精灵持续10秒，并回复10点/秒的生命。作用范围：300。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('003','4','推进','skill0034','T','
	提升魅惑魔女攻击的威力，目标距离越远造成的伤害越大。法球效果 英雄攻击 神圣（魔法）伤害
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','1','1','
	降低30%的攻击速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','1','2','
	降低50%的攻击速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','1','3','
	降低70%的攻击速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','1','4','
	降低90%的攻击速度。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','2','1','
	降低不能控制的目标10%的移动速度，施法距离700。 施法间隔:30秒 施法消耗:65点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','2','2','
	降低不能控制的目标20%的移动速度，施法距离700。 施法间隔:25秒 施法消耗:65点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','2','3','
	降低不能控制的目标30%的移动速度，施法距离700。 施法间隔:20秒 施法消耗:65点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','2','4','
	降低不能控制的目标40%的移动速度，施法距离700。 施法间隔:15秒 施法消耗:65点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','3','1','
	释放出3个小精灵。 施法间隔:45秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','3','2','
	释放出5个小精灵。 施法间隔:45秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','3','3','
	释放出7个小精灵。 施法间隔:45秒 施法消耗:155点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','3','4','
	释放出9个小精灵。 施法间隔:45秒 施法消耗:170点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','4','1','
	造成相隔距离15%的额外伤害。 施法间隔:0秒 施法消耗:55点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','4','2','
	造成相隔距离20%的额外伤害。 施法间隔:0秒 施法消耗:60点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('003','4','3','
	造成相隔距离25%的额外伤害。 施法间隔:0秒 施法消耗:65点魔法
	');



insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('004','1','波浪形态','skill0041','W','
	变体精灵将自己分解为元素，向前流动并对经过的敌方单位造成伤害。施法距离：1000。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('004','2','变体攻击','skill0042','E','
	变体精灵投掷能量打击对手，在属性构成由敏捷/力量为主时，造成伤害/晕眩并击退。 英雄
	攻击 普通伤害施法距离：600/700/800/900。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('004','3','变形','skill0043','M','
	变体精灵的物理形态能随环境自由变化。他可以牺牲一定量的力量转化为敏捷，反之亦然。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('004','4','复制','skill0044','R','
	变体精灵近乎完美地复制一个友方或者敌方的英雄。镜像拥有本体50%的攻击力，受到100%的
	伤害。变体精灵可以瞬间将镜像替换为自己。施法距离：1500 无视魔法免疫
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','1','1','
	造成100点的伤害。 施法间隔:11秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','1','2','
	造成175点的伤害。 施法间隔:11秒 施法消耗:155点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','1','3','
	造成250点的伤害。 施法间隔:11秒 施法消耗:160点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','1','4','
	造成325点的伤害。 施法间隔:11秒 施法消耗:165点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','2','1','
	当属性构成由敏捷为主时，造成30+1.4*敏捷的伤害；当属
	性构成由力量为主时，造成1.4秒的晕眩并击退。 施法间隔
	:20秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','2','2','
	当属性构成由敏捷为主时，造成60+1.8*敏捷的伤害；当属
	性构成由力量为主时，造成1.8秒的晕眩并击退。 施法间隔
	:20秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','2','3','
	当属性构成由敏捷为主时，造成90+2.2*敏捷的伤害；当属
	性构成由力量为主时，造成2.2秒的晕眩并击退。 施法间隔
	:20秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','2','4','
	当属性构成由敏捷为主时，造成120+2.6*敏捷的伤害；当属
	性构成由力量为主时，造成2.6秒的晕眩并击退。 施法间隔
	:20秒 施法消耗:100点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','3','1','
	转化2点/秒的属性点。 施法间隔:0秒 施法消耗:20点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','3','2','
	转化2点/0.5秒的属性点。 施法间隔:0秒 施法消耗:20点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','3','3','
	转化2点/0.33秒的属性点。 施法间隔:0秒 施法消耗:20点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','3','4','
	转化2点/0.25秒的属性点。 施法间隔:0秒 施法消耗:20点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','4','1','
	持续30秒。 施法间隔:80秒 施法消耗:25点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','4','2','
	持续45秒。 施法间隔:80秒 施法消耗:25点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('004','4','3','
	持续60秒。 施法间隔:80秒 施法消耗:25点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('005','1','霜冻之星','skill0051','V','
	冷冻爆炸打击目标及其周围200范围内的单位，冰冻效果减慢敌人的移动和攻击速度，持续4秒。施法距离：600
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('005','2','冰封禁制','skill0052','E','
	将目标锁在冰块之中，目标不能移动和攻击（可以使用技能），同时造成70点/秒的伤害。施法距离：500
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('005','3','辉煌光环','skill0053','R','
	提升全场内友方单位的魔法回复速度。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('005','4','极寒领域','skill0054','Z','
	在水晶室女四周635范围内造成随机的冰爆，持续4秒。（括号内为拥有Aghanim的神杖时的数值。）
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','1','1','
	造成75点的伤害，并对目标造成额外的50点的伤害。 施法间隔:11秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','1','2','
	造成100点的伤害，并对目标造成额外的100点的伤害。 施法间隔:11秒 施法消耗:130点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','1','3','
	造成125点的伤害，并对目标造成额外的125点的伤害。 施法间隔:11秒 施法消耗:155点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','1','4','
	造成125点的伤害，并对目标造成额外的175点的伤害。 施法间隔:11秒 施法消耗:185点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','2','1','
	持续1.5秒。 施法间隔:10秒 施法消耗:115点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','2','2','
	持续2秒。 施法间隔:10秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','2','3','
	持续2.5秒。 施法间隔:10秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','2','4','
	持续3秒。 施法间隔:10秒 施法消耗:150点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','3','1','
	提升0.75点/秒的魔法回复速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','3','2','
	提升1.5点/秒的魔法回复速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','3','3','
	提升2.25点/秒的魔法回复速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','3','4','
	提升3点/秒的魔法回复速度。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','4','1','
	每次冰爆造成105点（170点）的伤害。 施法间隔:150秒 施法消耗:300点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','4','2','
	每次冰爆造成170点（250点）的伤害。 施法间隔:120秒 施法消耗:400点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('005','4','3','
	每次冰爆造成250点（310点）的伤害。 施法间隔:90秒 施法消耗:600点魔法
	');



insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('006','1','风暴之锤','skill0061','T','
	向目标投掷魔法之锤，造成伤害，并晕眩2秒。施法距离：525
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('006','2','巨力挥舞','skill0062','C','
	流浪剑客在攻击中倾尽全力，对175范围内的所有敌方地面单位造成伤害。第一攻击目标受到全额伤害，其余目标受到较少伤害。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('006','3','坚韧光环','skill0063','G','
	有流浪剑客在就什么都不用怕了！增加周围700范围内的友方单位的护甲
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('006','4','神之力量','skill0064','R','
	流浪剑客这下牛b了，增加额外的攻击力，持续25秒。
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','1','1','
	造成100点的伤害。 施法间隔:8秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','1','2','
	造成150点的伤害。 施法间隔:8秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','1','3','
	造成200点的伤害。 施法间隔:8秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','1','4','
	造成250点的伤害。 施法间隔:8秒 施法消耗:140点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','2','1','
	10%的溅射伤害
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','2','2','
	20%的溅射伤害
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','2','3','
	30%的溅射伤害
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','2','4','
	40%的溅射伤害
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','3','1','
	增加2点的护甲。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','3','2','
	增加3点的护甲。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','3','3','
	增加4点的护甲。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','3','4','
	增加5点的护甲。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','4','1','
	增加100%的基础攻击力。 施法间隔:80秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','4','2','
	增加100%的基础攻击力。 施法间隔:80秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('006','4','3','
	增加100%的基础攻击力。 施法间隔:80秒 施法消耗:100点魔法
	');



insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('007','1','镜像','skill0071','R','
	创造具有攻击力的娜迦海妖的镜像来迷惑敌人。镜像受到300%的伤害，4级镜像受到350%的伤害。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('007','2','诱捕','skill0072','E','
	用一道网捕捉住目标，使其不能移动。施法距离：620/630/640/650 无视魔法免疫
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('007','3','致命一击','skill0073','C','
	有一定的几率造成额外的伤害。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('007','4','Siren之歌','skill0074','G','
	娜迦海妖迷人的歌声使附近的敌方单位进入深沉的睡眠。他们在睡眠中处于无敌状态。作用范围：1000/1500/2000
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','1','1','
	创造1个镜像，拥有本体15%的攻击力。持续时间：30秒 施法间隔:50秒 施法消耗:85点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','1','2','
	创造2个镜像，拥有本体30%的攻击力。持续时间：30秒 施法间隔:50秒 施法消耗:105点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','1','3','
	创造3个镜像，拥有本体30%的攻击力。持续时间：30秒 施法间隔:50秒 施法消耗:130点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','1','4','
	创造3个镜像，拥有本体45%的攻击力。持续时间：30秒 施法间隔:50秒 施法消耗:150点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','2','1','
	持续2秒。 施法间隔:14秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','2','2','
	持续3秒。 施法间隔:14秒 施法消耗:85点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','2','3','
	持续4秒。 施法间隔:14秒 施法消耗:95点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','2','4','
	持续5秒。 施法间隔:14秒 施法消耗:115点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','3','1','
	15%的几率造成致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','3','2','
	25%的几率造成致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','3','3','
	35%的几率造成致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','3','4','
	45%的几率造成致命一击。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','4','1','
	持续6秒。 施法间隔:120秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','4','2','
	持续7秒。 施法间隔:120秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('007','4','3','
	持续8秒。 施法间隔:120秒 施法消耗:200点魔法
	');



insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('008','1','沟壑','skill0081','F','
	撼地神牛用图腾撕裂地表，形成巨大的沟壑，8秒内不能通行，并造成伤害和晕眩。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('008','2','强化图腾','skill0082','E','
	在撼地神牛背上的图腾中注入力量，使撼地神牛的下一次攻击能造成额外的伤害。如果没有击中任何单位，效果持续14秒。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('008','3','余震','skill0083','A','
	每当撼地神牛施放技能时，都会使周围280范围内的地面震动，造成额外的伤害并晕眩。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('008','4','回音击','skill0084','C','
	撼地神牛重击地面发出震波，对周围的敌方单位造成伤害，震波相互弹射会造成更大伤害！ 作用范围：500。伤害无视魔法免疫
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','1','1','
	造成125点伤害，晕眩1秒。 施法间隔:15秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','1','2','
	造成175点伤害，晕眩1.25秒。 施法间隔:15秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','1','3','
	造成225点伤害，晕眩1.5秒。 施法间隔:15秒 施法消耗:155点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','1','4','
	造成275点伤害，晕眩1.75秒。 施法间隔:15秒 施法消耗:170点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','2','1','
	增加50%的基础攻击力。 施法间隔:7秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','2','2','
	增加100%的基础攻击力。 施法间隔:7秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','2','3','
	增加150%的基础攻击力。 施法间隔:7秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','2','4','
	增加200%的基础攻击力。 施法间隔:7秒 施法消耗:50点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','3','1','
	造成25点的额外伤害，晕眩0.3（0.6）秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','3','2','
	造成45点的额外伤害，晕眩0.7（1.4）秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','3','3','
	造成75点的额外伤害，晕眩1.2（2.4）秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','3','4','
	造成115点的额外伤害，晕眩1.5（3）秒。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','4','1','
	造成200点的伤害，震波每次弹射造成额外的35点的伤害。 施法间隔:150秒 施法消耗:145点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','4','2','
	造成275点的伤害，震波每次弹射造成额外的45点的伤害。 施法间隔:130秒 施法消耗:205点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('008','4','3','
	造成350点的伤害，震波每次弹射造成额外的65点的伤害。 施法间隔:110秒 施法消耗:265点魔法
	');




insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('009','1','烟幕','skill0091','C','
	丢出一个烟雾弹，烟雾区域内的敌方单位不能使用法术，攻击有一定几率落空，并降低25%的
	移动速度，持续6秒。施法距离：350
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('009','2','闪烁突袭','skill0092','B','
	瞬间移动到敌方单位身后对他进行攻击，造成额外的伤害。施法距离：700/800/900/1000 也可以对友方单位使用
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('009','3','背刺','skill0093','K','
	隐身刺客并不避讳这种肮脏的手段，事实上，他正精于此道。当他在背后攻击敌人时，会造成额外的伤害。伤害无视魔法免疫
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('009','4','永久隐身','skill0094','K','
	英雄在不攻击或使用技能及物品的情况下会一直保持隐身的状态。
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','1','1','
	烟雾区域扩大至250，攻击有40%几率落空。施法间隔：15秒
	 施法消耗：75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','1','2','
	烟雾区域扩大至250，攻击有50%几率落空。施法间隔：15秒
	 施法消耗：80点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','1','3','
	烟雾区域扩大至250，攻击有60%几率落空。施法间隔：15秒
	 施法消耗：85点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','1','4','
	烟雾区域扩大至250，攻击有70%几率落空。施法间隔：15秒
	 施法消耗：90点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','2','1','
	造成30点额外伤害。 施法间隔:30秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','2','2','
	造成60点额外伤害。 施法间隔:20秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','2','3','
	造成90点额外伤害。 施法间隔:10秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','2','4','
	造成120点额外伤害。 施法间隔:5秒 施法消耗:50点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','3','1','
	会附加0.25*[敏捷]的额外伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','3','2','
	会附加0.5*[敏捷]的额外伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','3','3','
	会附加0.75*[敏捷]的额外伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','3','4','
	会附加1*[敏捷]的额外伤害。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','4','1','
	恢复隐身时间3秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','4','2','
	恢复隐身时间2.25秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('009','4','3','
	恢复隐身时间1.5秒。
	');



insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('010','1','召唤熊灵','skill0101','B','
	召唤一个熊灵伙伴为自己作战。熊灵在攻击时有一定的几率缠绕目标，被缠绕的目标不能移动和攻击，持续3（9）秒。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('010','2','狂猛','skill0102','R','
	德鲁伊可以通过战斗的怒意暂时提升他或者他的熊灵伙伴的攻击速度和移动速度。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('010','3','协同','skill0103','Y','
	提升德鲁伊和他的熊灵伙伴的默契：熊灵伙伴 - 增加熊灵10点的攻击力和10的移动速度。狂
	猛 - 增加10秒的持续时间。变形术 - 增加巨熊形态时的生命值。 
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('010','4','变形术','skill0104','F','
	德鲁伊可以变形为一只棕色大熊，熊是近身攻击单位。他能够自由切换熊和人类的形态。
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','1','1','
	熊灵拥有1400点的生命。 施法间隔:180秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','1','2','
	熊灵拥有1800点的生命，同时获得回归能力。 施法间隔:160秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','1','3','
	熊灵拥有2300点的生命，同时获得回归能力粉碎击能力（对
	建筑160%的伤害），并能以16%的几率在攻击中缠绕目标。 
	施法间隔:140秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','1','4','
	熊灵拥有2700点的生命，16%的几率在攻击中缠绕目标，获
	得回归和粉碎击能力（对建筑160%的伤害）。提升33%的魔法
	抗性，并且能携带和使用物品。 施法间隔:120秒 施法消耗:
	75点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','2','1','
	提升10%的攻击速度和5%的移动速度。持续时间：10秒 施法间隔:30秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','2','2','
	提升20%的攻击速度和10%的移动速度。持续时间：10秒 施法间隔:30秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','2','3','
	提升30%的攻击速度和15%的移动速度。持续时间：10秒 施法间隔:30秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','2','4','
	提升40%的攻击速度和20%的移动速度。持续时间：10秒 施法间隔:30秒 施法消耗:50点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','3','1','
	增加熊灵10点的攻击力和10的移动速度；增加狂猛10秒的持续时间；巨熊形态得到额外的100点的生命。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','3','2','
	增加熊灵20点的攻击力和20的移动速度；增加狂猛20秒的持续时间；巨熊形态得到额外的200点的生命。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','3','3','
	增加熊灵30点的攻击力和30的移动速度；增加狂猛30秒的持续时间；巨熊形态得到额外的300点的生命。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','3','4','
	增加熊灵40点的攻击力和40的移动速度；增加狂猛40秒的持续时间；巨熊形态得到额外的400点的生命。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','4','1','
	增加250点的生命 施法间隔:0秒 施法消耗:25点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','4','2','
	增加400点的生命。熊具有战斗嚎叫（Battel Cry）的能力。 施法间隔:0秒 施法消耗:25点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('010','4','3','
	增加600点的生命。熊具有战斗嚎叫（Battel Cry）和合二为一（One）的能力。 施法间隔:0秒 施法消耗:25点魔法
	');




insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('011','1','龙破斩','skill0111','D','
	释放一道火焰，对一条直线上的敌方单位造成伤害。施法距离：600。最大距离：975
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('011','2','光击阵','skill0112','T','
	召唤一道火柱对区域内的目标造成伤害，并晕眩1.6秒。施法延迟：0.5秒。施法距离：600
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('011','3','极限速度','skill0113','E','
	大幅提升攻击速度。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('011','4','神灭斩','skill0114','G','
	向一个目标射出闪电，造成致命的伤害。（括号内为拥有Aghanim的神杖时的数值。）施法距离：600。
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','1','1','
	造成100点的伤害。 施法间隔:8.5秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','1','2','
	造成170点的伤害。 施法间隔:8.5秒 施法消耗:105点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','1','3','
	造成230点的伤害。 施法间隔:8.5秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','1','4','
	造成280点的伤害。 施法间隔:8.5秒 施法消耗:140点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','2','1','
	造成90点的伤害。 施法间隔:10秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','2','2','
	造成150点的伤害。 施法间隔:10秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','2','3','
	造成210点的伤害。 施法间隔:10秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','2','4','
	造成280点的伤害。 施法间隔:10秒 施法消耗:125点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','3','1','
	提升10%的攻击速度，持续20秒。 施法间隔:30秒 施法消耗:60点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','3','2','
	提升20%的攻击速度，持续25秒。 施法间隔:30秒 施法消耗:60点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','3','3','
	提升30%的攻击速度，持续30秒。 施法间隔:30秒 施法消耗:60点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','3','4','
	提升50%的攻击速度，持续30秒。 施法间隔:30秒 施法消耗:60点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','4','1','
	造成450点（600点）的伤害。 施法间隔:150秒 施法消耗:280点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','4','2','
	造成675点（875点）的伤害。 施法间隔:90秒 施法消耗:420点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('011','4','3','
	造成950点（1250点）的伤害。 施法间隔:55秒 施法消耗:680(640)点魔法
	');



insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('012','1','剑刃风暴','skill0121','F','
	舞起剑刃风暴，对周围的敌方单位造成伤害，持续5秒。剑圣此时对魔法免疫。作用范围：250。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('012','2','治疗守卫','skill0122','G','
	创造一个可以移动的治疗守卫，回复500范围内友方单位的生命。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('012','3','剑舞','skill0123','C','
	剑圣高超的剑术使他在攻击时有一定的几率造成2倍伤害的致命一击。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('012','4','无敌斩','skill0124','N','
	快速地在战场上移动，随机攻击附近可见的敌方单位，每剑造成150-250点的伤害，第一剑造
	成0.01秒的晕眩。顾名思义，在使用无敌斩的时候是无敌的。施法距离：450 伤害无视魔法免
	疫英雄攻击 普通伤害
	');




insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','1','1','
	造成80点/秒的伤害。持续时间：5秒 施法间隔:30秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','1','2','
	造成100点/秒的伤害。持续时间：5秒 施法间隔:30秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','1','3','
	造成120点/秒的伤害。持续时间：5秒 施法间隔:30秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','1','4','
	造成140点/秒的伤害。持续时间：5秒 施法间隔:30秒 施法消耗:110点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','2','1','
	回复500范围内友方非机械单位1%/秒的生命。持续时间：25秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','2','2','
	回复500范围内友方非机械单位2%/秒的生命。持续时间：25秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','2','3','
	回复500范围内友方非机械单位3%/秒的生命。持续时间：25秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','2','4','
	回复500范围内友方非机械单位4%/秒的生命。持续时间：25秒 施法消耗:140点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','3','1','
	10%的几率造成致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','3','2','
	18%的几率造成致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','3','3','
	26%的几率造成致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','3','4','
	36%的几率造成致命一击。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','4','1','
	瞬间出剑3次。 施法间隔:130秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','4','2','
	瞬间出剑5次。 施法间隔:120秒 施法消耗:275点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('012','4','3','
	瞬间出剑8次。 施法间隔:110秒 施法消耗:350点魔法
	');
