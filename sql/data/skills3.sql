
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('025','1','月光','skill0251','C','
	月之骑士聚集月亮的能量将它导向地面，对一个敌方单位造成伤害和短时间的晕眩。施法距离：800。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('025','2','月刃','skill0252','G','
	使Luna通过弹射可以攻击多个目标，从第二个开始，每次弹射，目标所受伤害衰减40%。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('025','3','月之祝福','skill0253','L','
	周围300范围内的友方单位因为受到了月之祝福而增加了基础远程攻击力。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('025','4','月蚀','skill0254','E','
	以月亮之名，将月光随即洒向700范围内的敌人单位。伤害取决于月光（Lucent Beam）的等级。无晕眩效果。（括号内为拥有Aghanim的神杖时的数值）。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','1','1','
	造成75点的伤害。造成0.01秒的晕眩。 施法间隔:6秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','1','2','
	造成150点的伤害。造成0.1秒的晕眩。 施法间隔:6秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','1','3','
	造成225点的伤害。造成0.3秒的晕眩。 施法间隔:6秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','1','4','
	造成300点的伤害。造成0.6秒的晕眩。 施法间隔:6秒 施法消耗:120点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','2','1','
	弹射2个目标。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','2','2','
	弹射3个目标。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','2','3','
	弹射4个目标。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','2','4','
	弹射5个目标。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','3','1','
	增加6%的基础远程攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','3','2','
	增加13%的基础远程攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','3','3','
	增加20%的基础远程攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','3','4','
	增加27%的基础远程攻击力。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','4','1','
	以月亮之名，召唤4（5）道月光，随机选择周围的目标造成伤害。 施法间隔:140秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','4','2','
	以月亮之名，召唤7（8）道月光，随机选择周围的目标造成伤害。任何单一目标不会受到多于4（5）道月光的打击。 施法间隔:140秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('025','4','3','
	以月亮之名，召唤10（12）道月光，随机选择周围的目标造成伤害。任何单一目标不会受到多于4（5）道月光的打击。 施法间隔:140秒 施法消耗:250点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('026','1','散弹','skill0261','C','
	往空中发射一发榴弹，随机向目标区域撒播爆炸性的弹片，每个弹片造成20-35点的穿刺物理伤害。施法距离：900 对建筑有效
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('026','2','爆头','skill0262','D','
	一枪爆头造成额外的伤害，并造成极短时间的晕眩。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('026','3','瞄准','skill0263','E','
	被动地增加狙击手的步枪的射程。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('026','4','暗杀','skill0264','T','
	花一定的时间瞄准，在极远的距离对目标造成非常大的伤害。施法距离：1500/2000/2500 暂时获得被瞄准单位的视野，造成0.01秒的晕眩。 视野和晕眩效果无视魔法免疫
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','1','1','
	撒播8个爆炸性的弹片。 施法间隔:13秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','1','2','
	撒播12个爆炸性的弹片。 施法间隔:13秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','1','3','
	撒播16个爆炸性的弹片。 施法间隔:13秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','1','4','
	撒播20个爆炸性的弹片。 施法间隔:13秒 施法消耗:120点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','2','1','
	25%的几率爆头，造成30点额外的伤害，晕眩0.01秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','2','2','
	30%的几率爆头，造成30点额外的伤害，晕眩0.1秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','2','3','
	35%的几率爆头，造成40点额外的伤害，晕眩0.2秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','2','4','
	40%的几率爆头，造成50点额外的伤害，晕眩0.2秒。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','3','1','
	被动地增加狙击手的步枪55的射程。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','3','2','
	被动地增加狙击手的步枪110的射程。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','3','3','
	被动地增加狙击手的步枪165的射程。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','3','4','
	被动地增加狙击手的步枪220的射程。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','4','1','
	造成355点的伤害。施法时间：1.7。 施法间隔:20秒 施法消耗:175点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','4','2','
	造成505点的伤害。施法时间：1.65。 施法间隔:15秒 施法消耗:275点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('026','4','3','
	造成655点的伤害。施法时间：1.5。 施法间隔:10秒 施法消耗:375点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('027','1','狂战士之怒','skill0271','G','
	巨魔战将化身为狂战士，不再丢掷斧头而将它们作为近战武器。他获得基础攻击（平均47<--->平均62）、基础护甲（3<--->5）、基础生命（150<--->250）、基础移动速度（280<--->300/310）和基础攻击速度（1.7<--->1.55）的提成，有一定几率通过重击造成额外伤害，但只能进行近身攻击。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('027','2','致盲','skill0272','D','
	通过一束灼热的光线将目标致盲，使他的攻击有一定的几率落空。施法距离：700
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('027','3','狂热光环','skill0273','F','
	被动地提升周围300范围内的友方单位的攻击速度。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('027','4','暴走','skill0274','R','
	巨魔战将变得蛮横狂暴，提升他的攻击速度和移动速度。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','1','1','
	攻击中有5%的几率造成额外的25点的伤害，晕眩2秒。 施法间隔:0秒 施法消耗:0点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','1','2','
	攻击中有5%的几率造成额外的50点的伤害，晕眩2秒。 施法间隔:0秒 施法消耗:0点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','1','3','
	攻击中有10%的几率造成额外的25点的伤害，晕眩2秒。 施法间隔:0秒 施法消耗:0点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','1','4','
	攻击中有10%的几率造成额外的50点的伤害，晕眩2秒，额外增加10点移动速度。 施法间隔:0秒 施法消耗:0点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','2','1','
	造成15%的几率攻击落空。持续时间：15（30）秒 施法间隔:8秒 施法消耗:20点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','2','2','
	造成24%的几率攻击落空。持续时间：15（30）秒 施法间隔:8秒 施法消耗:20点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','2','3','
	造成33%的几率攻击落空。持续时间：15（30）秒 施法间隔:8秒 施法消耗:20点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','2','4','
	成42%的几率攻击落空。持续时间：15（30）秒 施法间隔:8秒 施法消耗:20点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','3','1','
	提升5%的攻击速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','3','2','
	提升10%的攻击速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','3','3','
	提升15%的攻击速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','3','4','
	提升20%的攻击速度。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','4','1','
	提升40%的攻击速度和5%的移动速度。30秒 施法间隔:55秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','4','2','
	提升80%的攻击速度和7%的移动速度。30秒 施法间隔:55秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('027','4','3','
	提升120%的攻击速度和9%的移动速度。30秒 施法间隔:55秒 施法消耗:205点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('028','1','叉形闪电','skill0281','R','
	对前方锥面上（第一目标200范围内）的多个目标施放闪电，造成伤害。施法距离：600
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('028','2','妖术','skill0282','D','
	将敌方单位变成小动物，他将不能攻击、使用技能或物品，只拥有部分被动技能以及缓慢的移动速度。施法距离：500
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('028','3','枷锁','skill0283','E','
	使用魔法绑住一个敌方单位，造成40点/秒的伤害，目标不能移动、攻击、使用技能和物品。施法距离：400
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('028','4','群蛇守卫','skill0284','W','
	（括号内为拥有Aghanim的神杖时的数值。）召唤8（12）支固定的蛇形守卫来攻击攻击周围的敌方单位与建筑。守卫对魔法免疫。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','1','1','
	最多攻击3个目标，造成75点的伤害。 施法间隔:10秒 施法消耗:95点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','1','2','
	最多攻击4个目标，造成150点的伤害。 施法间隔:10秒 施法消耗:105点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','1','3','
	最多攻击6个目标，造成220点的伤害。 施法间隔:10秒 施法消耗:135点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','1','4','
	最多攻击7个目标，造成300点的伤害。 施法间隔:10秒 施法消耗:160点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','2','1','
	持续1秒。 施法间隔:13秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','2','2','
	持续2秒。 施法间隔:13秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','2','3','
	持续3秒。 施法间隔:13秒 施法消耗:170点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','2','4','
	持续4秒。 施法间隔:13秒 施法消耗:200点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','3','1','
	持续2.5秒。 施法间隔:16秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','3','2','
	持续3.25秒。 施法间隔:16秒 施法消耗:130点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','3','3','
	持续4秒。 施法间隔:16秒 施法消耗:155点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','3','4','
	持续4.75秒。 施法间隔:13秒 施法消耗:185点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','4','1','
	每支守卫拥有39-43点的攻击力 施法间隔:100秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','4','2','
	每支守卫拥有54-58点的攻击力 施法间隔:80秒 施法消耗:350点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('028','4','3','
	每支守卫拥有69-73点的攻击力 施法间隔:60秒 施法消耗:600点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('029','1','粘稠鼻液','skill0291','G','
	用鼻涕包裹一个敌方单位，降低他的移动速度，减少他的护甲。施法距离：600
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('029','2','刺针扫射','skill0292','R','
	将身上的尖刺发射出去，对650范围内的地方单位造成伤害。最近10秒内每次受到刺针扫射的打击会受到30点额外的伤害，可以叠加。作用范围：650 伤害无视魔法免疫英雄攻击 普通伤害
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('029','3','刚毛后背','skill0293','K','
	刚背兽的后背有刚毛覆盖，减少他所受到的伤害。刚毛后背每吸收300点伤害会自动释放一次刺针扫射。不能减少来自建筑的伤害。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('029','4','战意','skill0294','O','
	刚背兽在使用技能的时候会进入狂暴的状态，大幅提升攻击速度和移动速度，持续10秒，效果最多可以叠加4次。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','1','1','
	减少1点的护甲，降低20%的移动速度。再次施放，累积减少1点的护甲，降低3%的移动速度。效果可累积4次，持续5秒。 施法间隔:1.5秒 施法消耗:30点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','1','2','
	减少1点的护甲，降低20%的移动速度。再次施放，累积减少1点的护甲，降低6%的移动速度。效果可累积4次，持续5秒。 施法间隔:1.5秒 施法消耗:30点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','1','3','
	减少2点的护甲，降低20%的移动速度。再次施放，累积减少2点的护甲，降低6%的移动速度。效果可累积4次，持续5秒。 施法间隔:1.5秒 施法消耗:30点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','1','4','
	减少2点的护甲，降低20%的移动速度。再次施放，累积减少2点的护甲，降低12%的移动速度。效果可累积4次，持续5秒。 施法间隔:1.5秒 施法消耗:30点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','2','1','
	20点的直接伤害，30点的叠加伤害。刺针扫射的叠加上限为180。 施法间隔:3秒 施法消耗:35点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','2','2','
	40点的直接伤害，30点的叠加伤害。刺针扫射的叠加上限为180。 施法间隔:3秒 施法消耗:35点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','2','3','
	60点的直接伤害，30点的叠加伤害。刺针扫射的叠加上限为180。 施法间隔:3秒 施法消耗:35点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','2','4','
	80点的直接伤害，30点的叠加伤害。刺针扫射的叠加上限为180。 施法间隔:3秒 施法消耗:35点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','3','1','
	减少10%来自背后的伤害，5%来自侧面的伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','3','2','
	减少20%来自背后的伤害，10%来自侧面的伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','3','3','
	减少30%来自背后的伤害，15%来自侧面的伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','3','4','
	减少40%来自背后的伤害，20%来自侧面的伤害。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','4','1','
	战意提升30%的攻击速度和5%的移动速度，每次叠加提升5%的攻击速度和1%的移动速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','4','2','
	战意提升40%的攻击速度和7%的移动速度，每次叠加提升10%的攻击速度和2%的移动速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('029','4','3','
	战意提升50%的攻击速度和10%的移动速度，每次叠加提升15%的攻击速度和3%的移动速度。
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('030','1','雷霆一击','skill0301','C','
	重击地面，对周围的敌方地面单位造成伤害，降低他们的攻击速度和移动速度，持续4.25（8）秒。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('030','2','醉酒云雾','skill0302','D','
	用酒精迷醉敌方单位，降低其移动速度，并使攻击有一定几率落空。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('030','3','醉拳','skill0303','B','
	以一定的几率闪避攻击，同时也能以一定的几率造成1.8倍伤害的致命一击。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('030','4','元素分离','skill0304','R','
	熊猫酒仙分身为3个具有特殊能力的元素战士。在分身期间，只要任意一个元素战士还存活，熊猫酒仙就能从中复生（复生顺序：土--->风--->火）。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','1','1','
	造成100点的伤害，降低25%的攻击速度和移动速度。 施法间隔:12秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','1','2','
	造成175点的伤害，降低35%的攻击速度和移动速度。 施法间隔:12秒 施法消耗:105点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','1','3','
	造成225点的伤害，降低45%的攻击速度和移动速度。 施法间隔:12秒 施法消耗:130点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','1','4','
	造成300点的伤害，降低55%的攻击速度和移动速度。 施法间隔:12秒 施法消耗:150点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','2','1','
	降低10%的移动速度，造成45%的几率攻击落空。 施法间隔:12秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','2','2','
	降低14%的移动速度，造成55%的几率攻击落空。 施法间隔:12秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','2','3','
	降低18%的移动速度，造成65%的几率攻击落空。 施法间隔:12秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','2','4','
	降低22%的移动速度，造成75%的几率攻击落空。 施法间隔:12秒 施法消耗:50点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','3','1','
	有6%的几率闪避攻击，5%的几率造成1.8倍伤害的致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','3','2','
	有12%的几率闪避攻击，10%的几率造成1.8倍伤害的致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','3','3','
	有18%的几率闪避攻击，15%的几率造成1.8倍伤害的致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','3','4','
	有24%的几率闪避攻击，20%的几率造成1.8倍伤害的致命一击。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','4','1','
	分身为3个较强的元素战士。持续时间15秒。 施法间隔:180秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','4','2','
	分身为3个更强的元素战士。持续时间17秒。 施法间隔:150秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('030','4','3','
	分身为3个非常强的元素战士。持续时间19秒。 施法间隔:120秒 施法消耗:175点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('031','1','马蹄践踏','skill0311','F','
	猛击地面，对敌方地面单位造成伤害，并使他们晕眩。作用范围：315
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('031','2','双刃剑','skill0312','D','
	半人马酋长聚集起内在的力量进行一次威力巨大的攻击，同时对自己和目标造成伤害，并造成目标0.01秒的晕眩。施法距离：150
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('031','3','反击','skill0313','R','
	半人马酋长能迅速回敬任何攻击他的敌人。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('031','4','刚毅不屈','skill0314','G','
	半人马酋长硕大的身躯可以承受相当大的打击。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','1','1','
	造成100点的伤害，晕眩1.25秒。 施法间隔:15秒 施法消耗:80点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','1','2','
	造成152点的伤害，晕眩1.75秒。 施法间隔:15秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','1','3','
	造成200点的伤害，晕眩2.25秒。 施法间隔:15秒 施法消耗:115点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','1','4','
	造成250点的伤害，晕眩2.75秒。 施法间隔:15秒 施法消耗:130点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','2','1','
	对双方造成175点的伤害。 施法间隔:25秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','2','2','
	对双方造成250点的伤害。 施法间隔:25秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','2','3','
	对双方造成325点的伤害。 施法间隔:25秒 施法消耗:105点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','2','4','
	对双方造成400点的伤害。 施法间隔:25秒 施法消耗:120点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','3','1','
	对近身/远程攻击者造成12.5/12.5点的反馈伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','3','2','
	对近身/远程攻击者造成25/25点的反馈伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','3','3','
	对近身/远程攻击者造成37.5/37.5点的反馈伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','3','4','
	对近身/远程攻击者造成50/50点的反馈伤害。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','4','1','
	永久增加他12点的力量。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','4','2','
	永久增加他24点的力量。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('031','4','3','
	永久增加他36点的力量。
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('032','1','投掷飞镖','skill0321','T','
	赏金猎人向目标投掷一个飞镖，造成伤害以及0.01秒的晕眩。施法距离：650。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('032','2','忍术','skill0322','J','
	有一定的几率闪避攻击，并且有15%的几率造成额外的伤害。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('032','3','疾风步','skill0323','W','
	使英雄在一段时间内隐身，提升英雄的移动速度。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('032','4','追踪术','skill0324','R','
	追踪一个敌方英雄30秒或直到他死去，期间你得到该单位的视野以及20%的移动速度提升。如果你杀死了一个英雄，你将得到额外的金钱奖励（被动，无需使用此技能也能得到奖励）。无视魔法免疫。 施法距离：750/950/1150
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','1','1','
	造成100点的伤害。 施法间隔:10秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','1','2','
	造成200点的伤害。 施法间隔:10秒 施法消耗:115点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','1','3','
	造成250点的伤害。 施法间隔:10秒 施法消耗:135点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','1','4','
	造成325点的伤害。 施法间隔:10秒 施法消耗:155点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','2','1','
	有5%几率的闪避攻击，15%的几率造成1.25倍伤害的致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','2','2','
	有10%几率的闪避攻击，15%的几率造成1.5倍伤害的致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','2','3','
	有15%几率的闪避攻击，15%的几率造成1.75倍伤害的致命一击。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','2','4','
	有20%几率的闪避攻击，15%的几率造成2倍伤害的致命一击。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','3','1','
	30点的偷袭伤害，1秒的进入隐身时间。持续时间15秒。 施法间隔:20秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','3','2','
	60点的偷袭伤害，0.75秒的进入隐身时间。持续时间20秒。 施法间隔:20秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','3','3','
	90点的偷袭伤害，0.5秒的进入隐身时间。持续时间25秒。 施法间隔:20秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','3','4','
	120点的偷袭伤害，0.36秒的进入隐身时间。持续时间30秒。 施法间隔:20秒 施法消耗:50点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','4','1','
	每杀死1个英雄得到75额外的金钱奖励。降低目标1点的护甲。 施法间隔:10秒 施法消耗:70点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','4','2','
	每杀死1个英雄得到150额外的金钱奖励。降低目标3点的护甲。 施法间隔:7秒 施法消耗:60点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('032','4','3','
	每杀死1个英雄得到225额外的金钱奖励。降低目标5点的护甲。 施法间隔:5秒 施法消耗:50点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('033','1','火焰气息','skill0331','F','
	向面前锥形范围吐出一道火焰，对敌方单位造成伤害。施法距离：500。最大距离：650
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('033','2','神龙摆尾','skill0332','T','
	龙骑士在近身距离猛击对手，造成伤害并晕眩。施法距离：150
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('033','3','龙族血统','skill0333','D','
	龙骑士身上所流的龙族之血使他有更高的生命回复速度和护甲。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('033','4','真龙形态','skill0334','R','
	龙骑士化身为龙，获得特殊能力。龙是空中单位，拥有远程攻击能力，射程500。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','1','1','
	造成75点的伤害。 施法间隔:12秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','1','2','
	造成150点的伤害。 施法间隔:12秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','1','3','
	造成225点的伤害。 施法间隔:12秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','1','4','
	造成300点的伤害。 施法间隔:12秒 施法消耗:130点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','2','1','
	造成25点的伤害，晕眩2.5秒。 施法间隔:9秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','2','2','
	造成50点的伤害，晕眩2.75秒。 施法间隔:9秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','2','3','
	造成75点的伤害，晕眩3秒。 施法间隔:9秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','2','4','
	造成100点的伤害，晕眩3.25秒。 施法间隔:9秒 施法消耗:100点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','3','1','
	提高1点/秒的生命回复速度和2点的护甲。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','3','2','
	提高2点/秒的生命回复速度和4点的护甲。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','3','3','
	提高3点/秒的生命回复速度和6点的护甲。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','3','4','
	提高4点/秒的生命回复速度和8点的护甲。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','4','1','
	化身为绿龙，基础移动速度300，攻击带有毒性效果。持续时间：60秒。 施法间隔:50秒 施法消耗:115点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','4','2','
	化身为赤龙，基础移动速度325，攻击带有100%溅射效果。持续时间：60秒。 施法间隔:50秒 施法消耗:115点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('033','4','3','
	化身为碧龙，基础移动速度350，攻击带有100%的溅射效果和霜冻效果。持续时间：60秒。 施法间隔:50秒 施法消耗:115点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('034','1','法力损毁','skill0341','R','
	每次攻击燃烧目标一定量的魔法，并造成额外的相当于燃烧的魔法值60%的伤害。法球效果
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('034','2','闪烁','skill0342','B','
	短距离瞬间移动的能力让英雄在战场出入自如。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('034','3','法术护盾','skill0343','D','
	敌法师用守护魔法来抵御负面法术的攻击。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('034','4','法力虚空','skill0344','V','
	法力虚空对目标所造成的伤害取决于目标所缺失的魔法值（即 最大魔法值-当前魔法值），同时造成极短时间的晕眩。施法距离：600。晕眩效果无视魔法免疫
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','1','1','
	每次攻击消去目标16点的魔法。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','1','2','
	每次攻击消去目标32点的魔法。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','1','3','
	每次攻击消去目标48点的魔法。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','1','4','
	每次攻击消去目标64点的魔法。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','2','1','
	最小距离200，最大距离1000。 施法间隔:12秒 施法消耗:60点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','2','2','
	最小距离200，最大距离1075。 施法间隔:9秒 施法消耗:60点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','2','3','
	最小距离200，最大距离1150。 施法间隔:7秒 施法消耗:60点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','2','4','
	最小距离200，最大距离1150。 施法间隔:5秒 施法消耗:60点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','3','1','
	提升10%的魔法抗性。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','3','2','
	提升20%的魔法抗性。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','3','3','
	提升30%的魔法抗性。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','3','4','
	提升40%的魔法抗性。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','4','1','
	对目标所造成的伤害为目标所缺失的魔法值（即 最大魔法值-当前魔法值）*0.6，同时造成极短时间的晕眩。 施法间隔:120秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','4','2','
	对目标所造成的伤害为目标所缺失的魔法值（即 最大魔法值-当前魔法值）*0.85，同时造成极短时间的晕眩。 施法间隔:100秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('034','4','3','
	对目标所造成的伤害为目标所缺失的魔法值（即 最大魔法值-当前魔法值）*1.1，同时造成极短时间的晕眩。 施法间隔:80秒 施法消耗:275点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('035','1','霜冻之箭','skill0351','R','
	在英雄的普通攻击中附上霜冻的效果，降低目标的攻击速度和移动速度，持续1.5（7）秒。 法球效果
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('035','2','沉默魔法','skill0352','E','
	阻止所有作用范围内的敌方单位使用技能。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('035','3','强击光环','skill0353','T','
	能增加附近900范围内的友方单位基础远程攻击力的光环。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('035','4','射手天赋','skill0354','M','
	黑暗游侠的技艺变的更加精湛，增加他的敏捷值。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','1','1','
	降低目标5%的攻击速度和10%的移动速度。 施法间隔:0秒 施法消耗:12点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','1','2','
	降低目标10%的攻击速度和20%的移动速度。 施法间隔:0秒 施法消耗:12点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','1','3','
	降低目标15%的攻击速度和30%的移动速度。 施法间隔:0秒 施法消耗:12点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','1','4','
	降低目标20%的攻击速度和40%的移动速度。 施法间隔:0秒 施法消耗:12点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','2','1','
	持续3秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','2','2','
	持续4秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','2','3','
	持续5秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','2','4','
	持续6秒。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','3','1','
	增加7%的基础远程攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','3','2','
	增加14%的基础远程攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','3','3','
	增加21%的基础远程攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','3','4','
	增加28%的基础远程攻击力。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','4','1','
	永久增加15的敏捷。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','4','2','
	永久增加30的敏捷。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('035','4','3','
	永久增加45的敏捷。
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('036','1','洗礼','skill0361','R','
	立刻恢复一个友方单位的生命值，并对其附近250范围内的敌方单位造成伤害。施法距离：400 伤害无视魔法免疫法术攻击 神圣伤害
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('036','2','驱逐','skill0362','E','
	创造一个神圣守护使目标不受绝大部分魔法的影响。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('036','3','退化光环','skill0363','U','
	退化光环大幅降低附近300范围内敌方单位的移动速度。无视魔法免疫
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('036','4','守护天使','skill0364','G','
	使周围的友方单位对物理攻击免疫（增加1000点的护甲）并且提高25点/秒的生命回复速度。作用范围：1000
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','1','1','
	立即恢复一个友方单位90点的生命并对其附近的敌方单位造成90点的伤害。 施法间隔:12秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','1','2','
	立即恢复一个友方单位180点的生命并对其附近的敌方单位造成180点的伤害。 施法间隔:12秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','1','3','
	立即恢复一个友方单位270点的生命并对其附近的敌方单位造成270点的伤害。 施法间隔:12秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','1','4','
	立即恢复一个友方单位360点的生命并对其附近的敌方单位造成360点的伤害。 施法间隔:12秒 施法消耗:160点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','2','1','
	持续5秒。 施法间隔:20秒 施法消耗:60点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','2','2','
	持续10秒。 施法间隔:20秒 施法消耗:70点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','2','3','
	持续15秒。 施法间隔:20秒 施法消耗:80点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','2','4','
	持续20秒。 施法间隔:20秒 施法消耗:90点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','3','1','
	降低7%的移动速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','3','2','
	降低14%的移动速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','3','3','
	降低21%的移动速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','3','4','
	降低28%的移动速度。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','4','1','
	持续5秒。 施法间隔:150秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','4','2','
	持续7秒。 施法间隔:150秒 施法消耗:175点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('036','4','3','
	持续9秒。 施法间隔:150秒 施法消耗:250点魔法
	');
