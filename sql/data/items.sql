insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('01','001','坚韧球','item001','0','
	+10 点的攻击力，+5 点每秒的生命回复速度，+125% 的魔法回复速度。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('01','002','回复头巾','item002','225','
	+2 点的所有属性
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('01','003','玄冥盾牌','item003','200','
	2 点的所有属性，+5 点的护甲
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('01','004','圣殿指环','item004','0','
	+6 点的攻击力
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('01','005','远行鞋','item005','2200','
	+90 的移动速度,提升效果不能和速度之靴，假腿或者其他远行鞋叠加。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('01','006','假腿','item006','400','
	+65 的移动速度，+35% 的攻击速度。+10所选属性，点击假腿可以切换,提升效果不
	能和速度之靴，远行鞋或者其他假腿叠加。同时只能拥有一个假脚。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('01','007','迈达斯之手','item007','1400','
	+30% 的攻击速度，使用后 （消耗75点的魔法，冷却时间100秒）: 炼制（最高可将
	5级野怪转为金钱，施法距离 600）。修补匠不能刷新此物品。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('01','008','空明杖','item008','0','
	+6 点的智力，+15 点的攻击力，+75% 的魔法回复速度，+10% 的攻击速度。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('01','009','护腕','item009','175','
	+3 点的所有属性，+3 点的力量。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('01','010','幽灵系带','item010','125','
	+3 点的所有属性，+3 点的敏捷。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('01','011',无用挂件'','item011','150','
	+3 点的所有属性，+3 点的智力。
	');



insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('02','012','散华','item012','800','
	+16 点的力量，+10 点的攻击力.
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('02','013','夜叉','item013','800','
	+16 点的敏捷，+10% 的移动速度，+15% 的攻击速度。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('02','014','碎骨锤','item014','1450','
	+3 点的力量，+30 点的攻击力。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('02','015','刃甲','item015','0','
	+22 点的攻击力，+5 点的护甲，+10 点的智力。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('02','016','漩涡','item016','1400','
	+6 点的敏捷，+25 点的攻击力。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('02','017','散失之刃','item017','850','
	+22 点的敏捷，+6 点的智力。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('02','018','支配头盔','item018','900','
	+20 点的攻击力，+5 点的护甲, 15% 吸血（法球效果）。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('02','019','疯狂面具','item019','1050','
	17% 吸血（法球效果）。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('02','020','Eul的神圣法杖','item020','600','
	+16 点的智力，+100% 点/秒的魔法回复速度，+20移动速度。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('02','021','振魂石','item021','0','
	+450 的生命上限，+400 的魔法上限，+1 点/秒的生命回复速度，+10% 点/秒的魔法回复速度。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('02','022','梅肯斯姆','item022','900','
	+5 点的所有属性，+5 点的护甲。
	');








insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('03','023','散夜对剑','item023','500','
	+16 点的力量，+16 点的敏捷，+12 点的攻击力，+12% 的移动速度，+15% 的攻击
	速度，法球效果: 加强残废 （15% 的几率，-15% 的攻击速度-30% 的移动速度 持
	续4秒）。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('03','024','黯灭','item024','1200','
	+60 点的攻击力，法球效果: 降低对方 6点的 护甲，持续时间5秒。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('03','025','狂战斧','item025','0','
	+65 点的攻击力，+6 点/秒的生命回复速度，+150% 点/秒的魔法回复速度，效果:
	溅射攻击 （35%的伤害，225 有效范围，仅近身单位使用时有效）。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('03','026','水晶剑','item026','500','
	+35 点的攻击力，效果: 致命一击 （10% 的几率, 1.75倍伤害）。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('03','027','黑皇杖','item027','1300','
	+10 点的力量，+24 点的攻击力。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('03','028','幻影斧','item028','1400','
	+30 点的敏捷，+6 点的智力，+250 点的生命上限，法球效果: 反馈（每次攻击消
	去任何单位40 点的魔法，同时造成等量伤害）。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('03','029','不朽之守护','item029','0','
	效果:重生，死亡后3秒原地满血满魔复活，不可丢弃，产生后10分钟内不被使用则
	消失。
	');
insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('03','030','洛萨之锋','item030','1100','
	+15% 的攻击速度，+38 点的攻击力。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('03','031','达贡之神力','item031','1300','
	+3 点的所有属性，+12/14/16/18/20 点的智力，+9 点的攻击力。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('03','032','死灵书','item032','1300','
	+6/10/14 点的力量，+15/21/24 点的智力。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('03','033','林肯法球','item033','1325','
	+15 点的所有属性，+6 点/秒的生命回复速度，+150% 点/秒的魔法回复速度。
	');




insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('04','034','圣剑','item034','0','
	+200 点的攻击力。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('04','035','大炮','item035','1200','
	+75 点的攻击力，效果: 致命一击 （20% 的几率, 2.2倍伤害）。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('04','036','金箍棒','item036','0','
	+80 点的攻击力，+15% 的攻击速度，效果: 猛击 （35% 的几率造成0.01秒的眩晕，
	+100 点的附加伤害）。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('04','037','辉耀','item037','1525','
	+60 点的攻击力，8% 的闪避，效果: 辉耀 （在550范围内造成35点/秒的伤害）
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('04','038','魔龙之心','item038','1200','
	+35 点的力量，+300 点的生命上限，+1% 生命上限/秒的生命回复速度。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('04','039','撒旦之邪力','item039','1100','
	+25 点的力量，+5 点的护甲, 25% 吸血 （法球效果）
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('04','040','斯嘉蒂之眼','item040','1250','
	+25 点的所有属性，+200 点的生命上限，+150 点的魔法上限，法球效果: 霜冻攻
	击（攻击可以造成3秒的冰冻效果，-20% 的攻击速度 & -30% 的移动速度）。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('04','041','蝴蝶','item041','1800','
	+30 点的敏捷，+30 点的攻击力，+30% 的攻击速度, 30% 的闪避。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('04','042','Aghanim的神杖','item042','0','
	+30 点的智力，+500 点的生命上限，+500 点的魔法上限，效果: 提升 秀逗魔导师
	，众神之王，先知，遗忘法师，水晶室女，暗影撒满，痛苦女王，剧毒术士，巫妖，
	受折磨的灵魂，恶魔巫师，月之骑士，死灵法师、食人鬼魔法师、巫医以及末日使者
	的大招施法效果。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('04','043','刷新球','item043','1875','
	+40 点的攻击力，+5 点/秒的生命回复速度，+150% 点/秒的魔法回复速度。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('04','044','Guinsoo的邪恶镰刀','item044','0','
	+35 点的智力，+10 点的敏捷，+10 点的力量，+200% 的魔法回复速度。
	');





insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','045','先锋盾','item045','0','
	+300 点的生命上限，+5 点/秒的生命回复速度，效果: 65% 的几率 抵消40点的伤
	害 （受到最小伤害值是0）。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','046','秘法指环','item046','525','
	+3 点的护甲， +300 点的魔法上限。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','047','雷神之锤','item047','0','
	+35 点的敏捷 +35 点的攻击力，法球效果: 连环闪电 （20% 的释放几率，200点
	的恒定伤害，最多击中3个单位）
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','048','乌鸦','item048','200','
	只能使用1 次。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','049','吸血鬼的祭品','item049','300','
	光环:攻击光环（+15%攻击力） （900 有效范围），光环: 辉煌光环 （+0.8点/秒
	的魔法回复速度）（1100 有效范围），光环: 专注光环（+5 点的护甲）（1100有
	效范围），光环: 吸血光环（16% 的生命偷取）（1100 有效范围）。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','050','强袭装甲','item050','2000','
	+10 点的护甲， +40% 的攻击速度，光环: 荒芜光环 （敌方单位-5 点护甲）（900
	有效范围），光环: 专注光环（+5 点的护甲）（900 有效范围），光环: 狂热光环
	（+15% 的攻击速度）（900 有效范围）。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','051','血精石','item051','0','
	+450 点的生命， +400 点的魔法，+200% 的魔法回复速度，+6 点/秒的生命回复速
	度，效果: 血焰灌输 击杀对方英雄充能1，死亡以后损失能量1。每点能量提供额外
	的2点/秒的生命和魔法恢复速度。效果: 救赎 英雄死亡时恢复英雄1700范围内的友
	方单位450点生命和400点魔法，并在死亡地点留下图腾标记，拥有视野并分享经验。
	减少40%的死亡损失金钱和20%的复活时间。能量为0时效果减半。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','052','挑战头巾','item052','0','
	+30% 魔法抗性，+8点/秒的生命回复速度。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','053','食尸鬼王的臂章','item053','900','
	+9 点攻击力，+5 点护甲，15% 的攻击速度，+3 点/秒的生命回复速度。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','054','希瓦的守护','item054','600','
	+30 点的智力，+15 点护甲 光环: 霜冷光环（敌方单位-25% 的攻击速度）（1000
	有效范围）。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','055','紫怨','item055','0','
	+40 点攻击力，+20 点的智力，40% 的攻击速度，+225% 的魔法回复速度。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('05','056','相位鞋','item056','0','
	+16 点攻击力，+7 点护甲，+70 的移动速度。
	');





insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','057','食人魔力量手套','item057','150','
	+3 点的力量。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','058','敏捷便鞋','item058','150','
	+3 点的敏捷。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','059','智力斗篷','item059','150','
	3 点的智力。
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','060','铁树枝干','item060','53','
	+1 点的所有属性
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','061','巨人力量腰带','item061','450','
	+6 点的力量
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','062','精灵皮靴','item062','450','
	+6 点的敏捷
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','063','法师长袍','item063','450','
	+6 点的智力
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','064','贵族圆环','item064','185','
	+2 点的所有属性
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','065','食人魔之斧','item065','1000','
	+10 点的力量
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','066','欢欣之刃','item066','1000','
	+10 点的敏捷
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','067','魔力法杖','item067','1000','
	+10 点的智力
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('06','068','极限法球','item068','2100','
	+10点的所有属性
	');




insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('07','069','','item069','','
	
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('07','07','','item07','','
	
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('07','07','','item07','','
	
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('07','07','','item07','','
	
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('07','07','','item07','','
	
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('07','07','','item07','','
	
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('07','07','','item07','','
	
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('07','07','','item07','','
	
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('07','07','','item07','','
	
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('07','07','','item07','','
	
	');

insert into item(shop_no, item_no, item_name, item_pic, item_price, item_depict)
	values('07','07','','item07','','
	
	');


insert into item_skill(shop_no, item_no, item_skill_no, item_skill_name, item_skill_depict)
	values('01','001','1','','');