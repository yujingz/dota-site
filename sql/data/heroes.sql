insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','001','复仇之魂','hero001','
	复仇之魂出身于典狱长一族。在被她信任的Mortred背叛和杀害以后，
	她祈求月神Elune给她一次复仇的机会。现在，她的躯体已经死亡，但
	是她无情的灵魂却留了下来。她的存在是盟友的榜样，而她复仇的决心
	则带给敌人恐慌。通过运用灵界的力量，她能够使用纯净的能量击晕对
	手，同时能和其他英雄交换位置。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','002','众神之王','hero002','
	由于参与到近卫军团和天灾军团的世俗争斗中，宙斯（Zeus）被众神流
	放到人间。虽然他的力量因此被大幅削弱，但你仍然不能小看他对于闪
	电的运用。同时擅长杀伤单一和多个目标，非常强大的伤害性法师英雄。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','003','魅惑魔女','hero003','
	虽然所有森林女神都是赛那留斯的女儿，但魅惑魔女却是赛那留斯的第一
	个女儿，也是他最疼爱的一个。因为这层关系，Aiushtha能从赛那留斯那
	里借取更多的力量。之所以这么说，一是因为她能控制那些害羞的小精灵
	去医治自己的盟军，二是因为她能够让一个敌人服从近卫军团的意愿。相
	对于肉搏来说，她更喜欢射击，所以她亲手打造了一把长矛，能对越远的
	敌人造成越大的伤害。她不可侵犯，那些企图杀死她的敌人将会发现她会
	非常轻松地从他们的面前跳开，真的非常轻松。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','004','变体精灵','hero004','
	变体精灵，大自然的杰作，进化的巅峰，能将他液态的身体化作巨大的波
	浪，一边移动一边对流经的敌人造成伤害。他可以改变自身的组成，使自
	己变得更有力又或是更灵活。通过调整力量和敏捷的平衡，他可以选择震
	晕又或是伤害敌人。而真正让他如此非比寻常的地方在于他可以克隆一个
	选定的单位，在敌人不得不面对自身力量而惊慌失措时碾碎他们。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','005','水晶室女','hero005','
	经过寒冰泉巨魔冰冻魔法师的长年训练，水晶室女善于运用令人叹为观止
	的禁制魔法，她的绝技是异常强大的范围杀伤技能。水晶室女称得上是近
	卫军团所拥有的最为强大的法师之一。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','006','流浪剑客','hero006','
	伟大的秩序之神的信徒，圣骑士流浪剑客发誓要维护芸芸众生的权力。作
	为骑士和暗夜精灵的后代，流浪剑客一出生就被放逐，从此在独处中冥想
	和训练自己。在流浪的过程中他决定了要保护无辜的人免受邪恶的侵犯。
	带着受祝福的大剑“正义”，以神之力量击倒对手，任何流浪剑客所看到的
	不公正都将感受到他的愤怒。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','007','娜迦海妖','hero007','
	娜迦海妖喜欢跨出大海的边界，找寻新的事物，作为娜迦族的一员，她永
	远过着流浪的生活。在旅途中她遇见了一个想成为剑圣的年轻半兽人并同
	他共坠爱河。即使年轻的半兽人自己也没有什么实战经验，他还是教授了
	娜迦海妖许多半兽人故老相传的战斗技巧。在他们一起度过的甜蜜时光中，
	她还从当地的骑兵那里学会了如何设置陷阱。然而一切的幸福都随着天灾
	军团的到来化为泡影，她的爱人在她面前被杀害，她使用娜迦族的沉睡魔
	法得以逃脱。现在，她把她的一切投入到对抗天灾军团的战斗中，为了她
	心中永不会磨灭的爱！
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','008','撼地神牛','hero008','
	为了保护世界之树----自然界所有生命的源泉，撼地神牛加入到近卫军团
	中。凭借对大地的熟悉，撼地神牛能够熟练运用许多相关法术，他运用图
	腾进行攻击造成的伤害也非常惊人。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','009','隐形刺客','hero009','
	作为赛特斯一族的继承人，隐形刺客接受了族内最强勇士的严格训练。
	然而，燃烧军团的力量腐蚀了他许多族人的心智，使他们成为森林中的
	行尸走肉。他苦练技艺，发誓要向天灾军团复仇。他运用身材小巧的优
	势隐匿自己的行踪，从敌人背后发动攻击。通过烟雾的掩护，敌人根本
	连他的衣角都沾不到。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','010','德鲁伊','hero010','
	在那次剧烈的冲突中，德鲁伊是他的部族中仅存的一个。当感觉到居住的
	地方面临着毁灭的时候，他的族人们将他们的孩子变成一头熊，并放至山
	林。德鲁伊渐渐成长，并且变得和森林里那些栖息在他家的熊一样强壮，
	凶猛。他能通过德鲁依的力量进入一个狂热的状态，甚至能呼唤强大的熊
	伙伴来援助他。被先知唤醒他的精灵血统后，德鲁伊重新变回精灵形态，
	并将他的野性灵魂带到战场中去。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','011','秀逗魔导师','hero011','
	秀逗魔导师是一位非常强大的法师，她以打击盗贼（其实是为了他们的财
	宝）和屠龙（一不小心毁灭了边上的城市）而著称。她能够非常熟练地投
	掷火球打击对手，当然她还掌握了诸如龙破斩、神灭斩等更具破坏力的法
	术。秀逗魔导师承诺帮助近卫军团摧毁冰封王座，当然最主要是为了之后
	巨额的报酬——强力的武器和非常多的钱。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('1','012','剑圣','hero012','
	他出剑似舞蹈般灵动轻盈，无论多坚固的护甲都已悄然破碎：他行动如诗
	歌般优雅从容，无论多强的敌人弹指间灰飞烟灭。是的，剑圣追求完美，
	追求人剑合一的至高境界。成为那无人能敌的剑圣便是他的宿命。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','013','沉默术士','hero013','
	作为魔法王国的一员，沉默术士是保卫着大陆完整性的最强精灵战士之一
	。他精通剑刃投掷这一远古技艺，并将其与血精灵神秘魔法中的爆炸特性
	相结合，从而起到破坏敌人的魔法能量的效果。他能将他的法力附加到剑
	刃上，令其造成更多的伤害，并在每次击杀后获得更多的智慧。他还可以
	浓缩自己的能量，造成敌人魔法储藏的混乱，令他们无法施法。誓死阻挡
	天灾军团和他们的毁灭魔法，是他毕生奋斗的目标。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','014','树精卫士','hero014','
	在燃烧军团入侵的时候，树精卫士还只是棵幼小的树苗，而如今，他已成
	为经历了无穷岁月的树人智者。经过数千年的成长，现在他正在为近卫军
	团贡献他的力量。借助伟大的森林，树精卫士可以运用森林之力隐藏他的
	盟友，他也可以制造藤蔓护甲给予己方以充分的保护，他还可以用枝蔓缠
	绕住对手，然后挥舞他沉重的树干将对手置于死地。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','015','谜团','hero015','
	当一颗即将陨落的恒星进入一片充满着元素魔法的区域之后，一种最危险
	的生命体便产生了。这种生命体的名字叫做谜团。出于怨恨与饥饿，他将
	数百个世界吸入虚空。出于某种未知的原因，谜团现在效忠于近卫军团，
	并将他的仇恨转移到天灾军团身上。他能够在自己盟军的身上开启空间传
	送门，让他的仆从进入这个世界。在战斗中，他操纵着虚空与暗影的力量，
	但他的最强之处还是释放黑洞，将所有靠近黑洞的敌人吸入无底的深渊。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','016','光之守卫','hero016','
	光之守卫由于在魔法师战争中扮演了叛徒的角色而被放逐到灵界。近卫军
	团以他提供一个肉体为代价获得了他的忠诚。通过使用许多让人印象深刻
	的辅助性法术，光之守卫以并不光彩的身份参与到近卫军团的战斗中。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','017','熊战士','hero017','
	在天灾军团入侵以前，一群被称为毛皮兽的智慧熊族在Azshara的心脏地带
	建立了一个安宁而具灵性的社会。在大部分成员被不死族杀害或腐化以后，
	原本与世隔绝的熊族不得不向近卫军团寻求庇护。作为回报，熊族派出了他
	们最强大的熊战士参加到近卫军团。出于对玷污了他们圣地的敌人的愤怒，
	熊战士将他的怒火转化为一连串快速而残忍的攻击。他用他剃刀般的爪子击
	败敌人，将他们撕裂。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','018','食人魔魔法师','hero018','
	作为石锋半兽人的首领，食人魔魔法师是近卫军团中最强壮也最聪明的英雄
	之一。他最特别的能力就是能将无论是伤害性的还是辅助性的法术在一瞬间
	多次施法，这样使得这些法术有着无比惊人的威力。对于近卫军团来说，他
	是这次战争中绝对不可或缺的支援型英雄。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','019','地精修补匠','hero019','
	地精修补匠受过最为高阶的哥布林技术的教育。那些机械系统可以帮助他在
	相当远的距离外杀伤对手，而相对保证自己的安全。他还可以升级他的系统
	使它们能更快的发射。他向你展示了，知识就是力量。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','020','先知','hero020','
	作为暗夜精灵中超凡入圣的代表性人物，先知是一个非常特别的战士。他使
	用自然的力量伤害和阻碍敌人。而他瞬间传送至任何地点的能力带给他战术
	上的巨大优势。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','021','幻影长矛手','hero021','
	人们对他的种族、背景和动机都不甚明了，但他使用幻像和伪装的能力是不
	容置疑的。他时而使用召唤，时而创造镜像，或者干脆突然消失，以此来迷
	惑敌人。他就是如此的神秘，灵巧而又狡猾。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','022','山岭巨人','hero022','
	由Ashenra嶙峋的山脉孕育而生，山岭巨人可以通过投掷大量的泥石造成山崩
	，也能够将任何东西抛向空中，这都显示了他不起眼的身形下其实隐藏着巨
	人般的力量。他岩石般坚硬的外表，让攻击他的敌人反受其害。随着时间的
	推移，山岭巨人身体中心的磁石能够吸引四周的石头土块，让山岭巨人“发身
	长大”，成为名副其实的山岭巨人。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','023','哥布林工程师','hero023','
	极端智慧的大脑，弥补了他们在体形上的弱势，哥布林工程师可不是好对付
	的。他们善于埋布肉眼不能识别的地雷；在接受强大的半兽人巫师的训练以
	后，又学会了放置麻痹陷阱；更不要说他们那威力强大的遥控炸弹。在和这
	三个小家伙作对的时候，你最好别大意。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('2','024','圣骑士','hero024','
	为了预言中的新的黎明，一个没有恶魔的纯净世界，圣骑士加入到对抗天灾
	的战争之中，改变着历史的进程，作为Hextar旗下的一名远征军，他给予那
	些跟随他以战斗的人们以永久的光辉，允诺赦免他们的罪过，也随时考验着
	那些宣誓净化恶魔的人的忠诚，他的追随者不管来自哪个种族，有什么样的
	背景，都会狂热的投入到他的事业中去，在冰峰王座摧毁之前绝不罢休，如
	果上帝真的有向大地伸出援手的话，那么这只手一定是圣骑士！
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','025','月之骑士','hero025','
	月之骑士是月神坚定而又虔诚的信徒。在无尽的战争中，她加入近卫军团竭
	尽全力净化被邪恶的天灾军团所污染的土地。由于她的忠诚和勇敢，月神赐
	予了她一小部分神秘的力量。这份恩赐使得她能轻易打败敌人。据说月之骑
	士能够召唤最纯净的月光，而且永远被灼炽的光环所围绕，就好像身处在月
	光的照耀下。月之骑士是近卫军团最明亮的灯塔和永不懈怠的守卫。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','026','矮人狙击手','hero026','
	矮人狙击手是近卫军团的狙击专家。由于他超远的射程，总能在很安全的位
	置给敌人造成十分巨大的伤害。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','027','巨魔战将','hero027','
	在近卫军团第一次集结时，那些巨魔战士们曾经是被大家认为是不开化并且
	很不可靠的。他们的自尊心因此受到伤害，很多人拒绝加入，有些甚至考虑
	加入天灾军团。只有一个叫做巨魔战将的年轻热血的巨魔战将加入了近卫军
	团。他之所以被巨魔们称做“眩目之斧”，是因为他挥舞斧头的速度令人眩目，
	并且具有让敌人失明的招牌技能。他那不可阻挡的狂暴力量早已在他的盟友
	中成为一个传说。但是，巨魔战将不是为了近卫军团而战斗，甚至不是为了
	守护世界之树，而仅仅是为了向精灵，人类，兽族以及其他一切种族证明，
	巨魔一族才是世界上最强的战士。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','028','暗影萨满','hero028','
	作为巨魔中天赋异秉的巫师，年轻的暗影萨满精通各种巫术的奥义。战争爆
	发以后，他化身为影子猎手向近卫军团效忠并投入战斗，并决心让天灾军团
	万劫不复。身为一个巫师，他精通召唤群蛇守卫，束缚敌人，又或者通过使
	用闪电打击多个目标。他的实力已经全面超越了他的师傅，无愧于“天才般
	的影子猎手”这个称号。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','029','刚背兽','hero029','
	刚背兽是部族中久经考验的真正的斗士。他带领部族保卫家园，然而面对天
	灾军团潮水般的猛攻下不幸失败。随着战况日下，他已经无力阻止天灾军团
	的集结，最后只得答应加入近卫军团。凭借他得天独厚的素质以及经过大自
	然净化的力量，刚背兽的战斗方式非常残忍，使之成为战争中最不拘形式和
	最特立独行的一员。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','030','熊猫酒仙','hero030','
	拥有着常人无法企及的反射神经，熊猫酒仙有效地挥舞他的橡木棍打击对手
	。他是最难以杀死的英雄之一，他的攻击迅捷而有力。他已经实在无法计算
	到底有多少人倒在了他的脚下。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','031','半人马酋长','hero031','
	即使以半人马的标准来衡量，他们的族长半人马酋长的身材仍然称得上是“远
	古巨兽”。即使他的同胞们都已被天灾军团腐化而变节但半人马酋长却依然谨
	守着他的尊严不为所动。不得不依靠打倒自己的同胞才能逃出生天，这使得
	他变得愤怒而无情，他矢志要向天灾军团复仇。现在，他就像一座不可击倒
	的山那样屹立着，而他的敌人随着他手中挥舞着的巨大战斧纷纷倒地。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','032','赏金猎人','hero032','
	说到精湛的技艺，人们经常会想起赏金猎人这个名字。关于他的过去人们所
	知甚少，不过他的能力的确非比寻常。有人说他师从一个忍者组织，也有人
	说他的能力完全来自他的天赋。他的飞镖非常的精准，他锋利的双刀击倒任
	何敌人，他的敏捷无人能比，他隐形能力也数一数二。他只对赏金感兴趣，
	并已被近卫军团许以重金。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','033','龙骑士 ','hero033','
	龙骑士是龙族和人类通婚的产物。藉着变身为龙的能力，对于他的敌人来说，
	龙骑士简直是头凶恶的猛兽。即使在人类形态下，他身上的龙族血统也赋予他
	能力成为异常强壮而且攻击力强大的战士。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','034','敌法师','hero034','
	一种与生俱来的天赋，敌法师甚至可以看见魔法能量以最纯粹的形式出现并控
	制它。天灾军团对世界之树的污染破坏了这个世界的魔法结构。而敌法师将不
	惜牺牲自己来保持大自然的平衡。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','035','黑暗游侠','hero035','
	黑暗游侠曾经是Underdark这个压抑的地底世界的一名出色箭手。直到她再也不
	能忍受她同胞的邪恶行径，她选择逃离到了地表世界。在那里她加入了近卫军
	团以追求真实。虽然常有人因为她的出身对她表示怀疑，她却一次又一次地证
	明了自己。作为一名出色的猎手，她擅长使用冰冻箭减慢对手，也能使用沉默
	术阻止法师施法。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('3','036','全能骑士','hero036','
	作为神的使者，全能骑士能够借助神圣力量使用各种支援与防御技能。他竭尽
	全力地保护和强化他的盟友，不过当必要的时候，他也能亲身投入到攻击和阻
	碍的作战中。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('4','037','兽王','hero037','
	兽王来自传说中具有半兽半食人魔的血统的Mok`nathal氏族。原本是个四处漂
	泊的浪人，但为了更好的保护他身边的大自然，兽王还是提着他那两把充满力
	量的斧头加入了近卫军团。作为野兽的朋友，兽王善于用他那令人不安的野性
	撕扯敌人，而且为了胜利，他甚至会将自己的斧头往敌人身上扔去。兽王是天
	底下真正的勇士，并且在需要他的时候随时都可以挺身而出。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('4','038','双头龙','hero038','
	当人们在一个大洞里找到它时，这头巨兽还在咬着世界之树的树根。而这头华
	丽的奇美拉的年龄，已经无从知晓。由于它的2个大脑袋和它那充满着攻击性
	和不确定性的行为，双头龙是只难以驯化的危险生物。不过现在它出于自身的
	原因，愿意效忠于近卫军团。它对冰元素和火元素的奇异的亲和力，来自于它
	奇异的神秘天性。它毁灭眼前的一切，并且能够将敌人困在一条线上。天灾军
	团最好对这条龙保持警惕，以免他们全被冻住并破碎，就像在他们眼前慢慢融
	化的寒冰王座一样。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('4','039','炼金术士','hero039','
	作为地精炼金术士里精英中的精英，炼金术士受雇于近卫军团开发各种对抗天
	灾部队的化学武器。当亲自投入战斗时，他以一头用作实验品的巨型食人魔作
	为坐骑。那些投下的实验用的化学药剂，足以将食人魔这样已经非常难缠的对
	手变为最可怕的怪物。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('4','040','月之女祭司','hero040','
	作为月神Elune庇护下的长老和高等祭司，月之女祭司在近卫军团中就如同那
	黑暗中的一线光明。她以雨点一般的箭矢和坠落的流星抵挡蜂拥而至的天灾
	大军，而她神圣的存在，消除了四周友军的疲倦，让他们在战场上的速度得
	到极大的提升。在必要的时候，她还能以隐形掩护自己和其他伙伴，使她成
	为重要的支援力量。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('4','041','风暴之灵','hero041','
	作为近卫军团在最黑暗的年代所召唤的圣灵，风暴之灵决定在一个卑微的元素
	使身上证明自己的价值。由于熊猫人自身的灵魂已经在沛不可当的纯粹的电子
	精华的冲击下被毁灭，加上圣灵又无法使用自己的身体，就干脆降临至这个元
	素使的身上。尽管被凡人的躯体所束缚，风暴之灵仍然表现出极为强大的力量
	。通过以一种极为神秘的方式操纵广阔而无穷无忌的力之本源，在近卫军团的
	敌人面前召唤出毁灭性的电子火花，碾碎所有敢于在他面前出现的敌人。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('4','042','神灵武士','hero042','
	暗矛部落的巨魔们在很久以前就被从荆棘谷的原住地流放出来，他们是大多数
	人眼中最野蛮的种族，与他们相比，甚至连战场都显得优雅起来。这个“美誉”
	，更因他们在近卫军团的代表，那个为了将敌人烧成碎片而不惜燃烧自己生命
	的神灵武士，而名声大噪。在他的族人眼里，身为神灵武士的他，是被神祝福
	的烈士，不畏死亡，永远向前，甚至是疯狂的，誓要将天灾不死拖入坟墓。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('4','043','圣堂刺客','hero043','
	圣堂刺客师从于一位来自神族母星Aiur的传奇人物。他是一名使用精神力量的
	高手，并且在近卫军团中专门负责进行战略性质的刺杀行动。她的心灵异能，
	能够让她随意的进入和脱离战斗，并且可以增强攻击时所造成的伤害，同时保
	护她不受敌人的袭击。或者在狩猎哪个不走运的路人之前给自己披上一件隐身
	衣。敌人在战场上发现这个圣堂武士时，往往也就是他们死亡并被埋葬之时，
	当他们的生命被终结后，她便再次潜入暗影，等待下一个猎物。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('4','044','仙女龙','hero044','
	近卫军团一直在寻找能够帮助他们对抗天灾的英雄，最终他们发现了生活在失
	落大陆上的神秘的仙女龙。在解释清楚同女王阿格瑞斯的冲突后，她决定派他
	的贴身侍卫仙女龙来扭转整个战争的局势。虽然仙女龙身材纤细又调皮捣蛋，
	不过在战场上却有着足以证明自己的表现，能穿过所有敌人爆炸性魔法球，仙
	女粉尘，已经以无以伦比的幻术麻痹所有敌人的能力，给所有和她对抗的敌人
	上了一课，所谓不可以貌取人，在他们面前出现的，是无穷无尽的欺骗。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('4','045','发条地精','hero045','
	发条地精是旧战时期地精武器工业的副产品，在战后被弃置多年，直到近卫军
	团发现并重新启动了他。作战方面，发条地精的能力无可挑剔，他体内储藏的
	各式各样的弹药能轻而易举消灭任何方位的敌人。他标志性的铁钳能如索爪般
	伸长，即使针尖大小的东西，也手到擒来。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('4','046','船长','hero046','
	在近位的所有盟军中，只有一个人，在许多大陆上都有着不同但又令人生畏的
	名字。他的追随者们叫他COCO船长，有的人叫他杰克·斯帕罗，还有的人叫他传
	说中的水手。但只有极少数的人知道他的真名和含义。他可以驾御和施展洪流
	的力量，将它们象喷泉一样涌起从而将他们的敌人扔向天空。他华丽的刀锋可
	以接受大海的庇护，因此它的舞动就如同汹涌的潮水。通过一种以水为形式的
	空间魔法，他可以把你带回你以前的位置。现在船长驶向了天灾大陆，同时随
	身带着他那可以激发他和他的战友战斗意志的传说中的朗姆酒。他将为近位军
	团的胜利铺平道路。他就是船长，七大洋的统帅。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('4','047','风行者','hero047','
	在冬泉谷深处翠绿的森林里，一位拥有风之力量的精灵回到了她的故乡。在MAGI
	战争后，她作为一个游侠加入了近位军团。作为风神的宠儿，艾瑞拉得到了许多
	祝福。她精准的箭可以射穿一条直线上的所有敌人。她可以将风的力量赋予到箭
	上，从而将利用空气的枷锁将敌人控制在一起。利用风的涌动，她可以加快自己
	的速度。据说她可以把她的箭失如同狂风般释放出去,从而将敌人在一瞬间消灭
	干净。对于天灾来说，她确实是一个棘手的敌人。
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');

insert into hero(tavern_no, hero_no, hero_name, hero_pic, hero_intro)
	values ('','','','','
	
	');



