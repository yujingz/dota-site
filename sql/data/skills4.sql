

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('037','1','野性之斧','skill0371','W','
	兽王掷出他的双斧，它们交错回旋并最终会回到兽王手中。每把飞斧最多能对同一单位造成一次伤害。施法距离：1300 无视魔法免疫法术攻击 普通伤害
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('037','2','野性呼唤','skill0372','D','
	兽王召唤野兽来帮助战斗。持续时间、能力和野兽的数量随着技能等级的提升而提升。先前召唤的单位在下次召唤时会被移除。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('037','3','兽之狂暴','skill0373','G','
	兽王的动物天性让他在战斗中更加精于单打独斗。通过不断地攻击同一目标，他会得到他的守护神的保佑而使攻击更加迅捷。最多能提升100%的攻击速度。如果兽王改变攻击目标，则会损失一半的攻击速度提升。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('037','4','原始咆哮','skill0374','R','
	兽王惊天动地的咆哮对目标单位造成伤害并晕眩。兽王和目标单位之间的单位则因为被冲击波所波及而受到较少伤害，攻击速度和移动速度因此降低50%。被波及的单位同时会被震退至两侧，让兽王能长驱直入。施法距离：600
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','1','1','
	每把飞斧对经过的敌方单位造成90点的伤害。 施法间隔:13秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','1','2','
	每把飞斧对经过的敌方单位造成120点的伤害。 施法间隔:13秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','1','3','
	每把飞斧对经过的敌方单位造成150点的伤害。 施法间隔:13秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','1','4','
	每把飞斧对经过的敌方单位造成180点的伤害。 施法间隔:13秒 施法消耗:120点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','2','1','
	召唤一只猫头鹰哨兵。持续时间60秒。 施法间隔:25秒 施法消耗:40点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','2','2','
	召唤一只猫头鹰哨兵和一只豪猪。豪猪的毒性攻击降低20%的攻击速度和移动速度。持续时间70秒。 施法间隔:25秒 施法消耗:40点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','2','3','
	召唤一只高等猫头鹰哨兵和一只豪猪。高等猫头鹰哨兵具有永久隐身能力，豪猪的毒性攻击降低20%的攻击速度和移动速度。持续时间80秒。 施法间隔:25秒 施法消耗:40点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','2','4','
	召唤一只高等猫头鹰哨兵和一只高等豪猪。高等猫头鹰哨兵具有永久隐身能力，高等豪猪的毒性攻击降低35%的攻击速度和移动速度。持续时间90秒。 施法间隔:25秒 施法消耗:40点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','3','1','
	每攻击一次得到5%的攻击速度提升。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','3','2','
	每攻击一次得到10%的攻击速度提升。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','3','3','
	每攻击一次得到15%的攻击速度提升。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','3','4','
	每攻击一次得到20%的攻击速度提升。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','4','1','
	目标单位受到200点的伤害，晕眩3秒；受波及单位受到100点的伤害，攻击速度和移动速度下降50%，持续2秒。 施法间隔:80秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','4','2','
	目标单位受到250点的伤害，晕眩3.5秒；受波及单位受到200点的伤害，攻击速度和移动速度下降50%，持续3秒。 施法间隔:75秒 施法消耗:175点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('037','4','3','
	目标单位受到300点的伤害，晕眩4秒；受波及单位受到300点的伤害，攻击速度和移动速度下降50%，持续4秒。 施法间隔:70秒 施法消耗:200点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('038','1','冰火交加','skill0381','D','
	双头龙的双头能分别吐出寒冰和火焰气息，造成伤害。在接下的4秒内，敌方单位的攻击速度和移动速度因此下降，并受到持续的火焰伤害。施法距离：500。最大距离：500。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('038','2','冰封路径','skill0382','T','
	在双头龙面前创造一条冰封路径（无任何影响）。0.5秒后，路径碎裂，将周围的单位冻结起来。被冻结的单位无法移动、攻击、使用技能和物品。施法距离：1100
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('038','3','自动之火','skill0383','A','
	在Jakiro的一个脑袋进行攻击的同时，另一个脑袋也偶尔会向面前的敌方单位吐射火球，在区域内造成持续5秒的火焰伤害。一秒内最多吐射一次火球。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('038','4','万火焚身','skill0384','R','
	双头龙在前方施放连续的烈焰风暴，每次烈焰风暴造成持续5秒的伤害。施法距离：550/700/850
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','1','1','
	吐出寒冰和火焰气息，分别造成35点的伤害。在接下的4秒内，敌方单位的攻击速度和移动速度因此下降，并受到持续的5点/秒的火焰伤害。 施法间隔:10秒 施法消耗:135点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','1','2','
	吐出寒冰和火焰气息，分别造成70点的伤害。在接下的4秒内，敌方单位的攻击速度和移动速度因此下降，并受到持续的10点/秒的火焰伤害。 施法间隔:10秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','1','3','
	吐出寒冰和火焰气息，分别造成105点的伤害。在接下的4秒内，敌方单位的攻击速度和移动速度因此下降，并受到持续的15点/秒的火焰伤害。 施法间隔:10秒 施法消耗:155点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','1','4','
	吐出寒冰和火焰气息，分别造成140点的伤害。在接下的4秒内，敌方单位的攻击速度和移动速度因此下降，并受到持续的20点/秒的火焰伤害。 施法间隔:10秒 施法消耗:170点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','2','1','
	冻结0.5秒。 施法间隔:16秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','2','2','
	冻结1秒。 施法间隔:16秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','2','3','
	冻结1.5秒。 施法间隔:16秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','2','4','
	冻结2秒。 施法间隔:16秒 施法消耗:75点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','3','1','
	有25%的几率吐射火球，在区域内造成10点/秒的火焰伤害，持续8秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','3','2','
	有30%的几率吐射火球，在区域内造成20点/秒的火焰伤害，持续8秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','3','3','
	有35%的几率吐射火球，在区域内造成30点/秒的火焰伤害，持续8秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','3','4','
	有40%的几率吐射火球，在区域内造成40点/秒的火焰伤害，持续8秒。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','4','1','
	每次烈焰风暴造成75点/秒的伤害。 施法间隔:60秒 施法消耗:220点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','4','2','
	每次烈焰风暴造成125点/秒的伤害。 施法间隔:60秒 施法消耗:330点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('038','4','3','
	每次烈焰风暴造成175点/秒的伤害。 施法间隔:60秒 施法消耗:440点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('039','1','酸性喷雾','skill0391','D','
	在目标区域形成高腐蚀性的酸性雾气。逗留被腐蚀区域的敌方单位将持续受到伤害，护甲同时被降低。施法距离：900。作用范围：625。持续时间：16秒。 法术攻击 普通伤害无视魔法免疫
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('039','2','不稳定化合物','skill0392','E','
	用一些时间调配药剂，并在之后投掷向一个敌方单位，造成晕眩并造成伤害。晕眩的时间等同于之前用于调配的时间，联动的伤害为60点/秒。施法距离：650/750/850/950
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('039','3','地精的贪婪','skill0393','G','
	地精炼金术士在杀死敌方单位时能得到额外的金钱奖励，而在18秒内连续杀死单位更可以提高2的金钱奖励额度，上限18。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('039','4','化学狂暴','skill0394','R','
	炼金术士用化学药剂让他的食人魔坐骑陷入狂暴，得到额外的生命值奖励，降低基础攻击间隔，提高基础生命和魔法回复速度。移动速度增加为330/340/360。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','1','1','
	受到8点/秒的伤害，护甲降低3点。 施法间隔:22秒 施法消耗:160点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','1','2','
	受到16点/秒的伤害，护甲降低4点。 施法间隔:22秒 施法消耗:160点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','1','3','
	受到24点/秒的伤害，护甲降低5点。 施法间隔:22秒 施法消耗:160点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','1','4','
	受到32点/秒的伤害，护甲降低6点。 施法间隔:22秒 施法消耗:160点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','2','1','
	最多2秒。 施法间隔:16秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','2','2','
	最多3秒。 施法间隔:16秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','2','3','
	最多4秒。 施法间隔:16秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','2','4','
	最多5秒。 施法间隔:16秒 施法消耗:75点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','3','1','
	每杀死一个敌方单位得到额外的2的金钱奖励。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','3','2','
	每杀死一个敌方单位得到额外的4的金钱奖励。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','3','3','
	每杀死一个敌方单位得到额外的6的金钱奖励。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','3','4','
	每杀死一个敌方单位得到额外的8的金钱奖励。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','4','1','
	增加350点的生命，基础攻击间隔降低至1.45秒，基础生命回复速度上升至15点/秒，基础魔法回复速度上升至3点/秒。持续时间：25秒 施法间隔:45秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','4','2','
	增加700点的生命，基础攻击间隔降低至1.35秒，基础生命回复速度上升至30点/秒，基础魔法回复速度上升至7.5点/秒。持续时间：25秒 施法间隔:45秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('039','4','3','
	增加1050点的生命，基础攻击间隔降低至1.20秒，基础生命回复速度上升至60点/秒，基础魔法回复速度上升至12点/秒。持续时间：25秒 施法间隔:45秒 施法消耗:150点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('040','1','群星坠落','skill0401','T','
	召唤陨石对周围600范围内的敌方单位造成伤害。600-900范围内的敌方单位仍有60%的几率被击中；200范围内随机的一个敌方单位将额外受到一次半额伤害的打击。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('040','2','月神之箭','skill0402','R','
	月神之箭 (R)
以致命的精确性向一个地点射出一箭，箭矢带有视野，对触及的第一个敌方单位造成伤害并晕眩。晕眩时间随距离而上升，每隔150提升0.5秒的晕眩时间，最多晕眩5秒。施法距离：3000。最大距离：2500。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('040','3','跳跃','skill0403','E','
	月之女祭司的坐骑向前跳跃，她在跳跃过程中是非无敌的。坐骑在落地后的咆哮形成一个作用范围为800持续10秒的光环，提升其中的友方英雄的攻击速度和移动速度。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('040','4','月之暗面','skill0404','W','
	以夜色掩护自己和其他所有友方英雄，进入隐身状态。在持续时间内，即使隐身状态被打破，也能在较短的时间内恢复。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','1','1','
	造成75点的伤害。 施法间隔:12秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','1','2','
	造成150点的伤害。 施法间隔:12秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','1','3','
	造成225点的伤害。 施法间隔:12秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','1','4','
	造成300点的伤害。 施法间隔:12秒 施法消耗:160点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','2','1','
	造成90点的伤害。 施法间隔:25秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','2','2','
	造成180点的伤害。 施法间隔:25秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','2','3','
	造成270点的伤害。 施法间隔:25秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','2','4','
	造成360点的伤害。 施法间隔:25秒 施法消耗:100点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','3','1','
	跳跃距离400，提升4%的攻击速度和移动速度。 施法间隔:40秒 施法消耗:40点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','3','2','
	跳跃距离450，提升8%的攻击速度和移动速度。 施法间隔:35秒 施法消耗:35点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','3','3','
	跳跃距离500，提升12%的攻击速度和移动速度。 施法间隔:30秒 施法消耗:30点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','3','4','
	跳跃距离550，提升16%的攻击速度和移动速度。 施法间隔:20秒 施法消耗:20点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','4','1','
	持续7秒，2.5秒的恢复隐身时间。 施法间隔:160秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','4','2','
	持续9秒，2秒的恢复隐身时间。 施法间隔:160秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('040','4','3','
	持续11秒，1.5秒的恢复隐身时间。 施法间隔:160秒 施法消耗:200点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('041','1','残影','skill0411','R','
	风暴之灵用电的能量创造出自己的残影。残影会自动向靠的太近的敌方单位释放出电能，对其造成伤害。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('041','2','电子涡流','skill0412','E','
	风暴之灵聚集他的能量，以自身速度降低的代价，创造出涡流，将一个敌人牵引过来。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('041','3','超负荷','skill0413','V','
	风暴之灵施放任何技能，都能让体内的电能超负荷运转，下次攻击时释放出的电能冲击，对范围内的敌方单位造成额外的伤害，并降低275范围内单位50%的攻击速度和80%的移动速度，持续0.6秒。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('041','4','球状闪电','skill0414','R','
	风暴之灵被闪电包裹起来，丢弃其物理形态，以进行超速移动，直到其魔法耗尽或达到目标。在移动过程中，任何靠近他的单位都会受到电击的伤害（伤害值正比于风暴之灵移动的距离）。移动速度和伤害范围随着等级的提高而增加。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','1','1','
	造成140点伤害 施法间隔:4秒 施法消耗:70点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','1','2','
	造成180点伤害 施法间隔:4秒 施法消耗:80点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','1','3','
	造成220点伤害 施法间隔:4秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','1','4','
	造成260点伤害 施法间隔:4秒 施法消耗:100点魔法	
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','2','1','
	牵引范围100，用时1秒。 施法间隔:20秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','2','2','
	牵引范围150，用时1.5秒。 施法间隔:20秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','2','3','
	牵引范围200，用时2秒。 施法间隔:20秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','2','4','
	牵引范围250，用时2.5秒。 施法间隔:20秒 施法消耗:100点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','3','1','
	30点额外伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','3','2','
	45点额外伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','3','3','
	60点额外伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','3','4','
	75点额外伤害。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','4','1','
	每滚动100的距离，造成8点伤害。 施法间隔:0秒 施法消耗:根据其移动的距离
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','4','2','
	每滚动100的距离，造成12点伤害。 施法间隔:0秒 施法消耗:根据其移动的距离
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('041','4','3','
	每滚动100的距离，造成16点伤害。 施法间隔:0秒 施法消耗:根据其移动的距离
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('042','1','活血术','skill0421','V','
	以魔法激发目标单位身体内潜在的恢复力量，根据他的主属性恢复生命，当目标生命值低于40%的时候将得到更快的恢复速度（括号内是生命值低于40%时的数字）施法距离：400
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('042','2','沸血之矛','skill0422','R','
	用自己的鲜血让矛燃烧，如跗骨之蛆般使敌人单位受到最多可叠加5次的持续性伤害，持续6秒。在神灵武士生命值低于100时不能使用。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('042','3','狂战士之血','skill0423','B','
	神灵武士将所承受的每一分创伤和疼痛转变为自身力量的提升，每损失7%的血量就能够得到相应的攻击力和攻击速度的提升，总共可以提升7次。（满血是仍然能得到一次提升）
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('042','4','牺牲','skill0424','F','
	神灵武士以伤害自身为代价，对目标造成当前血量40%的伤害，并降低目标50%的移动速度，持续5秒。减速效果无视魔法免疫。施法距离：135。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','1','1','
	每秒恢复5(15)% *主属性+ 2点的生命。持续时间16秒。 施法间隔:30秒 施法消耗:170点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','1','2','
	每秒恢复10(30)% *主属性+ 4点的生命。持续时间16秒。 施法间隔:30秒 施法消耗:170点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','1','3','
	每秒恢复15(45)% *主属性+ 6点的生命。持续时间16秒。 施法间隔:30秒 施法消耗:170点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','1','4','
	每秒恢复20(60)% *主属性+ 8点的生命。持续时间16秒。 施法间隔:30秒 施法消耗:170点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','2','1','
	每秒造成6点伤害。每次攻击造成生命消耗15点。 施法间隔:0秒 施法消耗:15点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','2','2','
	每秒造成8点伤害。每次攻击造成生命消耗15点。 施法间隔:0秒 施法消耗:15点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','2','3','
	每秒造成10点伤害。每次攻击造成生命消耗15点。 施法间隔:0秒 施法消耗:15点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','2','4','
	每秒造成12点伤害。每次攻击造成生命消耗15点。 施法间隔:0秒 施法消耗:15点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','3','1','
	每次提升2%的攻击速度，增加2点攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','3','2','
	每次提升4%的攻击速度，增加4点攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','3','3','
	每次提升6%的攻击速度，增加6点攻击力。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','3','4','
	每次提升8%的攻击速度，增加8点攻击力。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','4','1','
	神灵武士受到40%当前血量的伤害。 施法间隔:0秒 施法消耗:45点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','4','2','
	神灵武士受到33%当前血量的伤害。 施法间隔:0秒 施法消耗:45点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('042','4','3','
	神灵武士受到25%当前血量的伤害。 施法间隔:0秒 施法消耗:45点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('043','1','折光','skill0431','R','
	使用心灵异能在圣堂刺客外形成一层折光外壳，以抵挡一定次数的伤害，并增加下几次攻击的攻击力。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('043','2','隐匿','skill0432','D','
	传承自黑暗圣堂母星沙克拉斯的技巧，使圣堂刺客能在任何时候进入隐身状态（任何隐身和攻击将打破隐身状态），如果以攻击打破隐身状态（攻击前不能移动），能对目标造成格外伤害并降低目标的护甲。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('043','3','灵能之刃','skill0433','B','
	运用他的心灵异能，圣堂刺客一方面增加自己的攻击距离，同时将范围扩展至目标身后的区域。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('043','4','灵能陷阱','skill0434','C','
	运用心灵异能制造一个陷阱，触发后降低周围400范围内敌人50%的移动速度。持续5秒。施法距离：2000。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','1','1','
	抵挡2次伤害，增加下两次攻击20点的攻击力。持续时间20秒。 施法间隔:23秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','1','2','
	抵挡3次伤害，增加下两次攻击40点的攻击力。持续时间20秒。 施法间隔:23秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','1','3','
	抵挡4次伤害，增加下两次攻击60点的攻击力。持续时间20秒。 施法间隔:23秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','1','4','
	抵挡5次伤害，增加下两次攻击80点的攻击力。持续时间20秒。 施法间隔:23秒 施法消耗:75点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','2','1','
	额外造成50点伤害，降低2点护甲。持续十秒。 施法间隔:7秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','2','2','
	额外造成100点伤害，降低4点护甲。持续十秒。 施法间隔:7秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','2','3','
	额外造成150点伤害，降低6点护甲。持续十秒。 施法间隔:7秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','2','4','
	额外造成200点伤害，降低8点护甲。持续十秒。 施法间隔:7秒 施法消耗:50点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','3','1','
	增加40的攻击距离， 目标身后60范围内的敌人收到伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','3','2','
	增加80的攻击距离， 目标身后120范围内的敌人收到伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','3','3','
	增加120的攻击距离， 目标身后180范围内的敌人收到伤害。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','3','4','
	增加160的攻击距离， 目标身后240范围内的敌人收到伤害。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','4','1','
	最多同时拥有5个陷阱。 施法间隔:14秒 施法消耗:15点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','4','2','
	最多同时拥有8个陷阱。 施法间隔:14秒 施法消耗:15点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('043','4','3','
	最多同时拥有11个陷阱。 施法间隔:14秒 施法消耗:15点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('044','1','幻象法球','skill0441','R','
	释放一个直线飞行的魔法球，移动过程中对附近225范围内的敌方单位造成伤害，仙女龙可以随时传送至魔法球所在的位置。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('044','2','新月之痕','skill0442','W','
	仙女龙撒出他神秘的仙女粉尘，对周围400范围内造成伤害并禁魔。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('044','3','相位转移','skill0443','F','
	仙女龙在短时间内改变自己所处的维度，暂时不受到任何的伤害。相位转移状态可随时中止。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('044','4','梦境缠绕','skill0444','C','
	仙女龙的强大幻术笼罩战场，持续5秒。创造的不断变化的魔法缠绕瞬间对附近400范围内的敌方英雄造成伤害和晕眩。试图挣脱缠绕而离开梦境原点600以外的英雄会再次收到伤害并晕眩。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','1','1','
	造成70点伤害。 施法间隔:15秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','1','2','
	造成140点伤害。 施法间隔:15秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','1','3','
	造成210点伤害。 施法间隔:15秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','1','4','
	造成280点伤害。 施法间隔:15秒 施法消耗:125点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','2','1','
	造成60点伤害，禁魔0.1秒。 施法间隔:20秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','2','2','
	造成120点伤害，禁魔1秒。 施法间隔:20秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','2','3','
	造成180点伤害，禁魔2秒。 施法间隔:20秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','2','4','
	造成240点伤害，禁魔3秒。 施法间隔:20秒 施法消耗:130点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','3','1','
	最多持续0.75秒。 施法间隔:6秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','3','2','
	最多持续1.5秒。 施法间隔:6秒 施法消耗:40点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','3','3','
	最多持续2.25秒。 施法间隔:6秒 施法消耗:30点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','3','4','
	最多持续3秒。 施法间隔:6秒 施法消耗:20点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','4','1','
	瞬间造成100点伤害和0.5秒晕眩，如果梦境破碎将额外造成100点伤害和1.5秒晕眩。 施法间隔:85秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','4','2','
	瞬间造成150点伤害和0.5秒晕眩，如果梦境破碎将额外造成150点伤害和2.25秒晕眩。 施法间隔:85秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('044','4','3','
	瞬间造成200点伤害和0.5秒晕眩，如果梦境破碎将额外造成200点伤害和3秒晕眩。 施法间隔:85秒 施法消耗:200点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('045','1','弹幕冲击','skill0451','E','
	发条地精从机械结构的开口处喷出高能弹片，每0.75秒对附近的随机的一个敌方单位造成一定的伤害和0.01秒的晕眩。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('045','2','能量齿轮','skill0452','C','
	发条地精以弹出的内部核心零件开成屏障环绕在他的四周，将附近的敌方单位困在其中。这些齿轮在消失前需要受到3次攻击才能完全摧毁。如果带有魔法值的敌方单位尝试从外侧靠近其中任意一个齿轮，就会受到震击，损失一定的生命和魔法，并被击退，齿轮在震击后耙尽能量。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('045','3','照明火箭','skill0453','R','
	向指定区域发射一枚快速的照明火箭，对作用范围内的敌方单位造成伤害。照明火箭同时带有侦察效果。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('045','4','发射钩爪','skill0454','T','
	向目标或者指定区域射出一个可以伸缩的钩爪，接触任意单位即缩回。如果这是一个敌方（不包括中立）单位，钩爪会将其牢牢抓住并将发条地精牵引至这个单位的身边。牵引过中对附近的敌方单位造成伤害并晕眩。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','1','1','
	每次喷射造成20点的伤害。 施法间隔:32秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','1','2','
	每次喷射造成40点的伤害。 施法间隔:28秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','1','3','
	每次喷射造成60点的伤害。 施法间隔:24秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','1','4','
	每次喷射造成80点的伤害。 施法间隔:20秒 施法消耗:75点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','2','1','
	屏障持续3秒，受到震击的单位损失55点的生命和魔法。 施法间隔:15秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','2','2','
	屏障持续4秒，受到震击的单位损失70点的生命和魔法。 施法间隔:15秒 施法消耗:60点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','2','3','
	屏障持续5秒，受到震击的单位损失85点的生命和魔法。 施法间隔:15秒 施法消耗:70点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','2','4','
	屏障持续6秒，受到震击的单位损失100点的生命和魔法。 施法间隔:15秒 施法消耗:80点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','3','1','
	造成80点伤害，视野持续5秒。 施法间隔:30秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','3','2','
	造成120点伤害，视野持续10秒。 施法间隔:25秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','3','3','
	造成160点伤害，视野持续15秒。 施法间隔:20秒 施法消耗:50点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','3','4','
	造成200点伤害，视野持续20秒。 施法间隔:15秒 施法消耗:50点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','4','1','
	施法距离2000，牵引造成100点的伤害和1秒的晕眩。 施法间隔:80秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','4','2','
	施法距离2500，牵引造成200点的伤害和1.5秒的晕眩。 施法间隔:60秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('045','4','3','
	施法距离3000，牵引造成300点的伤害和2秒的晕眩。 施法间隔:40秒 施法消耗:150点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('046','1','洪流','skill0461','E','
	通过运用他无与伦比的海洋方面的知识，普罗德摩尔将军有能力在指定的区域召唤一阵水流。2秒之后，水流变成一股汹涌的洪流，冲出地面，将范围内的敌方单位抛至空中，造成伤害并在接下来数秒内降低30%的移动速度。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('046','2','潮汐使者','skill0462','D','
	在剑上附加大海的力量，每过一定的时间，潮汐使者便被动地给予普罗德摩尔将军额外的力量（增加攻击力和380/420/460/500范围的溅射伤害，仅限一次攻击）
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('046','3','地图地标','skill0463','X','
	在一个英雄脚下画出一个十字，一段时间后这个英雄会自动被传送到这个十字点。也可以使用Return（R）让那个英雄提前回到十字。施法距离500/650/800/950。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('046','4','幽灵船','skill0464','T','
	召唤出鬼船S.S.CoCo，鬼船会在船长与目标点连成的直线上航行2000距离（从船长身后开到身前，各1000距离），然后发生爆炸，对425范围内的敌方单位造成一定伤害并且造成1秒眩晕。途中鬼船经过的己方英雄移动速度+10%，只受到50%的伤害，当加速效果消失后会受到在这段时间里积攒的另外50%的伤害。幽灵船有视野。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','1','1','
	造成120点伤害，落下之后移动速度减少30%，1秒减速 施法间隔:12秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','1','2','
	造成180点伤害，落下之后移动速度减少30%，2秒减速 施法间隔:12秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','1','3','
	造成240点伤害，落下之后移动速度减少30%，3秒减速 施法间隔:12秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','1','4','
	造成300点伤害，落下之后移动速度减少30%，4秒减速 施法间隔:12秒 施法消耗:120点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','2','1','
	每16秒在下次攻击中增加15点攻击力。作用范围425。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','2','2','
	每12秒在下次攻击中增加30点攻击力。作用范围450。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','2','3','
	每8秒在下次攻击中增加45点攻击力。作用范围475。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','2','4','
	每4秒在下次攻击中增加60点攻击力。作用范围500
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','3','1','
	1秒后传送回十字点施法距离500。 施法间隔:30秒 施法消耗:80点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','3','2','
	2秒后传送回十字点施法距离650。 施法间隔:30秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','3','3','
	3秒后传送回十字点施法距离800。 施法间隔:30秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','3','4','
	4秒后传送回十字点施法距离950。 施法间隔:30秒 施法消耗:110点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','4','1','
	爆炸造成300伤害，加速效果持续5秒。 施法间隔:100秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','4','2','
	爆炸造成400伤害，加速效果持续6秒。 施法间隔:100秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('046','4','3','
	爆炸造成500伤害，加速效果持续7秒。 施法间隔:100秒 施法消耗:250点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('047','1','束缚击','skill0471','E','
	风行者射出一支被施展了风魔法的箭矢，将目标束缚其身后的一个地方单位或树上。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('047','2','强力击','skill0472','R','
	风行者张紧她的弓，用1秒钟的时间积攒力量，进行一次强力的射击。射出的箭最远飞出1700的距离，能摧毁树木，并对碰触到的所有敌方单位造成伤害（对第一个单位造成最大伤害，以后每个单位递减10%的伤害）。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('047','3','风行者','skill0473','W','
	通过让风的精粹浸透自己的身体，风行者能够在短时间内提升自身50%的移动速度。同时，环绕在她身边的风之屏障会给予强大的保护，使她无视所有的物理攻击，并且使周围300范围内敌方单位的移动速度降低。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('047','4','集中火力','skill0474','F','
	风行者将风贯穿她的体内，激发出巨大的能量，使她在接下来的攻击速度达到极限。然而，副作用的攻击力会在期间内降低。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','1','1','
	束缚1.5秒。 施法间隔:10秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','1','2','
	束缚2.25秒。 施法间隔:10秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','1','3','
	束缚3秒。 施法间隔:10秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','1','4','
	束缚3.75秒。 施法间隔:10秒 施法消耗:90点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','2','1','
	120点最大伤害。 施法间隔:9秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','2','2','
	200点最大伤害。 施法间隔:9秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','2','3','
	280点最大伤害。 施法间隔:9秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','2','4','
	360点最大伤害。 施法间隔:9秒 施法消耗:120点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','3','1','
	降低周围敌军8%的移动速度，持续2.75秒。 施法间隔:30秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','3','2','
	降低周围敌军16%的移动速度，持续3.5秒。 施法间隔:25秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','3','3','
	降低周围敌军24%的移动速度，持续4.25秒。 施法间隔:20秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','3','4','
	降低周围敌军30%的移动速度，持续5秒。 施法间隔:15秒 施法消耗:100点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','4','1','
	降低自身50%的攻击力。 施法间隔:60秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','4','2','
	降低自身40%的攻击力。 施法间隔:60秒 施法消耗:300点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('047','4','3','
	降低自身30%的攻击力。 施法间隔:60秒 施法消耗:400点魔法
	');
