insert into member(member_no, password, birthday, name)
	values ('0743111268','123','1990-10-21','double');

insert into tavern(tavern_no, tavern_name, tavern_pic)
	values ('1','清晨酒馆','tavern1');
insert into tavern(tavern_no, tavern_name, tavern_pic)
	values ('2','旭日酒馆','tavern2');
insert into tavern(tavern_no, tavern_name, tavern_pic)
	values ('3','曙光酒馆','tavern3');
insert into tavern(tavern_no, tavern_name, tavern_pic)
	values ('4','黎明酒馆','tavern4');
insert into tavern(tavern_no, tavern_name, tavern_pic)
	values ('5','落日酒馆','tavern5');
insert into tavern(tavern_no, tavern_name, tavern_pic)
	values ('6','午夜酒馆','tavern6');
insert into tavern(tavern_no, tavern_name, tavern_pic)
	values ('7','暮色酒馆','tavern7');
insert into tavern(tavern_no, tavern_name, tavern_pic)
	values ('8','黄昏酒馆','tavern8');


insert into shop(shop_no, shop_name, shop_pic)
	values('01','卷轴商人 等级1','shop01');
insert into shop(shop_no, shop_name, shop_pic)
	values('02','卷轴商人 等级2','shop02');
insert into shop(shop_no, shop_name, shop_pic)
	values('03','卷轴商人 等级3','shop03');
insert into shop(shop_no, shop_name, shop_pic)
	values('04','卷轴商人 等级4','shop04');
insert into shop(shop_no, shop_name, shop_pic)
	values('05','新卷轴','shop05');
insert into shop(shop_no, shop_name, shop_pic)
	values('06','饰品商人 希娜','shop06');
insert into shop(shop_no, shop_name, shop_pic)
	values('07','武器商人 比泽','shop07');
insert into shop(shop_no, shop_name, shop_pic)
	values('08','黑市商人 雷拉格','shop08');
insert into shop(shop_no, shop_name, shop_pic)
	values('09','奎尔瑟兰的密室','shop09');
insert into shop(shop_no, shop_name, shop_pic)
	values('10','奇美拉栖木','shop10');
insert into shop(shop_no, shop_name, shop_pic)
	values('11','地精商店','shop11');


insert into mix(shop_no, item_no, mix_shop_no, mix_item_no)
	values('01','001','','');

insert into vedio(vedio_no, page_no, vedio_title, vedio_depict, vedio_comment_num)
	values('00001','1','测试视频','这是测试视频...','0');
insert into vedio(vedio_no, page_no, vedio_title, vedio_depict, vedio_comment_num)
	values('00002','1','测试视频2','这是测试视频...','4');
insert into vedio(vedio_no, page_no, vedio_title, vedio_depict, vedio_comment_num)
	values('00003','1','测试视频3','这是测试视频...','22');
insert into vedio(vedio_no, page_no, vedio_title, vedio_depict, vedio_comment_num)
	values('00004','2','测试视频4','这是测试视频...','7');

insert into article(article_no, page_no, category, article_title, article_date, content)
	values('00001','001','游戏攻略','关小黑屋和翻牌子的技巧','2009-06-10','
	这篇东西的对某特定单一比赛是没有太多价值的，原因有二：在石头剪刀布的游戏中猜石头出现次数不能帮助你下一次猜拳能赢；在目前ban&pick时间过短，战术的制定随意性很大，凭picker经验成分比较多。
肯定有人骂：LZ你二嘛，你他奶奶的知道还写。原因有二：我是在写完后才发现这篇东西的无用之处，这真是一个悲剧；数据分析和类似数据分析类体现后验性，即在总结问题时能够拿来参考。
对Ban&pick分析总结采取“我知道你知道我知道所以我这样”的分析办法。
什么叫那个啥分析办法呢？比如我接到案子写起诉书，然后根据起诉书站在被告律师角度写一稿答辩状，然后回来根据答辩状写起诉书二稿。
对战时每个战队都会实现准备三到四套战术（排除放大招），而战术都是围绕核心英雄建立，而在Ban&Pick阶段目标就是ban掉对手选择的核心英雄，拿到自己最合手的阵容。
将对手熟悉惯用能够打败自己的战术核心关在小黑屋里，就是Ban的作用
先手Ban
1不要Ban地穴刺客
无数先Ban甚至在Firstban就关掉NA，这让我无法理解：一个非Ban即选的英雄，如果自己能选到（先ban者先选）无疑可以对对方核心，尤其是现在由法师作的核心造成巨大损害；就算ban也要等对方动用一个ban权去关，根本不需要自己去。这种选择在各种大牌战队上都出现，十分不合理。
2少ban热门核心英雄。所谓热门英雄就是目前无法有效抑制的核心英雄，所以根本不在乎动用FirstPick去选，反正对方找不到有效克制办法，不用怕过早暴露战术。57-59的幽鬼隐刺，59后的尸王小歪NEC都是这类。
3Ban团控而不是ban辅助。这多是在诱使对方上自己套子时的选择，留下优秀的英雄让对手先双选，然后在自己二手选是双选团控，让对方失去团队作战能力。热门辅助的作用是比不上团控的，毕竟现在版本节奏加快，早期结束的时间提前很多而中期提前到来，辅助在保护自己核心的作用上被削弱很多。
后手ban
1NA热门核心英雄，没有什么好想的，能ban多少ban多少。
2少团控多辅助，因为后手选有连续2次连选，更多的机会拿团控，而热门辅助很可能在先手选2次同对方核心直接出现。
针对ban:摸了你的宙斯 原7L的睡箭，目前只有这两个值得被专门针对。由于战术在57后极大的丰富，使得ban的影响减弱而在pick上较起劲来。
这里似乎要说明一下我对团控的理解- -潮汐大树我一般理解为大控，而沙王恶魔巫师等XX帮，pucklina痛苦才是团控之列，对团控的推崇多是因为现行控制技能combo的流行。
先pick
1FirstPck这是一个尴尬的先手选。有热门核心无疑立即选，但大多数情况下是看不到SSNEC会活着出现的，选核心无疑会过早暴露，选辅助太不划算。
这时可以选择团控，本着团控永远不嫌多的准则，牛头沙王斧王痛苦都是上佳之选；可以选双核打法，选择一些能够不需要过多占线依然能发挥作用，且有拯救世界潜力的英雄，目前看只有船长Dp能够作为双核战术的优秀弱核；陈，如果你有一个APM够高的选手，请毫不犹豫的Fpick之。
2双选 更多的应该是主力法师和团控的登场。在没有大后期出现的情况下主力法师和团控在早中期占据优势十分必要，优秀的组合如沙王火女不光在线上而且游走都能提供极大威胁，更重要的是，他们不需长期占线。这时选核心+辅助的劣势是在对方核心出现前暴露，被对方有效选择克制英雄，或者浪费抢团控的机会，也失去在对方第二次双选出核心的情况下反克制的机会。
当然在后手方第一次双选就亮出核心的情况下，选出自己核心也是可以的
3终选 核心登场或者是补强或者是双核。
后pick
1核心登场时间或者双控 后手永远面临一个被抢的处境，抢得从banlist漏网的强力英雄是第一要务，优秀团控也需要在对方双选前先取得一个。
2补强。辅助和法师的登场，pom和puck也是中庸的选择。在对手战术整体构建完成时选择能够破坏的英雄是明智之举。保证己方早期线上优势，打击对方核心成为这一双选的要务。
3终选。这是另一个尴尬之处。能够沉得住气可以这时选核心英雄是不错的时机，不过要沉得住气。看运气捡漏吧。
现阶段的Ban&Pick更多的是对热门核心英雄选取或者抑制，但针对一些有自己风格和技术特点的战队时则完全是另一套Ban选方式。不过能够称得上“有自己风格”的成名战队：gank向KS.int,原7L，团战原MYM，Farm啊FarmSK如此而已。一只成熟的竞技队伍，应该有属于自己的标签，比如华丽技术的巴萨，比如ShowTime表演风格的湖人，根据队伍风格找队友而不是来一个队员就换一个风格。不过要求Dota这种幼儿期的竞技比赛像足球篮球是一件可笑的事情，但是我们依然看到KS.int和Maelk努力保持自己的风格特点。继承7L骨风的KS.cn虽然在原战术核心LDD出走的情况下，坚持自己gank风拿全gank阵容，玩一级草肉山,虽然现在走的可能并不太顺，但我个人依然希望能够保持并希望有自己风格标签的战队越来越多。
	');

insert into article(article_no, page_no, category, article_title, article_date, content)
	values('0000','00','','','2009-06-','
	
	');