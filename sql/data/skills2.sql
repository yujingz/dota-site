insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('013','1','静默诅咒','skill0131','C','
	用静默诅咒折磨目标。目标会持续损失生命和魔法，直到他使用了任意技能为止。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('013','2','智慧之刃','skill0132','W','
	沉默术士的剑刃经过的法术强化。如果他杀死一个英雄，他将会永久地从这个英雄身上偷取1点的智力。法球效果英雄攻击 神圣（魔法）伤害
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('013','3','最后遗言','skill0133','L','
	所有在光环笼罩下的敌方单位在施放完一个技能后会被沉默，在一段时间内不能施法，作用范围700。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('013','4','全领域静默','skill0134','D','
	阻止地图上所有的敌方单位使用魔法。无视魔法免疫
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','1','1','
	每秒损失10点生命值和5点魔法值。 施法间隔:25秒 施法消耗:105点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','1','2','
	每秒损失20点生命值和10点魔法值。 施法间隔:25秒 施法消耗:115点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','1','3','
	每秒损失30点生命值和15点魔法值。 施法间隔:25秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','1','4','
	每秒损失40点生命值和20点魔法值。 施法间隔:25秒 施法消耗:135点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','2','1','
	每次攻击附加智力的15%作为额外伤害。 施法间隔:3秒 施法消耗:15点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','2','2','
	每次攻击附加智力的30%作为额外伤害。 施法间隔:2秒 施法消耗:15点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','2','3','
	每次攻击附加智力的45%作为额外伤害。 施法间隔:1秒 施法消耗:15点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','2','4','
	每次攻击附加智力的60%作为额外伤害。 施法间隔:0秒 施法消耗:15点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','3','1','
	持续1秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','3','2','
	持续2秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','3','3','
	持续3秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','3','4','
	持续4秒。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','4','1','
	持续3秒。 施法间隔:120秒 施法消耗:250点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','4','2','
	持续4秒。 施法间隔:120秒 施法消耗:350点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('013','4','3','
	持续5秒。 施法间隔:120秒 施法消耗:450点魔法
	');


insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('014','1','大自然的掩护','skill0141','T','
	改变一个友方单位的外形，将他掩于树林之中。他进入隐身状态，直到他攻击、使用技能或者离开树林周围。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('014','2','森林之眼','skill0142','E','
	让树精卫士和目标树木心灵相通。森林之眼视野600/630/660/690，并具有真视能力
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('014','3','活体护甲','skill0143','V','
	为友方单位或建筑物创造一个魔法藤蔓护甲，保护他不受伤害并医治伤口，持续40秒。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('014','4','疯狂生长','skill0144','R','
	让树精卫士四周的藤蔓和树枝疯狂地生长，缠绕周围的敌方地面单位。被缠绕的单位受到95点/秒的伤害，不能移动和攻击（可以使用技能）。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','1','1','
	持续15秒。 施法间隔:3秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','1','2','
	持续30秒。 施法间隔:3秒 施法消耗:80点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','1','3','
	持续45秒。 施法间隔:3秒 施法消耗:70点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','1','4','
	持续60秒。 施法间隔:3秒 施法消耗:60点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','2','1','
	冷却时间300秒。 施法间隔:300秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','2','2','
	冷却时间225秒。 施法间隔:225秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','2','3','
	冷却时间150秒。 施法间隔:150秒 施法消耗:20点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','2','4','
	冷却时间75秒。 施法间隔:75秒 施法消耗:25点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','3','1','
	增加3点的护甲，提升1点/秒的生命回复速度。 施法间隔:7秒 施法消耗:30点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','3','2','
	增加6点的护甲，提升2点/秒的生命回复速度。 施法间隔:7秒 施法消耗:35点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','3','3','
	增加9点的护甲，提升3点/秒的生命回复速度。 施法间隔:7秒 施法消耗:40点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','3','4','
	增加12点的护甲，提升4点/秒的生命回复速度。 施法间隔:7秒 施法消耗:40点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','4','1','
	持续3秒。 施法间隔:160秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','4','2','
	持续4秒。 施法间隔:160秒 施法消耗:175点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('014','4','3','
	持续5秒。 施法间隔:160秒 施法消耗:200点魔法
	');

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('015','1','憎恶','skill0151','F','
	将谜团的憎恶集中于一个目标，造成多次的伤害和晕眩。施法距离：600
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('015','2','转化','skill0152','C','
	将将任何的非英雄单位撕裂成3个小精神体，同时杀死被释放这个技能的单位。小精神体攻击6次之后能自我分裂。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('015','3','午夜凋零','skill0153','D','
	谜团用黑暗魔法制造出一块死亡区域，持续8秒，任何进入此区域的敌方单位都将受到伤害。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('015','4','黑洞','skill0154','B','
	召唤来自最黑暗的深渊的力量，创造一个能够牵引附近敌方单位的引力漩涡，持续4秒。被牵引的单位无法移动、攻击、使用技能和物品。施法距离：250 需要持续施法牵制效果无视魔法免疫
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','1','1','
	每2秒造成30点的伤害和1秒的晕眩，持续2秒。 施法间隔:15秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','1','2','
	每2秒造成40点的伤害和1秒的晕眩，持续4秒。 施法间隔:15秒 施法消耗:130点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','1','3','
	每2秒造成65点的伤害和1秒的晕眩，持续4秒。 施法间隔:15秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','1','4','
	每2秒造成80点的伤害和1秒的晕眩，持续6秒。 施法间隔:15秒 施法消耗:160点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','2','1','
	每个精神体拥有200点的生命，16-24点的攻击力和50%的魔法抗性。持续时间30秒。 施法间隔:15秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','2','2','
	每个精神体拥有200点的生命，24-32点的攻击力和50%的魔法抗性。持续时间30秒。 施法间隔:15秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','2','3','
	每个精神体拥有200点的生命，34-42点的攻击力和50%的魔法抗性。持续时间30秒。 施法间隔:15秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','2','4','
	每个精神体拥有200点的生命，43-51点的攻击力和50%的魔法抗性。持续时间30秒。 施法间隔:15秒 施法消耗:125点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','3','1','
	每秒流失相当于最大生命值3%的生命。 施法间隔:25秒 施法消耗:95点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','3','2','
	每秒流失相当于最大生命值4%的生命。 施法间隔:25秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','3','3','
	每秒流失相当于最大生命值5%的生命。 施法间隔:25秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','3','4','
	每秒流失相当于最大生命值6%的生命。 施法间隔:25秒 施法消耗:140点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','4','1','
	对中心/边缘的敌方单位造成60/30点/秒的伤害，持续4秒。 施法间隔:200秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','4','2','
	对中心/边缘的敌方单位造成100/50点/秒的伤害，持续4秒。 施法间隔:190秒 施法消耗:300点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('015','4','3','
	对中心/边缘的敌方单位造成140/70点/秒的伤害，持续4秒。 施法间隔:180秒 施法消耗:400点魔法
	');

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('016','1','冲击波','skill0161','T','
	将正相魔法积聚成球，每秒向其中凝聚100点的伤害。当能量球被释放的时候（到达积聚极限或者Ezalor进行其它动作），将对一条直线上的敌方单位造成伤害。射程与凝聚时间成正比。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('016','2','法力流失','skill0162','E','
	弱化目标魔法与肉体之间的契约，如果目标在期间移动，他将损失正比于移动距离的魔法，而且，如果目标i期间损失了所有的魔法，他会为了自行恢复魔法而不得不麻痹数秒。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('016','3','查克拉魔法','skill0163','C','
	打开一个友军为的穴道，使其查克拉澎湃流出。施法距离：500
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('016','4','灵魂形态','skill0164','F','
	变身为灵魂形态。该状态下光法释放冲击波后可以进行其他动作，而且积聚能量的过程不会被打断。同时可以使用致盲之光(B)和召回(R)。致盲之光可以击退650范围内的敌人250距离，80%几率攻击落空，施法距离600；召回可以传送地图任意位置的队友到光法身边，被召唤队友在施法期间不能被攻击。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','1','1','
	最大凝聚时间2秒，最大射程500。 施法间隔:10秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','1','2','
	最大凝聚时间3秒，最大射程600。 施法间隔:10秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','1','3','
	最大凝聚时间4秒，最大射程700。 施法间隔:10秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','1','4','
	最大凝聚时间5秒，最大射程800。 施法间隔:10秒 施法消耗:150点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','2','1','
	损失相当于移动距离3.5%魔法，1.25秒的麻痹。持续时间5秒。 施法间隔:20秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','2','2','
	损失相当于移动距离4%魔法，1.5秒的麻痹。持续时间6秒。 施法间隔:20秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','2','3','
	损失相当于移动距离4.5%魔法，1.75秒的麻痹。持续时间7秒。 施法间隔:20秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','2','4','
	损失相当于移动距离5%魔法，2秒的麻痹。持续时间8秒。 施法间隔:20秒 施法消耗:75点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','3','1','
	恢复75点的魔法。 施法间隔:19秒 施法消耗:40点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','3','2','
	恢复150点的魔法。 施法间隔:18秒 施法消耗:55点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','3','3','
	恢复225点的魔法。 施法间隔:17秒 施法消耗:70点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','3','4','
	恢复300点的魔法。 施法间隔:16秒 施法消耗:85点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','4','1','
	变身持续时间40秒；致盲之光攻击落空效果持续时间3秒；召回施法吟唱时间为5秒。变身魔法消耗100点，施法间隔80秒；致盲之光魔法消耗50点，施法间隔20秒；召回魔法消耗100点，施法间隔40秒。 施法间隔:40秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','4','2','
	变身持续时间40秒；致盲之光攻击落空效果持续时间4秒；召回施法吟唱时间为4秒。变身魔法消耗100点，施法间隔70秒；致盲之光魔法消耗50点，施法间隔20秒；召回魔法消耗100点，施法间隔40秒。 施法间隔:40秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('016','4','3','
	变身持续时间40秒；致盲之光攻击落空效果持续时间5秒；召回施法吟唱时间为3秒。变身魔法消耗100点，施法间隔60秒；致盲之光魔法消耗50点，施法间隔20秒；召回魔法消耗100点，施法间隔40秒。 施法间隔:40秒 施法消耗:100点魔法
	');

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('017','1','震撼大地','skill0171','E','
	猛击地面，对周围的敌方地面单位造成伤害，降低他们的移动速度，持续4秒。作用范围：365 
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('017','2','超强力量','skill0172','V','
	让熊战士变得狂暴，急切想把这股力量宣泄到目标身上，大幅提升攻击速度。持续15秒，或者维持5次攻击。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('017','3','怒意狂击','skill0173','W','
	每次攻击撕开目标的伤口，使其受到比上次攻击更多的伤害。撕裂伤口状态持续5秒。重击和致命一击在启动时会中断撕裂伤口状态。 法球效果无视魔法免疫
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('017','4','激怒','skill0174','R','
	根据熊战士的当前生命值，在攻击中附加额外的伤害。 英雄攻击 普通伤害
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','1','1','
	造成70点的伤害，降低25%的移动速度。 施法间隔:7秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','1','2','
	造成120点的伤害，降低35%的移动速度。 施法间隔:7秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','1','3','
	造成170点的伤害，降低45%的移动速度。 施法间隔:7秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','1','4','
	造成220点的伤害，降低55%的移动速度。 施法间隔:7秒 施法消耗:75点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','2','1','
	提升100%的攻击速度。 施法间隔:10秒 施法消耗:45点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','2','2','
	提升200%的攻击速度。 施法间隔:10秒 施法消耗:55点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','2','3','
	提升300%的攻击速度。 施法间隔:10秒 施法消耗:65点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','2','4','
	提升400%的攻击速度。 施法间隔:10秒 施法消耗:75点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','3','1','
	目标每次被攻击所受伤害递增6点。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','3','2','
	目标每次被攻击所受伤害递增12点。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','3','3','
	目标每次被攻击所受伤害递增18点。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','3','4','
	目标每次被攻击所受伤害递增24点。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','4','1','
	在攻击中附加相当于当前生命值4%的伤害。持续时间15秒。 施法间隔:0秒 施法消耗:25点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','4','2','
	在攻击中附加相当于当前生命值5%的伤害。持续时间15秒。 施法间隔:0秒 施法消耗:25点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('017','4','3','
	在攻击中附加相当于当前生命值6%的伤害。持续时间15秒。 施法间隔:0秒 施法消耗:25点魔法
	');

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('018','1','火焰爆轰','skill0181','F','
	发出火焰轰炸一个目标，造成伤害并晕眩1.5秒。（括号中为升级了多重施法的数值）
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('018','2','引燃','skill0182','G','
	向目标投掷挥发性的化学物品并引燃（括号中为升级了多重施法的数值），对目标（四周150/四周300/四周450）的敌方单位造成影响，他们将持续受到伤害，移动速度也因此降低。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('018','3','嗜血术','skill0183','B','
	使一个友方单位变得狂热，提升攻击速度和移动速度。（括号中为升级了多重施法的数值）
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('018','4','多重施法','skill0184','C','
	多重施法让食人魔魔法师能更快更有效地使用他的技能。（括号内为拥有Aghanim的神杖时的数值。） 火焰爆轰 - 每级降低3秒的冷却时间，魔法消耗上升30/80/110点。嗜血术 - 每级降低5秒的冷却时间。引燃 - 每次扩大150的作用范围。有20%（30%）的几率连续施展2（3）/3（4）/4（5）次法术
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','1','1','
	造成75点的伤害并晕眩1.5秒。 施法间隔:15（12/9/6）秒 施法消耗:75（105/155/185）
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','1','2','
	造成125点的伤害并晕眩1.5秒。 施法间隔:15（12/9/6）秒 施法消耗:85（115/165/195）
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','1','3','
	造成175点的伤害并晕眩1.5秒。 施法间隔:15（12/9/6）秒 施法消耗:95（125/175/205）
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','1','4','
	造成275点的伤害并晕眩1.5秒。 施法间隔:15（12/9/6）秒 施法消耗:105（135/185/215）
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','2','1','
	引燃一个目标（四周150/四周300/四周450）的敌方单位，造成10点/秒的伤害，同时降低10%的移动速度。持续时间7.5秒。 施法间隔:15秒 施法消耗:95点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','2','2','
	引燃一个目标（四周150/四周300/四周450）的敌方单位，造成20点/秒的伤害，同时降低15%的移动速度。持续时间7.5秒。 施法间隔:15秒 施法消耗:105点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','2','3','
	引燃一个目标（四周150/四周300/四周450）的敌方单位，造成30点/秒的伤害，同时降低20%的移动速度。持续时间7.5秒。 施法间隔:15秒 施法消耗:115点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','2','4','
	引燃一个目标（四周150/四周300/四周450）的敌方单位，造成40点/秒的伤害，同时降低25%的移动速度。持续时间7.5秒。 施法间隔:15秒 施法消耗:125点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','3','1','
	提升一个友方单位20%的攻击速度和6%的移动速度。持续时间30秒。 施法间隔:20（15/10/5）秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','3','2','
	提升一个友方单位30%的攻击速度和9%的移动速度。持续时间30秒。 施法间隔:20（15/10/5）秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','3','3','
	提升一个友方单位40%的攻击速度和12%的移动速度。持续时间30秒。 施法间隔:20（15/10/5）秒 施法消耗:75点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','3','4','
	提升一个友方单位50%的攻击速度和15%的移动速度。持续时间30秒。 施法间隔:20（15/10/5）秒 施法消耗:75点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','4','1','
	火焰爆轰的冷却时间降低至12秒，魔法消耗上升30点；嗜血术的冷却时间降低至15秒；引燃将影响150范围内的敌方单位；有20%的几率连续施展两次法术，有Aghanim的神杖时，有30%的几率连续施展三次法术。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','4','2','
	火焰爆轰的冷却时间降低至9秒，魔法消耗上升80点；嗜血术的冷却时间降低至10秒；引燃将影响300范围内的敌方单位；有20%的几率连续施展三次法术，有Aghanim的神杖时，有30%的几率连续施展四次法术。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('018','4','3','
	火焰爆轰的冷却时间降低至6秒，魔法消耗上升110点；嗜血术的冷却时间降低至5秒；引燃将影响450范围内的敌方单位；有20%的几率连续施展四次法术，有Aghanim的神杖时，有30%的几率连续施展五次法术。
	');

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('019','1','激光','skill0191','E','
	向目标发射一束激光，造成伤害，并致盲他和他周围的单位9（18）秒。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('019','2','热导飞弹','skill0192','T','
	修补匠发射热导飞弹，攻击距离最近的英雄。英雄必须在己方视野内。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('019','3','机器人的进军','skill0193','C','
	召唤一大群机器人对附近的敌方单位造成伤害。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('019','4','再装填','skill0194','R','
	修补匠重新装填他的武器，立即刷新所有技能和物品的冷却时间。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','1','1','
	造成80点的伤害，10%的几率攻击落空。 施法间隔:14秒 施法消耗:95点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','1','2','
	造成160点的伤害，15%的几率攻击落空。 施法间隔:14秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','1','3','
	造成240点的伤害，20%的几率攻击落空。 施法间隔:14秒 施法消耗:145点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','1','4','
	造成320点的伤害，25%的几率攻击落空。 施法间隔:14秒 施法消耗:170点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','2','1','
	攻击1个英雄，造成100点的伤害。 施法间隔:25秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','2','2','
	攻击1个英雄，造成175点的伤害。 施法间隔:25秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','2','3','
	攻击2个英雄，造成250点的伤害。 施法间隔:25秒 施法消耗:160点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','2','4','
	攻击2个英雄，造成325点的伤害。 施法间隔:25秒 施法消耗:180点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','3','1','
	每个机器人造成16点的伤害。 施法间隔:35秒 施法消耗:135点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','3','2','
	每个机器人造成24点的伤害。 施法间隔:35秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','3','3','
	每个机器人造成32点的伤害。 施法间隔:35秒 施法消耗:165点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','3','4','
	每个机器人造成40点的伤害。 施法间隔:35秒 施法消耗:190点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','4','1','
	3秒的装填时间。 施法间隔:0秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','4','2','
	2秒的装填时间。 施法间隔:0秒 施法消耗:250点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('019','4','3','
	1秒的装填时间。 施法间隔:0秒 施法消耗:350点魔法
	');

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('020','1','发芽','skill0201','T','
	使目标周围的树木发芽生长，将他围在中央。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('020','2','传送','skill0202','R','
	将自己传送至地图上任意已探索的地区，目标区域的魔法效果会泄露你的意图。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('020','3','自然之力','skill0203','F','
	将一定区域内的树木转化为树人。树人拥有550点的生命，21-23点的攻击力。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('020','4','自然之怒','skill0204','W','
	释放伤害性的能量，随机打击地图上己方视野内的目标。每攻击一个目标，伤害递增7%。（括号内为拥有Aghanim的神杖时的数值。）
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','1','1','
	持续3秒。 施法间隔:11秒 施法消耗:115点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','1','2','
	持续3.75秒。 施法间隔:11秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','1','3','
	持续4.5秒。 施法间隔:11秒 施法消耗:165点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','1','4','
	持续5.25秒。 施法间隔:11秒 施法消耗:205点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','2','1','
	消耗55点的魔法。冷却时间60秒。 施法间隔:60秒 施法消耗:55点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','2','2','
	消耗45点的魔法。冷却时间50秒。 施法间隔:50秒 施法消耗:45点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','2','3','
	消耗40点的魔法。冷却时间40秒。 施法间隔:40秒 施法消耗:40点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','2','4','
	消耗35点的魔法。冷却时间30秒。 施法间隔:30秒 施法消耗:35点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','3','1','
	将一定区域内的树木转化为2个树人。持续60秒。 施法间隔:37秒 施法消耗:160点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','3','2','
	将一定区域内的树木转化为3个树人。持续60秒。 施法间隔:37秒 施法消耗:160点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','3','3','
	将一定区域内的树木转化为4个树人。持续60秒。 施法间隔:37秒 施法消耗:160点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','3','4','
	将一定区域内的树木转化为5个树人。持续60秒。 施法间隔:37秒 施法消耗:160点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','4','1','
	对第一目标造成140点（155点）的伤害。最多攻击12个目标。 施法间隔:90秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','4','2','
	对第一目标造成180点（210点）的伤害。最多攻击14个目标。 施法间隔:60秒 施法消耗:175点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('020','4','3','
	对第一目标造成225点（275点）的伤害。最多攻击16个目标。 施法间隔:60秒 施法消耗:200点魔法
	');

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('021','1','灵魂之矛','skill0211','T','
	掷出一支附有精神力量的矛，对目标造成伤害并晕眩0.01秒，同时降低其攻击速度和移动速度，持续3秒。投掷出长矛后会产生一个拥有本体25%攻击力的幻想对目标进行攻击。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('021','2','神出鬼没','skill0212','W','
	幻影长矛手进入隐形状态，并留下一个幻象来迷惑敌人。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('021','3','并列','skill0213','X','
	幻影长矛手在攻击中有一定几率制造一个幻象。幻象的攻击力为本体的25%，受到300%的伤害，持续15秒。最多制造8个幻象。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('021','4','幻化之锋','skill0214','D','
	让幻影长矛手能提高一定的魔法抗性，提升在攻击中制造幻象的几率。同时，幻象本身也能以一定的几率制造幻象。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','1','1','
	造成100点伤害，并降低10%攻击速度和移动速度。 施法间隔:9秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','1','2','
	造成150点伤害，并降低20%攻击速度和移动速度。 施法间隔:9秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','1','3','
	造成215点伤害，并降低30%攻击速度和移动速度。 施法间隔:9秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','1','4','
	造成310点伤害，并降低40%攻击速度和移动速度。 施法间隔:9秒 施法消耗:140点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','2','1','
	消耗150点的魔法，持续20秒。 施法间隔:30秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','2','2','
	消耗120点的魔法，持续20秒。 施法间隔:25秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','2','3','
	消耗90点的魔法，持续20秒。 施法间隔:20秒 施法消耗:90点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','2','4','
	消耗60点的魔法，持续20秒。 施法间隔:15秒 施法消耗:60点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','3','1','
	有3%的几率制造一个幻象。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','3','2','
	有6%的几率制造一个幻象。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','3','3','
	有9%的几率制造一个幻象。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','3','4','
	有12%的几率制造一个幻象。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','4','1','
	让幻影长矛手提高10%的魔法抗性，提高2%的幻象生成几率，同时，幻象在攻击中有3%的几率制造幻象。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','4','2','
	让幻影长矛手提高15%的魔法抗性，提高4%的幻象生成几率，同时，幻象在攻击中有5%的几率制造幻象。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('021','4','3','
	让幻影长矛手提高20%的魔法抗性，提高6%的幻象生成几率，同时，幻象在攻击中有7%的几率制造幻象。
	');

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('022','1','山崩','skill0221','V','
	用岩石轰炸一个区域，造成伤害并晕眩2秒。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('022','2','投掷','skill0222','T','
	山岭巨人随机抓起275范围内的一个单位，将他投向一个目标。落地以后对附近的敌方单位造成伤害，被投掷的单位本身受到20%（35%/50%/65%，随长大的等级而上升）的伤害。不能投掷真龙形态下的龙骑士。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('022','3','崎岖外表','skill0223','C','
	山岭巨人的身体由坚硬的花岗岩组成，任何对他进行近身攻击的单位都有可能受到伤害，并晕眩1.2秒。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('022','4','长大！','skill0224','W','
	大幅增加山岭巨人的体型和攻击力，攻击速度却因此降低。大幅增加山岭巨人的体型和攻击力，攻击速度却因此降低。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','1','1','
	造成100点的伤害。 施法间隔:20秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','1','2','
	造成180点的伤害。 施法间隔:20秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','1','3','
	造成260点的伤害。 施法间隔:20秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','1','4','
	造成300点的伤害。 施法间隔:20秒 施法消耗:120点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','2','1','
	投掷距离500，造成75点的伤害。 施法间隔:15秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','2','2','
	投掷距离700，造成150点的伤害。 施法间隔:15秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','2','3','
	投掷距离900，造成225点的伤害。 施法间隔:15秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','2','4','
	投掷距离1100，造成300点的伤害。 施法间隔:15秒 施法消耗:120点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','3','1','
	对山岭巨人进行近身攻击的单位有6%的几率受到25点的伤害，并晕眩1.2秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','3','2','
	对山岭巨人进行近身攻击的单位有12%的几率受到35点的伤害，并晕眩1.2秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','3','3','
	对山岭巨人进行近身攻击的单位有18%的几率受到45点的伤害，并晕眩1.2秒。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','3','4','
	对山岭巨人进行近身攻击的单位有24%的几率受到55点的伤害，并晕眩1.2秒。
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','4','1','
	增加40点的攻击力，降低20%的攻击速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','4','2','
	增加80点的攻击力，降低35%的攻击速度。
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('022','4','3','
	增加120点的攻击力，降低50%的攻击速度。
	');

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('023','1','埋布地雷','skill0231','E','
	埋布地雷，当有敌方单位靠近时会自动爆炸，对范围内的单位和建筑造成伤害。离爆心较远的单位受到的伤害也较少。同一时间内最多只能埋布15颗的地雷。飘浮和飞行单位不会触发地雷。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('023','2','麻痹陷阱','skill0232','T','
	布置一个麻痹陷阱，当有敌方单位靠近时会自动触发，造成晕眩。启动延迟2秒，启动范围200，有效范围450。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('023','3','自杀攻击','skill0233','G','
	牺牲小我而成全大我，对一定范围内的非远古单位造成巨大的伤害。距离爆心较远的单位，受到的伤害也较少。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('023','4','遥控炸弹','skill0234','R','
	布置一颗不可见的炸弹，只有在你的控制下才会引爆。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','1','1','
	对200/500范围内的敌方单位造成250/150点的伤害。 施法间隔:25秒 施法消耗:120点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','1','2','
	对200/500范围内的敌方单位造成350/200点的伤害。 施法间隔:20秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','1','3','
	对200/500范围内的敌方单位造成450/250点的伤害。 施法间隔:15秒 施法消耗:175点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','1','4','
	对200/500范围内的敌方单位造成550/300点的伤害。 施法间隔:12秒 施法消耗:205点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','2','1','
	晕眩3秒。 施法间隔:20秒 施法消耗:80点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','2','2','
	晕眩4秒。 施法间隔:16秒 施法消耗:110点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','2','3','
	晕眩5秒。 施法间隔:13秒 施法消耗:140点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','2','4','
	晕眩6秒。 施法间隔:10秒 施法消耗:160点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','3','1','
	对200/500范围内的目标造成650/350点的伤害。 施法间隔:75秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','3','2','
	对200/500范围内的目标造成850/400点的伤害。 施法间隔:75秒 施法消耗:125点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','3','3','
	对200/500范围内的目标造成1150/450点的伤害。 施法间隔:75秒 施法消耗:150点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','3','4','
	对200/500范围内的目标造成1550/500点的伤害。 施法间隔:75秒 施法消耗:175点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','4','1','
	引爆后造成300点的伤害。 施法间隔:10秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','4','2','
	引爆后造成450点的伤害。 施法间隔:10秒 施法消耗:240点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('023','4','3','
	引爆后造成600点的伤害。 施法间隔:10秒 施法消耗:300点魔法
	');

insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('024','1','赎罪','skill0241','E','
	使一个敌方单位因他的罪孽而接受惩罚，降低他的移动速度，并且在被攻击时受到更多的伤害，持续7秒。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('024','2','忠诚考验','skill0242','T','
	如果施放在敌人身上，会根据敌人的忠诚造成不定量的伤害。如果施放在友方英雄身上，会在一段时间后将其传送回本方基地。立即传送已被圣骑士劝化的单位。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('024','3','神圣劝化','skill0243','R','
	取得一个敌方非远古、镜像、死灵书单位的控制权。
	');
insert into skill(hero_no, skill_no, skill_name, skill_pic, skill_shortcut, skill_depict)
	values('024','4','上帝之手','skill0244','D','
	完全恢复皈依你的单位，治疗地图上所有的友方英雄。
	');


insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','1','1','
	移动速度降低8%，受到7%的额外伤害。 施法间隔:14秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','1','2','
	移动速度降低16%，受到14%的额外伤害。 施法间隔:14秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','1','3','
	移动速度降低24%，受到21%的额外伤害。 施法间隔:14秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','1','4','
	移动速度降低32%，受到28%的额外伤害。 施法间隔:14秒 施法消耗:100点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','2','1','
	对敌方单位造成50-100点的伤害；在6秒内将友方英雄传送回基地；立即传送已被Chen劝化的单位。 施法间隔:30秒 施法消耗:175点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','2','2','
	对敌方单位造成100-200点的伤害；在5秒内将友方英雄传送回基地；立即传送已被Chen劝化的单位。 施法间隔:30秒 施法消耗:175点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','2','3','
	对敌方单位造成150-300点的伤害；在4秒内将友方英雄传送回基地；立即传送已被Chen劝化的单位。 施法间隔:30秒 施法消耗:175点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','2','4','
	对敌方单位造成200-400点的伤害；在3秒内将友方英雄传送回基地；立即传送已被Chen劝化的单位。 施法间隔:30秒 施法消耗:175点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','3','1','
	上限1个，被劝化的单位获得75点额外的生命 施法间隔:30秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','3','2','
	上限1个，被劝化的单位获得150点额外的生命 施法间隔:30秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','3','3','
	上限2个，被劝化的单位获得225点额外的生命 施法间隔:30秒 施法消耗:100点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','3','4','
	上限3个，被劝化的单位获得300点额外的生命 施法间隔:30秒 施法消耗:100点魔法
	');

insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','4','1','
	治疗地图上所有的友方英雄200点的生命，完全恢复皈依你的单位。 施法间隔:120秒 施法消耗:200点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','4','2','
	治疗地图上所有的友方英雄300点的生命，完全恢复皈依你的单位。 施法间隔:120秒 施法消耗:300点魔法
	');
insert into level(hero_no, skill_no, level_no, level_depict)
	values('024','4','3','
	治疗地图上所有的友方英雄400点的生命，完全恢复皈依你的单位。 施法间隔:120秒 施法消耗:400点魔法
	');
