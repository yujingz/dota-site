package dota.action;


import java.util.List;


import dao.DaoManager;
import dao.dota.ACommentDao;
import dota.bean.AComment;

public class SearchAComment
{
	public List<AComment> getACommentsInfo(String objectNo, String category)
	{
		DaoManager dm = DaoManager.getInstance();
		ACommentDao ad = dm.getDao(ACommentDao.class);
		List<AComment> list = null;
		
		try
		{
			dm.begin();
			list = ad.getACommentsByObjectNoAndCategory(objectNo, category);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return list;
	}
	
	public boolean addAComment(AComment aComment)
	{
		DaoManager dm = DaoManager.getInstance();
		ACommentDao ad = dm.getDao(ACommentDao.class);
		int insertRows = 0;
		try
		{
			dm.begin();
			insertRows = ad.addAComment(aComment);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		
		return (insertRows==0 ? false : true);
	}
}
