package dota.action;


import dao.DaoManager;
import dao.dota.ItemDao;
import dota.bean.Item;

public class SearchItem 
{
	public Item getItemInfo(String itemNo)
	{
		DaoManager dm = DaoManager.getInstance();
		ItemDao ItemDao = dm.getDao(ItemDao.class);
		Item i = null;
		
		try
		{
			dm.begin();
			i=ItemDao.getItemByItemNo(itemNo); 
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return i;
	}
}
