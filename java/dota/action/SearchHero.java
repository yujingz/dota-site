package dota.action;


import dao.DaoManager;
import dao.dota.HeroDao;
import dota.bean.Hero;

public class SearchHero 
{
	public Hero getHeroInfo(String heroNo)
	{
		DaoManager dm = DaoManager.getInstance();
		HeroDao heroDao = dm.getDao(HeroDao.class);
		Hero h = null;
		
		try
		{
			dm.begin();
			h=heroDao.getHeroByHeroNo(heroNo); 
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return h;
	}
}
