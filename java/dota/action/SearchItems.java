package dota.action;


import java.util.List;

import dao.DaoManager;
import dao.dota.ItemsDao;
import dota.bean.Item;

public class SearchItems
{
	public List<Item> getItemsInfo(String shopNo)
	{
		DaoManager dm = DaoManager.getInstance();
		ItemsDao hd = dm.getDao(ItemsDao.class);
		List<Item> list = null;
		
		try
		{
			dm.begin();
			list = hd.getAllItemsByShopNo(shopNo);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return list;
	}
}
