package dota.action;


import java.util.List;

import dao.DaoManager;
import dao.dota.HeroesDao;
import dota.bean.Hero;

public class SearchHeroes
{
	public List<Hero> getHeroesInfo(String tavernNo)
	{
		DaoManager dm = DaoManager.getInstance();
		HeroesDao hd = dm.getDao(HeroesDao.class);
		List<Hero> list = null;
		
		try
		{
			dm.begin();
			list = hd.getAllHeroesByTavernNo(tavernNo);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return list;
	}
}
