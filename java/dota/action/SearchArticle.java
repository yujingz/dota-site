package dota.action;


import java.util.List;

import dao.DaoManager;
import dao.dota.ArticleDao;
import dota.bean.Article;


public class SearchArticle
{
	public List<Article> getArticlesInfo(String pageNo)
	{
		DaoManager dm = DaoManager.getInstance();
		ArticleDao articleDao = dm.getDao(ArticleDao.class);
		List<Article> list = null;
		
		try
		{
			dm.begin();
			list =articleDao.getArticlesByPageNo(pageNo); 
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return list;
	}
	
	public Article getArticleInfo(String articleNo)
	{
		DaoManager dm = DaoManager.getInstance();
		ArticleDao ArticleDao = dm.getDao(ArticleDao.class);
		Article a = null;
		
		try
		{
			dm.begin();
			a=ArticleDao.getArticleByArticleNo(articleNo);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return a;
	}
}
