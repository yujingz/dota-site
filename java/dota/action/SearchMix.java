package dota.action;

import java.util.List;
import dao.DaoManager;
import dao.dota.MixDao;
import dota.bean.Mix;

public class SearchMix
{
	public List<Mix> getMixInfo(String itemNo)
	{
		DaoManager dm = DaoManager.getInstance();
		MixDao md = dm.getDao(MixDao.class);
		List<Mix> list = null;
		
		try
		{
			dm.begin();
			list = md.getItemsByItemNo(itemNo);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return list;
	}
}
