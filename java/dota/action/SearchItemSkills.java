package dota.action;

import java.util.List;
import dao.DaoManager;
import dao.dota.ItemSkillDao;
import dota.bean.ItemSkill;

public class SearchItemSkills
{
	public List<ItemSkill> getItemSkillsInfo( String itemNo)
	{
		DaoManager dm = DaoManager.getInstance();
		ItemSkillDao id = dm.getDao(ItemSkillDao.class);
		List<ItemSkill> list = null;
		
		try
		{
			dm.begin();
			list = id.getItemSkillByItemNo(itemNo);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return list;
	}
}
