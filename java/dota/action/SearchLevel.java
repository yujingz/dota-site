package dota.action;


import java.util.List;

import dao.DaoManager;
import dao.dota.LevelDao;
import dota.bean.Level;

public class SearchLevel
{
	public List<Level> getLevelInfo(String heroNo,String skillNo)
	{
		DaoManager dm = DaoManager.getInstance();
		LevelDao ld = dm.getDao(LevelDao.class);
		List<Level> list = null;
		
		try
		{
			dm.begin();
			list = ld.getAllLevelInfoByHeroNoAndSkillNo(heroNo, skillNo);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return list;
	}
}
