package dota.action;


import java.util.List;

import dao.DaoManager;
import dao.dota.SkillDao;
import dota.bean.Skill;

public class SearchSkill
{
	public List<Skill> getSkillInfo(String heroNo)
	{
		DaoManager dm = DaoManager.getInstance();
		SkillDao skillDao = dm.getDao(SkillDao.class);
		List<Skill> list = null;
		
		try
		{
			dm.begin();
			list = skillDao.getAllSkillsByHeroNo(heroNo);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return list;
	}
}
