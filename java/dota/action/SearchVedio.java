package dota.action;


import dao.DaoManager;
import dao.dota.VedioDao;
import dota.bean.Vedio;

public class SearchVedio
{
	public Vedio getVedioInfo(String vedioNo)
	{
		DaoManager dm = DaoManager.getInstance();
		VedioDao vedioDao = dm.getDao(VedioDao.class);
		Vedio v = null;
		
		try
		{
			dm.begin();
			v=vedioDao.getVediosByVedioNo(vedioNo);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return v;
	}
}
