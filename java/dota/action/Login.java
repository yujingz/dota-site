package dota.action;


import dao.DaoManager;
import dao.dota.MemberDao;
import dota.bean.Member;

public class Login 
{
	public Member memberLogin(String memberNo, String password)
	{
		DaoManager dm = DaoManager.getInstance();
		MemberDao memberDao = dm.getDao(MemberDao.class);
		Member member = null;
		
		try
		{
			dm.begin();
			member = memberDao.getMemberByMemberNoAndPassword(memberNo, password);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return member;
	}
	
	public int memberRegister(Member member)
	{
		DaoManager dm = DaoManager.getInstance();
		MemberDao memberDao = dm.getDao(MemberDao.class);
		int insertRows = 0;
		
		try
		{
			dm.begin();
			insertRows = memberDao.addNewMember(member);
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return insertRows;
	}
}
