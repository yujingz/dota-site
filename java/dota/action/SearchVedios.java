package dota.action;


import java.util.List;

import dao.DaoManager;
import dao.dota.VedioDao;
import dota.bean.Vedio;

public class SearchVedios
{
	public List<Vedio> getVediosInfo(String pageNo)
	{
		DaoManager dm = DaoManager.getInstance();
		VedioDao vedioDao = dm.getDao(VedioDao.class);
		List<Vedio> list = null;
		
		try
		{
			dm.begin();
			list =vedioDao.getVediosByPageNo(pageNo); 
			dm.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			dm.end();
		}
		return list;
	}
}
