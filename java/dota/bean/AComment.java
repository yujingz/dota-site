package dota.bean;

import java.util.Date;

public class AComment {
	String commentNo;
	String objectNo;
	String memPetname;
	Date date;
	String content;
	String category;
	
	public AComment(){}
	
	public String getCommentNo()
	{
		return commentNo;
	}
	
	public void setACommentNo(String commentNo)
	{
		this.commentNo=commentNo;
	}
	
	public String getObjectNo()
	{
		return objectNo;
	}
	
	public void setObjectNo(String objectNo)
	{
		this.objectNo=objectNo;
	}
	
	public String getMemPetname()
	{
		return memPetname;
	}
	
	public void setMemPetname(String memPetname)
	{
		this.memPetname=memPetname;
	}
	
	
	public Date getDate()
	{
		return date;
	}
	
	public void setDate(Date date)
	{
		this.date=date;
	}
	
	public String getContent()
	{
		return content;
	}
	
	public void setContent(String content)
	{
		this.content=content;
	}
	
	public String getCategory()
	{
		return category;
	}
	
	public void setCategory(String category)
	{
		this.category=category;
	}

}
