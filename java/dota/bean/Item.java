package dota.bean;

public class Item {
	String shopNo;
	String itemNo;
	String itemName;
	String itemPic;
	String itemIntro;
	String itemPrice;
	String itemDepict;
	
	public Item(){}
	
	public String getshopNo()
	{
		return shopNo;
	}
	
	public void setShopNo(String shopNo)
	{
		this.shopNo=shopNo;
	}
	
	public String getItemNo()
	{
		return itemNo;
	}
	
	public void setItemNo(String itemNo)
	{
		this.itemNo=itemNo;
	}
	public String getItemName()
	{
		return itemName;
	}
	
	public void setItemName(String itemName)
	{
		this.itemName=itemName;
	}
	public String getItemPic()
	{
		return itemPic;
	}
	
	public void setItemPic(String itemPic)
	{
		this.itemPic=itemPic;
	}
	public String getItemPrice()
	{
		return itemPrice;
	}
	
	public void setItemPrice(String itemPrice)
	{
		this.itemPrice=itemPrice;
	}
	public String getItemDepict()
	{
		return itemDepict;
	}
	
	public void setItemDepict(String itemDepict)
	{
		this.itemDepict=itemDepict;
	}
}
