package dota.bean;

public class Hero {
	String tavernNo;
	String heroNo;
	String heroName;
	String heroPic;
	String heroIntro;
	
	public Hero(){}
	
	public String getTavernNo()
	{
		return tavernNo;
	}
	
	public void setTavernNo(String tavernNo)
	{
		this.tavernNo=tavernNo;
	}
	
	public String getHeroNo()
	{
		return heroNo;
	}
	
	public void setHeroNo(String heroNo)
	{
		this.heroNo=heroNo;
	}
	public String getHeroName()
	{
		return heroName;
	}
	
	public void setHeroName(String heroName)
	{
		this.heroName=heroName;
	}
	public String getHeroPic()
	{
		return heroPic;
	}
	
	public void setHeroPic(String heroPic)
	{
		this.heroPic=heroPic;
	}
	public String getHeroIntro()
	{
		return heroIntro;
	}
	
	public void setHeroIntro(String heroIntro)
	{
		this.heroIntro=heroIntro;
	}
}
