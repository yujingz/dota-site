package dota.bean;

public class ItemSkill {
	String shopNo;
	String itemNo;
	String itemSkillNo;
	String itemSkillName;
	String itemSkillDepict;
	
	public ItemSkill(){}
	
	public String getShopNo()
	{
		return shopNo;
	}
	
	public void setShopNo(String shopNo)
	{
		this.shopNo=shopNo;
	}
	public String getItemNo()
	{
		return itemNo;
	}
	
	public void setItemNo(String itemNo)
	{
		this.itemNo=itemNo;
	}
	
	public String getItemSkillNo()
	{
		return itemSkillNo;
	}
	public void setItemSkillNo(String itemSkillNo)
	{
		this.itemSkillNo=itemSkillNo;
	}
	
	public String getItemSkillName()
	{
		return itemSkillName;
	}
	
	public void setItemSkillName(String itemSkillName)
	{
		this.itemSkillName=itemSkillName;
	}
	
	public String getItemSkillDepict()
	{
		return itemSkillDepict;
	}	
	public void setItemSkillDepict(String itemSkillDepict)
	{
		this.itemSkillDepict=itemSkillDepict;
	}

}
