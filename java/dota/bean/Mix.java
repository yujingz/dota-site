package dota.bean;

public class Mix {
	String shopNo;
	String itemNo;
	String mixShopNo;
	String mixItemNo;
	
	public Mix(){}
	
	public String getshopNo()
	{
		return shopNo;
	}
	
	public void setShopNo(String shopNo)
	{
		this.shopNo=shopNo;
	}
	
	public String getItemNo()
	{
		return itemNo;
	}
	
	public void setItemNo(String itemNo)
	{
		this.itemNo=itemNo;
	}
	
	public String getMixShopNo()
	{
		return mixShopNo;
	}
	
	public void setMixShopNo(String mixShopNo)
	{
		this.mixShopNo=mixShopNo;
	}
	
	public String getMixItemNo()
	{
		return mixItemNo;
	}
	
	public void setMixItemNo(String mixItemNo)
	{
		this.mixItemNo=mixItemNo;
	}
	
}
