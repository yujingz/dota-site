package dota.bean;

public class Vedio {
	String vedioNo;
	String pageNo;
	String vedioTitle;
	String vedioDepict;
	String vedioCommentNum;

	
	public Vedio(){}
	
	public String getVedioNo()
	{
		return vedioNo;
	}
	
	public void setVedioNo(String vedioNo)
	{
		this.vedioNo=vedioNo;
	}
	public String getPageNo()
	{
		return pageNo;
	}
	
	public void setPageNo(String pageNo)
	{
		this.pageNo=pageNo;
	}
	public String getVedioTitle()
	{
		return vedioTitle;
	}
	
	public void setVedioTitle(String vedioTitle)
	{
		this.vedioTitle=vedioTitle;
	}
	public String getVedioDepict()
	{
		return vedioDepict;
	}
	
	public void setVedioDepict(String vedioDepict)
	{
		this.vedioDepict=vedioDepict;
	}
	public String getVedioCommentNum()
	{
		return vedioCommentNum;
	}
	
	public void setVedioCommentNum(String vedioCommentNum)
	{
		this.vedioCommentNum=vedioCommentNum;
	}
	
}
