package dota.bean;

import java.util.Date;

public class Member 
{
	String memberNo;
	String password;
	String name;
	Date birthday;
	
	public Member(){}
	
	public String getMemberNo() 
	{
		return memberNo;
	}
	public void setMemberNo(String memberNo) 
	{
		this.memberNo = memberNo;
	}
	public String getPassword() 
	{
		return password;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}

	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
}
