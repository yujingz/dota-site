package dota.bean;

public class Skill {
	String heroNo;
	String skillNo;
	String skillName;
	String skillPic;
	String skillShortcut;
	String skillDepict;
	
	public Skill(){}
	
	public String getHeroNo()
	{
		return heroNo;
	}
	
	public void setHeroNo(String heroNo)
	{
		this.heroNo=heroNo;
	}
	public String getSkillNo()
	{
		return skillNo;
	}
	
	public void setSkillNo(String skillNo)
	{
		this.skillNo=skillNo;
	}
	public String getSkillName()
	{
		return skillName;
	}
	
	public void setSkillName(String skillName)
	{
		this.skillName=skillName;
	}
	public String getSkillPic()
	{
		return skillPic;
	}
	
	public void setSkillPic(String skillPic)
	{
		this.skillPic=skillPic;
	}
	public String getSkillShortcut()
	{
		return skillShortcut;
	}
	
	public void setSkillShortcut(String skillShortcut)
	{
		this.skillShortcut=skillShortcut;
	}
	public String getSkillDepict()
	{
		return skillDepict;
	}
	
	public void setSkillDepict(String skillDepict)
	{
		this.skillDepict=skillDepict;
	}

}
