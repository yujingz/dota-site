package dota.bean;

public class Level {
	String heroNo;
	String skillNo;
	String levelNo;
	String levelDepict;
	
	public Level(){}
	
	public String getHeroNo()
	{
		return heroNo;
	}
	public void setHeroNo (String heroNo)
	{
		this.heroNo =heroNo;
	}
	public String getSkillNo()
	{
		return skillNo;
	}
	public void setSkillNo (String skillNo)
	{
		this.skillNo =skillNo;
	}
	public String getLevelNo()
	{
		return levelNo;
	}
	public void setLevelNo (String levelNo)
	{
		this.levelNo =levelNo;
	}
	public String getLevelDepict()
	{
		return levelDepict;
	}
	public void setLevelDepict (String levelDepict)
	{
		this.levelDepict =levelDepict;
	}
	
	

}
