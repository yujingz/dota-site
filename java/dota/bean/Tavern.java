package dota.bean;

public class Tavern {
	String tavernNo;
	String tavernName;
	String tavernPic;
	
	public Tavern(){}
	
	public String getTavernNo()
	{
		return tavernNo;
	}
	
	public void setTavernNo(String tavernNo)
	{
		this.tavernNo=tavernNo;
	}
	
	public String getTavernName()
	{
		return tavernName;
	}
	
	public void SetTavernName(String tavernName)
	{
		this.tavernName=tavernName;
	}
	
	public String getTavernPic()
	{
		return tavernPic;
	}
	
	public void setTavernPic(String tavernPic)
	{
		this.tavernPic=tavernPic;
	}

}
