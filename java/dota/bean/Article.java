package dota.bean;

import java.util.Date;

public class Article {
	String articleNo;
	String pageNo;
	String category;
	String articleTitle;
	Date articleDate;
	String content;
	
	public Article(){}
	
	public String getArticleNo()
	{
		return articleNo;
	}
	
	public void setArticleNo(String articleNo)
	{
		this.articleNo=articleNo;
	}
	
	public String getPageNo()
	{
		return pageNo;
	}
	
	public void setPageNo(String pageNo)
	{
		this.pageNo=pageNo;
	}
	
	public String getCategory()
	{
		return category;
	}
	
	public void setCategory(String category)
	{
		this.category=category;
	}
	
	public String getArticleTitle()
	{
		return articleTitle;
	}
	
	public void setArticleTitle(String articleTitle)
	{
		this.articleTitle=articleTitle;
	}
	
	public Date getArticleDate()
	{
		return articleDate;
	}
	
	public void setArticleDate(Date articleDate)
	{
		this.articleDate=articleDate;
	}
	
	public String getContent()
	{
		return content;
	}
	
	public void setContent(String content)
	{
		this.content=content;
	}

}
