package dota.bean;

public class Shop {
	String shopNo;
	String shopName;
	String shopPic;
	
	public Shop(){}
	
	public String getShopNo()
	{
		return shopNo;
	}
	
	public void setShopNo(String shopNo)
	{
		this.shopNo=shopNo;
	}
	
	public String getShopName()
	{
		return shopName;
	}
	
	public void SetShopName(String shopName)
	{
		this.shopName=shopName;
	}
	
	public String getShopPic()
	{
		return shopPic;
	}
	
	public void setShopPic(String shopPic)
	{
		this.shopPic=shopPic;
	}

}
