package dota.controller;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dota.action.SearchArticle;
import dota.bean.Article;


public class LoadArticle extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		SearchArticle sa=new SearchArticle();
		List<Article> list=sa.getArticlesInfo("001");
		
		request.getSession().setAttribute("articleList", list);
	
		try 
		{
			response.sendRedirect("/dota/articleList.jsp");
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

}
