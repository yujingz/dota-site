package dota.controller;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import dota.action.SearchHeroes;
import dota.bean.Hero;


public class LoadTavern extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String heroNo=request.getParameter("click_on");
			
		SearchHeroes shes=new SearchHeroes();
		List<Hero> heroes=shes.getHeroesInfo(heroNo);
		request.getSession().setAttribute("heroes", heroes);
		
	
		try 
		{
			response.sendRedirect("/dota/displayHeroesInTavern.jsp");
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

}
