package dota.controller;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dota.action.SearchAComment;
import dota.action.SearchVedio;
import dota.bean.AComment;
import dota.bean.Vedio;


public class LoadVedioInfo extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
						
		String vedioNo=request.getParameter("vedioNo");
		
		SearchVedio sv=new SearchVedio();
		Vedio v=sv.getVedioInfo(vedioNo);
		SearchAComment sac=new SearchAComment();
		List<AComment> list=sac.getACommentsInfo(vedioNo, "v");
		request.getSession().setAttribute("vedio", v);
		request.getSession().setAttribute("aComments", list);
		
		try 
		{
			response.sendRedirect("/dota/displayVedio.jsp");
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

}
