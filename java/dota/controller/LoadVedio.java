package dota.controller;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dota.action.SearchVedios;
import dota.bean.Vedio;


public class LoadVedio extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//String VedioNo=request.getParameter("click_on");
			
		SearchVedios sv=new SearchVedios();
		List<Vedio> list=sv.getVediosInfo("1");
		
		request.getSession().setAttribute("vedioList", list);
	
		try 
		{
			response.sendRedirect("/dota/vedioList.jsp");
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

}
