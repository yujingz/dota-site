package dota.controller;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import dota.action.SearchItems;
import dota.bean.Item;


public class LoadShop extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String shopNo=request.getParameter("click_on");
			
		SearchItems sis=new SearchItems();
		List<Item> items=sis.getItemsInfo(shopNo);
		request.getSession().setAttribute("itemList", items);
		
	
		try 
		{
			response.sendRedirect("/dota/displayItemsInShop.jsp");
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

}
