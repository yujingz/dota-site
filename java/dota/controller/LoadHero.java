package dota.controller;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dota.action.SearchHero;
import dota.action.SearchLevel;
import dota.action.SearchSkill;
import dota.bean.Hero;
import dota.bean.Level;
import dota.bean.Skill;

public class LoadHero extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String heroNo=request.getParameter("click_on");
			
		SearchHero sh=new SearchHero();
		Hero h=sh.getHeroInfo(heroNo);
		SearchSkill ss=new SearchSkill();
		List<Skill> s=ss.getSkillInfo(heroNo);
		SearchLevel sl=new SearchLevel();
		List<Level> firstSkillLevelInfo=sl.getLevelInfo(heroNo, "1");
		List<Level> secondSkillLevelInfo=sl.getLevelInfo(heroNo, "2");
		List<Level> thirdSkillLevelInfo=sl.getLevelInfo(heroNo, "3");
		List<Level> lastSkillLevelInfo=sl.getLevelInfo(heroNo, "4");

		request.getSession().setAttribute("levelList1", firstSkillLevelInfo);
		request.getSession().setAttribute("levelList2", secondSkillLevelInfo);
		request.getSession().setAttribute("levelList3", thirdSkillLevelInfo);
		request.getSession().setAttribute("levelList4", lastSkillLevelInfo);
		request.getSession().setAttribute("hero", h);
		request.getSession().setAttribute("skillList", s);
		
	
		try 
		{
			response.sendRedirect("/dota/heroInfo.jsp");
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

}
