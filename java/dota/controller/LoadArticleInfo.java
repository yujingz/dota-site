package dota.controller;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dota.action.SearchAComment;
import dota.action.SearchArticle;
import dota.bean.AComment;
import dota.bean.Article;


public class LoadArticleInfo extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String articleNo=request.getParameter("articleNo");
			
		SearchArticle sa=new SearchArticle();
		Article a=sa.getArticleInfo(articleNo);
		SearchAComment sac=new SearchAComment();
		List<AComment> list=sac.getACommentsInfo(articleNo, "a");
		request.getSession().setAttribute("article", a);
		request.getSession().setAttribute("aComments", list);
		try 
		{
			response.sendRedirect("/dota/displayArticle.jsp");
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

}
