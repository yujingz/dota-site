package dota.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dota.action.Login;
import dota.bean.Member;

public class Register extends HttpServlet 
{

	private static final long serialVersionUID = -8052223741830167994L;

	public void service(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String memberNo = request.getParameter("member_no");
		String password = request.getParameter("password");
		//String rePassword = request.getParameter("rePassword");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-mm-dd");
		Date d=new Date();
		try {
			d = dateFormat.parse(birthday);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Member member = new Member();
		member.setMemberNo(memberNo);
		member.setPassword(password);
		member.setName(name);
		member.setBirthday(d);
		Login login = new Login();
		login.memberRegister(member);

			try {
				response.sendRedirect("/dota/errorpage.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		
	/*
			request.getSession().setAttribute("member", member);

			try {
				//response.sendRedirect("/dota/servlet/dota.controller.LoadArticle");
				response.sendRedirect("/dota/index.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		*/
	}

}
