package dota.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dota.action.SearchAComment;
import dota.bean.AComment;
import dota.bean.Member;

public class RefreshAComment extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response) {
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Member m = (Member) request.getSession().getAttribute("member");
		if ((m != null) && (m.getMemberNo() != null)) {
			String content = request.getParameter("content");
			String memPetname = request.getParameter("memPetname");
			String objectNo = request.getParameter("objectNo");
			String category = request.getParameter("category");
			Date d = new Date();
			AComment ac = new AComment();
			ac.setObjectNo(objectNo);
			ac.setMemPetname(memPetname);
			ac.setDate(d);
			ac.setContent(content);
			SearchAComment sac = new SearchAComment();

			if (category.equals("a")) {
				ac.setCategory("a");
				sac.addAComment(ac);
				List<AComment> list = sac.getACommentsInfo(objectNo, "a");
				request.getSession().setAttribute("aComments", list);

				try {
					response.sendRedirect("/dota/displayArticle.jsp");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (category.equals("v")) {
				ac.setCategory("v");
				sac.addAComment(ac);
				List<AComment> list = sac.getACommentsInfo(objectNo, "v");
				request.getSession().setAttribute("aComments", list);

				try {
					response.sendRedirect("/dota/displayVedio.jsp");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				try {
					response.sendRedirect("/dota/errorpage.jsp");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} else {
			try {
				response.sendRedirect("/dota/errorpage.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
