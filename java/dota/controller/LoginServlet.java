package dota.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import dota.action.Login;
import dota.bean.Member;

public class LoginServlet extends HttpServlet 
{

	private static final long serialVersionUID = -8052223741830167994L;

	public void service(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String memberNo = request.getParameter("member_no");
		String password = request.getParameter("password");

		Login login = new Login();
		Member member = login.memberLogin(memberNo, password);
		

		if (member == null)
		{
			try {
				response.sendRedirect("/dota/errorpage.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else 
		{
			request.getSession().setAttribute("member", member);

			try {
				//response.sendRedirect("/dota/servlet/dota.controller.LoadArticle");
				response.sendRedirect("/dota/index.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
