package dota.controller;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dota.action.SearchItem;
import dota.action.SearchItemSkills;
import dota.action.SearchMix;
import dota.bean.Item;
import dota.bean.ItemSkill;
import dota.bean.Mix;



public class LoadItem extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response)
	{
		try {
			request.setCharacterEncoding("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String itemNo=request.getParameter("click_on");
		
		SearchItem si=new SearchItem();
		Item i=si.getItemInfo(itemNo);
		SearchItemSkills sis =new SearchItemSkills();
		List<ItemSkill> is=sis.getItemSkillsInfo(itemNo);
		
		List<Item> mixList=new ArrayList<Item>();
		
		SearchMix sm=new SearchMix();
		List<Mix> m=sm.getMixInfo(itemNo);
		//if(m!=null){
			Iterator<Mix> it=m.iterator();
			while(it.hasNext())
			{
				Mix mix=(Mix)it.next();
				Item item=si.getItemInfo(mix.getMixItemNo());
				mixList.add(0, item);
			}
			request.getSession().setAttribute("mixList", mixList);
		//}
		request.getSession().setAttribute("item", i);
		if(is.isEmpty())
			request.getSession().setAttribute("itemSkills", null);
		else	
			request.getSession().setAttribute("itemSkills", is);
		
	
		try 
		{
			response.sendRedirect("/dota/equipmentInfo.jsp");
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

}
