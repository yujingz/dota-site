package dao.dota;

import java.util.List;

import dota.bean.Level;

public interface LevelDao {
	public List<Level> getAllLevelInfoByHeroNoAndSkillNo(String heroNo,String skillNo) throws Exception;

}