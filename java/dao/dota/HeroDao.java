package dao.dota;

import dota.bean.Hero;

public interface HeroDao
{
	public Hero getHeroByHeroNo(String heroNo) throws Exception;
}
