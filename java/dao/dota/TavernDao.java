package dao.dota;

import dota.bean.Tavern;

public interface TavernDao {
	public Tavern getTavernByTavernNo(String tavernNo) throws Exception;
}
