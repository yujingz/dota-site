package dao.dota;

import java.util.List;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.AComment;

public class ACommentDaoImpl extends BaseDao implements PostgreSQL, ACommentDao
{

	public int addAComment(AComment aComment) throws Exception {
		// TODO Auto-generated method stub
		String sql = "insert into a_comment(object_no, mem_petname, date, content, category) values (?,?,?,?,?)";
		return esql.update(sql,aComment.getObjectNo(),aComment.getMemPetname(),aComment.getDate(), aComment.getContent(),aComment.getCategory());	
	}

	public List<AComment> getACommentsByObjectNoAndCategory(String objectNo,
			String category) throws Exception {
		// TODO Auto-generated method stub
		String sql="select mem_petname, date, content, category from a_comment where object_no=? and category=?";
		// TODO Auto-generated method stub
		return esql.list(AComment.class, sql,objectNo,category);
	}

}
