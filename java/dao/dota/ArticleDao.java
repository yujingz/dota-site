package dao.dota;

import java.util.List;

import dota.bean.Article;

public interface ArticleDao
{
	public List<Article> getArticlesByPageNo(String pageNo) throws Exception;	
	public Article getArticleByArticleNo(String articleNo) throws Exception;
}
