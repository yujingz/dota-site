package dao.dota;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Hero;

public class HeroDaoImpl extends BaseDao implements PostgreSQL, HeroDao
{
	public Hero getHeroByHeroNo(String heroNo) throws Exception {
		String sql="select * from hero where hero_no = ?";
		// TODO Auto-generated method stub
		return esql.query(Hero.class, sql, heroNo);
	}

}
