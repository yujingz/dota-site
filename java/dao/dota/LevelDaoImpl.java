package dao.dota;

import java.util.List;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Level;

public class LevelDaoImpl extends BaseDao implements LevelDao, PostgreSQL{

	public List<Level> getAllLevelInfoByHeroNoAndSkillNo(String heroNo,
			String skillNo) throws Exception {
		String sql = "select * from level where hero_no = ? and skill_no = ?";
		return esql.list(Level.class, sql, heroNo, skillNo);
	}
}
