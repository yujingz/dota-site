package dao.dota;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Tavern;

public class TavernDaoImpl extends BaseDao implements PostgreSQL, TavernDao
{

	public Tavern getTavernByTavernNo(String tavernNo) throws Exception {
		String sql="select * from tavern where tavern_no = ?";
		// TODO Auto-generated method stub
		return esql.query(Tavern.class, sql, tavernNo);
	}

}
