package dao.dota;

import dota.bean.Shop;

public interface ShopDao {
	public Shop getShopByShopNo(String shopNo) throws Exception;
}
