package dao.dota;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Shop;

public class ShopDaoImpl extends BaseDao implements PostgreSQL, ShopDao
{

	public Shop getShopByShopNo(String shopNo) throws Exception {
		String sql="select * from Shop where shop_no = ?";
		// TODO Auto-generated method stub
		return esql.query(Shop.class, sql, shopNo);
	}

}
