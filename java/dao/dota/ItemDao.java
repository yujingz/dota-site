package dao.dota;

import dota.bean.Item;

public interface ItemDao
{
	public Item getItemByItemNo(String itemNo) throws Exception;
}
