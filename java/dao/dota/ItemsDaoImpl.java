package dao.dota;

import java.util.List;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Item;

public class ItemsDaoImpl extends BaseDao implements ItemsDao, PostgreSQL{

	public List<Item> getAllItemsByShopNo(String shopNo) throws Exception {
		String sql ="select * from item where shop_no = ?";
		// TODO Auto-generated method stub
		return esql.list(Item.class, sql, shopNo);
	}
}
