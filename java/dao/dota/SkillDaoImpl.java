package dao.dota;

import java.util.List;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Skill;


public class SkillDaoImpl extends BaseDao implements PostgreSQL, SkillDao
{

	public List<Skill> getAllSkillsByHeroNo(String heroNo) throws Exception {
		String sql ="select * from skill where hero_no = ?";
		// TODO Auto-generated method stub
		return esql.list(Skill.class, sql, heroNo);
	}


}
