package dao.dota;

import java.util.List;
import dota.bean.ItemSkill;

public interface ItemSkillDao
{
	public List<ItemSkill> getItemSkillByItemNo(String itemNo) throws Exception;
}



