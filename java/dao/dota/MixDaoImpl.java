package dao.dota;

import java.util.List;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Mix;


public class MixDaoImpl extends BaseDao implements PostgreSQL, MixDao
{
	public List<Mix> getItemsByItemNo(String itemNo)
			throws Exception {
		String sql="select * from mix where item_no= ?";
		// TODO Auto-generated method stub
		return esql.list(Mix.class, sql, itemNo);
	}

}
