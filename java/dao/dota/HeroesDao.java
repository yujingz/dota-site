package dao.dota;

import java.util.List;

import dota.bean.Hero;

public interface HeroesDao {
	public List<Hero> getAllHeroesByTavernNo(String tavernNo) throws Exception;

}
