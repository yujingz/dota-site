package dao.dota;

import java.util.List;

import dota.bean.AComment;

public interface ACommentDao
{
	public List<AComment> getACommentsByObjectNoAndCategory(String objectNo,String category) throws Exception;	
	public int addAComment(AComment aComment) throws Exception;
}
