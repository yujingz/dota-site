package dao.dota;

import java.util.List;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Vedio;

public class VedioDaoImpl extends BaseDao implements PostgreSQL, VedioDao
{
	public List<Vedio> getVediosByPageNo(String pageNo) throws Exception {
		String sql="select * from vedio where page_no=?";
		// TODO Auto-generated method stub
		return esql.list(Vedio.class, sql,pageNo);
	}

	public Vedio getVediosByVedioNo(String vedioNo) throws Exception {
		String sql="select * from vedio where vedio_no=?";
		// TODO Auto-generated method stub
		return esql.query(Vedio.class, sql,vedioNo);
	}

}
