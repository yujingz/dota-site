package dao.dota;

import java.util.List;
import dota.bean.Mix;

public interface MixDao
{
	public List<Mix> getItemsByItemNo(String itemNo) throws Exception;
}



