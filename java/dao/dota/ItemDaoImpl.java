package dao.dota;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Item;

public class ItemDaoImpl extends BaseDao implements PostgreSQL, ItemDao
{
	public Item getItemByItemNo(String itemNo) throws Exception {
		String sql="select * from item where item_no = ?";
		// TODO Auto-generated method stub
		return esql.query(Item.class, sql, itemNo);
	}

}
