package dao.dota;

import java.util.List;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Hero;

public class HeroesDaoImpl extends BaseDao implements HeroesDao, PostgreSQL{

	public List<Hero> getAllHeroesByTavernNo(String tavernNo) throws Exception {
		String sql ="select * from hero where tavern_no = ?";
		// TODO Auto-generated method stub
		return esql.list(Hero.class, sql, tavernNo);
	}
}
