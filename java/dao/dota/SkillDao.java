package dao.dota;

import java.util.List;

import dota.bean.Skill;

public interface SkillDao {
	public List<Skill> getAllSkillsByHeroNo(String heroNo) throws Exception;

}
