package dao.dota;

import dota.bean.Member;

public interface MemberDao 
{
	public Member getMemberByMemberNoAndPassword(String memberNo, String password) throws Exception;
	public int addNewMember(Member member)throws Exception;
}
