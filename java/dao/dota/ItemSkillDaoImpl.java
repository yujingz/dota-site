package dao.dota;

import java.util.List;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.ItemSkill;

public class ItemSkillDaoImpl extends BaseDao implements ItemSkillDao, PostgreSQL{

	public List<ItemSkill> getItemSkillByItemNo(String itemNo) throws Exception {
		String sql ="select * from item_skill where item_no= ?";
		// TODO Auto-generated method stub
		return esql.list(ItemSkill.class, sql, itemNo);
	}
}
