package dao.dota;

import java.util.List;

import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Article;

public class ArticleDaoImpl extends BaseDao implements PostgreSQL, ArticleDao
{
	public List<Article> getArticlesByPageNo(String pageNo) throws Exception {
		String sql="select * from article where page_no=?";
		// TODO Auto-generated method stub
		return esql.list(Article.class, sql,pageNo);
	}

	public Article getArticleByArticleNo(String articleNo) throws Exception {
		String sql="select * from article where article_no=?";
		// TODO Auto-generated method stub
		return esql.query(Article.class, sql,articleNo);
	}

}
