package dao.dota;

import java.util.List;

import dota.bean.Vedio;

public interface VedioDao
{
	public List<Vedio> getVediosByPageNo(String pageNo) throws Exception;	
	public Vedio getVediosByVedioNo(String vedioNo) throws Exception;
}
