package dao.dota;

import java.util.List;

import dota.bean.Item;

public interface ItemsDao {
	public List<Item> getAllItemsByShopNo(String shopNo) throws Exception;

}
