package dao.dota;


import dao.BaseDao;
import dao.PostgreSQL;
import dota.bean.Member;

public class MemberDaoImpl extends BaseDao implements MemberDao, PostgreSQL 
{
	public Member getMemberByMemberNoAndPassword(String memberNo, String password) throws Exception 
	{
		String sql = "select * from member where member_no = ? and password = ?";
		return esql.query(Member.class, sql, memberNo, password);
	}

	public int addNewMember(Member member) throws Exception {
		// TODO Auto-generated method stub
		String sql = "insert into member(member_no, password, birthday, name) values (?,?,?,?)";
		return esql.update(sql,member.getMemberNo(),member.getPassword(),member.getBirthday(),member.getName());
	}

}
